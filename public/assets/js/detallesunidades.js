
function  detallesunidades(idsubrubro,idunidad,url='/detallesfisicos') {
console.log(idsubrubro,idunidad);
              $.ajax({
                 type:'get',
                 url: url,
                 data:{
                   '_token':$('input[name=_token').val(),
                   'idsubrubro':idsubrubro,
                   'idunidad':idunidad,
                 },

             success:function(data){
              switch(url) {
                case '/detalleshumanos':
                  // code block
                  break;
                case '/detallesfisicos':
                  onSuccessFisicos(data);
                  break;
                case '/detallesmateriales':
                  onSuccessMateriales(data)
                  break;
                default:
                  // code block
              }

                

                         
             },
    });
}


function onSuccessFisicos(data) {
  console.log(data);
  let body = "";
  let head = ` <div class="kt-section">
          <div class="kt-section__content">
            <table class="table table-striped">
                <thead>
                  <tr>
                      <th>Recurso Fisico</th>
                      <th>Total</th>
                     
                  </tr>
                </thead>
                <tbody>`;


                
           
  $.each(data, function(k, v){
    body += `<tr > <td>${ v.cFisico }</td> <td >${ v.totales }</td></tr>`;
  })

  let footer = `</tbody>
            </table>
          </div>
        </div>`;

  $('#contenido').html(head+body+footer);

  $('#fisicos').modal('show');
}


function onSuccessMateriales(data) {
  console.log(data);
  let body = "";
  let head = ` <div class="kt-section">
          <div class="kt-section__content">
            <table class="table table-striped">
                <thead>
                  <tr>
                      <th>Recurso Fisico</th>
                      <th>Total</th>
                     
                  </tr>
                </thead>
                <tbody>`;


                
           
  $.each(data, function(k, v){
    body += `<tr > <td>${ v.cEquipamiento }</td> <td >${ v.total }</td></tr>`;
  })

  let footer = `</tbody>
            </table>
          </div>
        </div>`;

  $('#contenidomaterial').html(head+body+footer);

  $('#materiales').modal('show');
}