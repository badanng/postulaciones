//variables globales
  var id_fuaps;//variable para obtener el id del fuaps
  var tipo_fuapselect;

$('.baja_equipo').click(function(){ //////////////////////////////////////////////////////////////////////////////////
  });//////////////////////////////////////////////////////////////////////////////////////////////////////////////////


$('.alta_equipo').click(function(){ //////////////////////////////////////////////////////////////////////////////////

    llenadoSelectEquipo();
    id_fuaps = this.value;
  });//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$('.guarda_alta_equipo').click(function(){
    
                   $.ajax({
                 type:'post',
                 url: '/equipo/nuevoEquipo',
                 data:{
                   '_token':$('input[name=_token').val(),
                   'id_fuaps':id_fuaps,
                   'marca':$('input[name=marca').val(),
                   'modelo':$('input[name=modelo').val(),
                   'serie':$('input[name=serie').val(),
                   'tipo_equipo':$('#tipo_equipo').val(),

                 },
             success:function(data){

      
             },

           });
});

$('.alta_fuap').click(function(){ //////////////////////////////////////////////////////////////////////////////////

    llenadoSelectEquipo();
  });


function llenadoSelectEquipo(){//funcion para llenar los select 
      $.ajax({
  headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/equipo/selects_tipo_equipo',
        dataType: 'html',
        beforeSend: function () 
                {
                  
                },

        success: function(data){
          var valores = JSON.parse(data);

        $.each(valores.equipo_tipo,function(key, registro) {
          var cadena = registro.tipo.toUpperCase();
           $('#tipo_equipo').append('<option value='+registro.id+'>'+ cadena +'</option>');
        }); 
    },
        error: function(resp_success){ alerta('Alerta!','Error Modal Editar Usuario 1');}
      });
}/////////////////////////////////////////////////////////////////////////////



$('.historial').click(function(){
    $id_fuaps=this.value;
                   $.ajax({
                 type:'get',
                 url: '/historico/historicoEquipo',
                 data:{
                   '_token':$('input[name=_token').val(),
                   'id_fuaps':$id_fuaps,
                 },
             success:function(data){
               //$("#id").value(data);
               $('#id').val(data[0].id);
             },
           });
});

// par el select tipo fuap
$(document).ready(function() {
    $("#tipo_fuap").change(function() {
          tipo_fuapselect =($('#tipo_fuap option:selected').html());
    });
});
// fin select tipo fuap

// referencia altafuap
$('#guarda_fuap').click(function(){
      if($('#nombre_r').val() != '' && $('#correo_r').val() != '' &&
         $('#expediente').val() != '' && $('#marca').val() != '' && $('#modelo').val() != '' && $('#serie') != ''){
        $.ajax({
                 type:'post',
                 url: '/fuap/guardafuap',
                 data:{
                  '_token':$('input[name=_token').val(),
                  // tipo fuapperfil_fuap
                  'tipo_fuap':$('#tipo_fuap').val(),

                  'perfil_fuap':$('#perfil_fuap').val(),
                    
                  // resguardante
                  'nombre_r':$('input[name=nombre_r]').val(),
                  'correo_r':$('input[name=correo_r]').val(),
                  // equipo
                   'expediente':tipo_fuapselect,
                   'marca':$('input[name=marca').val(),
                   'modelo':$('input[name=modelo').val(),
                   'serie':$('input[name=serie').val(),
                   'tipo_equipo':$('#tipo_equipo').val(),
                 },
             success:function(data){
              swal("ALta", "Datos Dados de Alta", "success")
                .then((value) => {
                    icon: "success";
                });
                  $('input[type="text"]').val('');
                  $().val('selected[type]');
                  $('#tipo_fuap').val('');
                  $('#perfil_fuap').val('');
                  $('#tipo_equipo').val('');
             },
        });
      }else{
        swal({
              title: "Verifica",
              text: "Campos Requeridos",
              type: "info",
            });
      }
});




// modal guarda equipo
// referencia altafuap
$('#guarda_equipoM').click(function(){
      if($('#marcaM').val() != '' && $('#modeloM').val() != '' && $('#serieM') != ''){
        $.ajax({
                 type:'post',
                 url: '/fuap/guardaequipo',
                 data:{
                  '_token':$('input[name=_token').val(),
                   'expediente':tipo_fuapselect,
                   'marca':$('input[name=marcaM').val(),
                   'modelo':$('input[name=modeloM').val(),
                   'serie':$('input[name=serieM').val(),
                   'tipo_equipo':$('#tipo_equipoM').val(),
                 },
             success:function(data){
              swal("ALta", "Datos Dados de Alta", "success")
                .then((value) => {
                    icon: "success";
                });
                  $('input[type="text"]').val('');
                  $().val('selected[type]');
                  $('#tipo_equipoM').val('');
             },
        });
      }else{
        swal({
              title: "Verifica",
              text: "Campos Requeridos",
              type: "info",
            });
      }
});
// fin modal guarda equipo
//validacion de solo letras
function soloLetras(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toString();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyzÁÉÍÓÚABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
    especiales = [8, 37, 39, 46, 6]; 
    tecla_especial = false
    for(var i in especiales) {
        if(key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }
    if(letras.indexOf(tecla) == -1 && !tecla_especial){
        // alert('prueba de mensaje');
        return false;
      }
}



