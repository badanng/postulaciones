//-- Tabla de vista Inventarios --//


  $(document).ready( function() //funcion que se encarga de visualizar todaas las entidades
   {
      $('#inventario_table').DataTable({ //tabla para cargar todas las entidades
          "pageLength": 5,
           processing: true,
           serverSide: true,
           ajax: "censo/entidad",
           columns: [
                    { data: 'id_entidad', name: 'id_entidad' },
                    { data: 'entidad', name: 'entidad' },
                    { render: function () {

                        return '<button  class="btn btn-info btn-outline-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" data-toggle="modal" data-target="#editt" ><i class="fas fa-cog left"></i></button>';}
                    } 
           ]
        });
   } );//////////////////////////////////////////////////////




   $('#table_entidad').DataTable({
        "lengthMenu": [[32, 50, 75, -1], [32, 50, 75, "All"]]
    });
   $('#table_unidadesMed').DataTable({
        "lengthMenu": [[50, 75, 100, -1], [50, 75, 100, "All"]]
    });

$('#filtroFuap').click(function(){//funcion para el listado de fuaps por unidad 
   
 var id_ubicacion =  $('#unidades').val();

$('#inventario_table').DataTable({
           processing: true,
           serverSide: true,//sirve para traer los datos del servidor
           // destroy: true,
            ajax:{
           url : '/censo/recusosfisicos',
           // type: "get",
           // data: {"id_ubicacion": id_ubicacion}
            },
           columns: [
                    { data: 'Id_RecFisico', name: 'Id_RecFisico' },
                     { data: 'id_Fisicos', name: 'id_Fisicos'},
                     { data: 'id_Subrubro', name: 'id_Subrubro' },
                     { data: 'bFueraServicio', name: 'bFueraServicio' },
                     { data: 'Id_CausaFueraServ', name: 'Id_CausaFueraServ' },
                 ]
        });
  });
//-- Fin fucnion vista inventarios  --//
//-- Tabla de vista Usuarios --//
$('#user_table').DataTable({
           processing: true,
           serverSide: true,
           ajax: "user/action",
           columns: [
                    { data: 'nombre', name: 'nombre' },
                    { data: 'puesto', name: 'puesto' },
                    { data: 'email', name: 'email' },
                    { data: 'estado', name: 'estado' },
                    // { data: 'subdireccion', name: 'subdireccion' },
                    { data: 'telefono', name: 'telefono' },
                    // { data: 'red', name: 'red' },
                    // { data: 'unidad_laboral', name: 'unidad_laboral' },
                    { render: function () {

                        return '<button  class="btn btn-info btn-outline-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" data-toggle="modal" data-target="#editt" ><i class="fas fa-cog left"></i></button>';}
                    } 

       
            ]
        });
//-- Fin fucnion vista usuarios  --//
//-- Tabla de vista Ubicación --//
$('#ubicaciones_table').DataTable({
           processing: true,
           serverSide: true,
           ajax: "ubicacion/ubicacionesList",
           columns: [
                    { data: 'id', name: 'id' },
                    { data: 'zona.nombre_zona', name: 'zona.nombre_zona' },
                    { data: 'inmueble', name: 'inmueble' },
                    { data: 'codigo_postal', name: 'codigo_postal' },
                    { data: 'municipio.municipio', name: 'municipio.municipio' },
                    { data: 'colonia', name: 'colonia' },
                    { data: 'ubicacion', name: 'ubicacion'}
                 ]
        });
//-- Fin fucnion vista ubicación  --//
//-- Funcion para llenar los Select de la vista inventarios filtrado --//

