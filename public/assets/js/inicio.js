 $.sessionTimeout({
    title: 'Su sesión está por expirar',
    message: '¿Desea continuar con su actual sesión activa?.',
    countdownMessage: 'Se cerrará su sesión en',
    logoutButton: 'Salir',
    keepAliveButton: 'Continuar',
   	// keepAliveUrl:'/',
    ajaxType: 'POST',
    logoutUrl: '/sessiondestroy',
    redirUrl: '/sessiondestroy',
    
    warnAfter:  500000,//9 minutos para que caduque la sesión
    redirAfter: 600000,//tiempo de espera despues de que sale la modal de caducar 
    countdownBar:true
});
