//----------------------------------funcion para agregar un nuevo usuario---------------------//

$('#agregarUsuario').click(function(){ 
    var msj_error="";
      //valida que los campos no esten vacios
     if( document.getElementById("nombre").value  == ""){
      msj_error+='Olvidó ingresar el nombre.\n';
     }else{
      document.getElementById('nombre').style.borderColor = "green";
     }
     if( document.getElementById("puesto").value  == ""){
      msj_error+='<br>Olvidó ingresar el puesto.\n';
     }
     else{
      document.getElementById('puesto').style.borderColor = "green";
     }
     if( document.getElementById("email").value  == ""){
      msj_error+='<br>Olvidó ingresar el email.\n';
      }else{
        document.getElementById('email').style.borderColor = "green";
      } 
     /* if( document.getElementById("subdireccion").value  == ""){
      msj_error+='<br>Olvidó ingresar la subdirección.\n';
     }
     else{
      document.getElementById('subdireccion').style.borderColor = "green";
      }   */
     if( document.getElementById("estado").value  == ""){
        msj_error+='<br>Olvidó poner un estado.\n';
      }
     else{
      document.getElementById('estado').style.borderColor = "green";
      }
     if( document.getElementById("telefono").value  == ""){
        msj_error+='<br>Olvidó ingresar un telefono.\n';
      }
     else{
      document.getElementById('telefono').style.borderColor = "green";
      }
    //  if( document.getElementById("red").value  == ""){
    //     msj_error+='<br>Olvidó ingresar una red o extensión.\n';
    //   }
    //  else{
    //   document.getElementById('red').style.borderColor = "green";
    //   }
     /* if( document.getElementById("unidad_laboral").value  == ""){
        msj_error+='<br>Olvidó ingresar el lugar de trabajo de la persona.\n';
      }
     else{
      document.getElementById('unidad_laboral').style.borderColor = "green";
      } */
     if( document.getElementById("rol_usuario").value  == ""){
        msj_error+='<br>Olvidó ingresar el rol del usuario.\n';
      }
     else{
      document.getElementById('rol_usuario').style.borderColor = "green";
      } 
     if( document.getElementById("password").value  == ""){
        msj_error+='<br>Olvidó ingresar la contraseña. (Recuerda que deben ser 8 caracteres minimos)\n';
      }
     else{
      document.getElementById('password').style.borderColor = "green";
      }    
        if( !msj_error == "" ){
             swal("Error",msj_error,"error");
              return false;
            }   
            if($('#email').val().substr(-14)=="@issste.gob.mx"){
                   $.ajax({
                      type:'post',
                      url: '/personalUsuario/registroUsuario',
                      data:{'id_entidad':$('#estado').val(),
                        '_token':$('input[name=_token').val(),
                        'nombre':$('input[name=nombre').val(),
                        'puesto':$('input[name=puesto').val(),
                        'email':$('input[name=email').val(),
                        'estado':$('#estado').val(),
                        'telefono':$('#telefono').val(),
                        // 'red':$('#red').val(),
                       /*  'unidad_laboral':$('#unidad_laboral').val(),     */                
                        'password':$('input[name=password').val(),
                       /*  'subdireccion':$('#subdireccion').val(), */
                        'id_rol':$('#rol_usuario').val(),
                      },
                      success:function(data){
                        if(data == 0){
                          swal({
                          title: 'El correo ya existe en el sistema',
                          type: 'error',
                          })
                        } else {
                        swal({
                          title: 'Tu registro se realizo correctamente',
                          type: 'success',
                          showConfirmButton:false,
                          timer: 5000,
                        })
                        location.href='/user/datos_personal';
                        }
                      },
                   });
            }else{
              swal({
                title: 'EL correo tiene un formato invalido',
                type: 'error',
                })
              }//fin de else
      }); //--- fin de la funcion ---//

//------------- funcion para recuperar los datos y pintarlos en el modal de editar usuario ------------------//

$('.btn-edit').click(function(){          //-- Funcion para pintar el select del editar --//
  var idestado = $(this).data("idestado")
  var idrol = $(this).data("idrol")  
   $.ajax({
    headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      url: '/user/selects_entidad',
      dataType: 'html',
      success: function(data){
        var valores = JSON.parse(data);
        var estadosedit = "#estadosedit"

        $("#rol_usuarioedit").val(idrol);

        $.each(valores.entidad,function(key, registro) {
          var cadena = registro.entidad.toUpperCase();
          if(idestado == registro.id_entidad){
            $(estadosedit).append('<option selected value='+registro.id_entidad+'>'+ cadena +'</option>');
          }else{
            $(estadosedit).append('<option value='+registro.id_entidad+'>'+ cadena +'</option>');
          } //fin del else  
        });//fin del each 
      },
    }) 
}) //-- fin de la funcion --//
 
$('#show_modal_edit').on('show.bs.modal', function (event) {    //-- funcion para pintar model-edit --//
    var target = event.relatedTarget;
    var tr = $( target ).closest( 'tr' );
    var tds = tr.find( 'td' );
    var modal = $(this)
     id = modal.find('.modal-body #idm').val(tds.eq(0).text());
     nombre = modal.find('.modal-body #nombrem').val(tds.eq(1).text());
     puesto = modal.find('.modal-body #puestom').val(tds.eq(2).text());
     email = modal.find('.modal-body #emailm').val(tds.eq(3).text());
     subdireccion = modal.find('.modal-body #subdireccionm').val(tds.eq(5).text());
     telefono = modal.find('.modal-body #telefonom').val(tds.eq(5).text());
     red = modal.find('.modal-body #redm').val(tds.eq(7).text());
     unidad_laboral = modal.find('.modal-body #unidad_laboralm').val(tds.eq(8).text());
 });//--- fin de la funcion ---//
 
//------------------------------ funcion que hace el editar usuarios -----------------------------------///
       
     $('#usr_editar').click(function(){ 
      // console.log( $("#rol_usuarioedit").val(),'DANG' );

      if(email.val().substr(-14)=="@issste.gob.mx"){
        
         $.ajax({
             type:'post',
             url: '/personalUsuario/updateData',
             data: {
                '_token':$('input[name=_token').val(),
                'id':id.val(),
                'nombre':nombre.val(),
                'puesto':puesto.val(),
                'email':email.val(),
                'id_entidad':$("#estadosedit").val(),
                'estado':$("#estadosedit").val(),
                'subdireccion':subdireccion.val(),
                'telefono':telefono.val(),
                'rol_usuario':$("#rol_usuarioedit").val(),
                'red':red.val(),
                'unidad_laboral':unidad_laboral.val(),
             },
             success:function(data){
              swal({
                title: 'Tu registro se realizo correctamente',
                type: 'success',
                showConfirmButton:false,
                timer: 5000,
              })
              location.href='/user/datos_personal';
            },
         });
        }else{
          swal({
            title: 'EL correo tiene un formato invalido',
            type: 'error',
            })
          }
          //fin de else
      }); //--- fin de la funcion ---//

//------------------------------ funcion para dar de baja usuarios -----------------------------------///

$('#delete_usr').click(function(){ 
  Swal.fire({
    title: '¿Estás seguro de eliminar este usuario?',
    text: "¡No se pueden revertir los cambios!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Eliminar'
   }).then((result) => {
    if (result.value) {
      $.ajax({
        type:'post',
        url: '/personalUsuario/deletePersonal',
        data: {
           '_token':$('input[name=_token').val(),
           'id':id.val(),
        },
      success:function(data){
        swal({
          title: 'El usuario se elimino correctamente',
          type: 'success',
          showConfirmButton:false,
          timer: 5000,
        })
          location.href='/user/datos_personal';
       }, //fin de succes
    }); //fin de ajax 
   }//fin de if
  });
}); //--- fin de la funcion ---//


//-----------------------------------llenado select's de la modal nuevo usuario --------------------------------------------------//

$('#reg_nue').on ('click', function() {
  var id_entidad = 0, id_rol=0
  var nombreSelect = '#estado', nombreSelectR = '#rol_usuario'
           llenadoSelectEntidad(id_entidad,nombreSelect);
          //  llenadoSelectRol(id_rol, nombreSelectR);
});//--- fin de la funcion ---//

function llenadoSelectEntidad(id_entidad,nombreSelect){//funcion para llenar los select 
  $.ajax({
  headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    url: '/user/selects_entidad',
    dataType: 'html',
    success: function(data){
     var valores = JSON.parse(data);
      $.each(valores.entidad,function(key, registro) {
        var cadena = registro.entidad.toUpperCase();
        $(nombreSelect).append('<option value='+registro.id_entidad+'>'+ cadena +'</option>');
          if(id_entidad !=0 ){
              $(nombreSelect+' > option[value='+id_entidad+']').attr('selected', 'selected');
          }//fin de if    
      });//fin del each 
    },
  error: function(resp_success){ /*alerta('Alerta!','Error Modal');*/ }
  });
 }//--- fin de la funcion ---//

 function llenadoSelectRol(id_rol,nombreSelectR){//funcion para llenar los select 
  $.ajax({
  headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    url: '/user/selects_rol',
    dataType: 'html',
    success: function(data){
     var valores = JSON.parse(data);
      $.each(valores.rol,function(key, registro) {
        var cadena = registro.nombre_rol.toUpperCase();
        $(nombreSelectR).append('<option value='+registro.id+'>'+ cadena +'</option>');
          if(id_rol !=0 ){
              $(nombreSelectR+' > option[value='+id+']').attr('selected', 'selected');
          }//fin de if    
      });//fin del each 
    },
  error: function(resp_success){ /*alerta('Alerta!','Error Modal');*/ }
  });
 }//--- fin de la funcion ---//

 //----------------------------------- CAMBIO CONTRASEÑA USUARIO --------------------------------------------------//

 $('#show_modal_password_edit').on('show.bs.modal', function (event) { 
  var target = event.relatedTarget;
  var tr = $( target ).closest( 'tr' );
  var tds = tr.find( 'td' );
  var modal = $(this)
  id = modal.find('.modal-body #id').val(tds.eq(0).text());
});//--- fin de la funcion ---//

 $('#save_pass_button').click(function(){ 
  console.log("desde actualizar")
    if(document.getElementById("new_pass").value  == "" && document.getElementById("new_pass_confirm").value  == ""){
      swal({
            title: "Verifica",
            text: "Campos Requeridos vacios",
            type: "info",
            });
        return false;
      }else{
        if($('#new_pass').val() == $('#new_pass_confirm').val()){
          $.ajax({
            type:'POST',
            url: '/personalUsuario/updateDataPassword',
            data:{
              '_token':$('input[name=_token').val(),
              'id':id.val(),
              'new_pass':$('#new_pass').val(),
              'new_pass_confirm':$('#new_pass_confirm').val(),
          },
            success:function(data){
              console.log(data)
              swal({
                title: 'La contraseña se actualizo correctamente',
                type: 'success',
                showConfirmButton:false,
                timer: 5000,
              })
              location.href='/user/datos_personal';
            }
          });
        }else{
            swal ( "Error" ,  "Las contraseñas no coinciden" ,  "error" )
            } //fin del primer else 
        }//fin del else  
    }); //- fin de la funcion --//
