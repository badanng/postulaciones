 var bloque;
 var  $archivoInput = $('#archivo');
 var $bajaForm = $('#bajaForm');
var formData1 = new FormData();
var id_equipo;
var id_fuap;

  $('#tipo_baja').change(function(){ //select estado nuevo usuario

        tipo_baja = document.getElementById('tipo_baja').value;

        if(tipo_baja ==2 || tipo_baja==3 ){
          var bloque = '<div id="campos" class="col-md-6" style="text-align: left; display:block;"></div>';
          $('#div_archivo').show(bloque);
          
        }
        else{

           $('#div_archivo').hide(bloque);
        }

   });


$('.baja_equipo').click(function(){

    id_equipo = this.value;//recuperamos el id del equipo 


          
      $.ajax({
  headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/equipo/selects_tipo_baja',
        dataType: 'html',
        beforeSend: function () 
                {
                    //$("#id_rol").html('disabled', true);
                },

        success: function(data){
          var valores = JSON.parse(data);

        $.each(valores.tipo,function(key, registro) {
          //var cadena = registro.tipo.toUpperCase();
           $('#tipo_baja').append('<option value='+registro.cve+'>'+ registro.etiqueta +'</option>');

           
        }); 
        
    },


        error: function(resp_success){ alerta('Alerta!','Error Modal Editar Usuario 1');}
      });




});


$('#cerrar_modal_bajaEquipo').click(function (){
  
                   $('#tipo_baja')
                  .empty()
                  .append('<option  value="" selected="selected">Seleccione...</option>');
});





  $('.elimina_equipo').click(function(){


          var file = $("#archivo")[0].files[0];
          var reporte = $('input[name=reporte').val();
          var tipo_baja = $('#tipo_baja').val();
          //console.log(file);
          var formData = new FormData();
          formData.append("archivo_pdf", file);
          formData.append("id_equipo", id_equipo);
          formData.append("reporte", reporte);
          formData.append("tipo_baja", tipo_baja);

    
          var msj_error="";


      if(tipo_baja ==2 || tipo_baja==3){

         ( $('#archivo').get(0).files.length === 0) ? msj_error+='Olvidó ingresar el Archivo.<br />' : msj_error += "";
      }

      ( $('#reporte').get(0).value == "" ) ? msj_error+='Olvidó ingresar el Numero de Reporte.<br />' : msj_error += "";


        if( !msj_error == "" ){
            swal("Error",msj_error,"error");
            return false;
          }

            $.ajax({
                headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
              
                 url: $bajaForm.attr('action') + '?' + $bajaForm.serialize(),
                 method: $bajaForm.attr('method'),
                 data: formData,
                 processData: false,
                 contentType: false,
             success:function(data){
               swal("Bien", "Baja correcta", "success");
               location.reload();
                   $('#tipo_baja')
                  .empty()
                  .append('<option  value="" selected="selected">Seleccione...</option>');
              
             },

           });
  });



$('.cambia_resguardante').click(function(){

    id_fuap = this.value;
    var id_entidad = 0;
    var nombreSelect = '#estado_fuap';
    llenadoSelectEntidad(id_entidad,nombreSelect);


  });///////////////

$('.cambioUsuarioFuap').click(function(){
  var es= '#estado_fuap';
  var mun = '#municipio_fuap';
  var uni = '#unidad_fuap';
  limpiarSelect(es,mun,uni);

});

$('.cambiaUsuarioFuap').click(function(){
  
       $.ajax({
    
                  type:'post',
                  url: '/fuap/cambiaUsuarioFuap',
                  data:{
                  	'_token':$('input[name=_token').val(),
                  	'id_fuap':id_fuap,
                  	'ubicacion':$('#unidad_fuap').val(),
                  	'nombre':$('#nombre_fuap').val(),
                  	'puesto':$('#puesto_fuap').val(),
                  	'email':$('#email_fuap').val(),

            
                  },
                  success:function(data){

                    swal("Bien",data,"success");
                     location.reload();
                   
         
                  },
               });

});




   $('#estado_fuap').change(function(){ //select estado nuevo usuario

     $('#municipio_fuap')
    .empty()
    .append('<option  value="" selected="selected">Seleccione...</option>');

      $('#unidad')
      .empty()
      .append('<option  value="" selected="selected">Seleccione...</option>');

    var id_entidad = $(this).val();
    $.ajax({
        headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/user/selects_municipio',
        data:{
          'id_entidad':id_entidad
              },
        dataType: 'html',
        beforeSend: function () 
                {
                    //$("#id_rol").html('disabled', true);
                },

        success: function(data){
            var valores = JSON.parse(data);

          $.each(valores.municipios,function(key, muni) {
            
             $("#municipio_fuap").append('<option value='+muni.id+'>'+ muni.municipio +'</option>');
             
          });      
        },


        error: function(resp_success){ alerta('Alerta!','Error Modal Editar Usuario 2');}
      });


   });

        $('#municipio_fuap').change(function(){

         $('#unidad_fuap')
        .empty()
        .append('<option  value="" selected="selected">Seleccione...</option>');

    var id_municipio = $(this).val();
    $.ajax({
        headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/user/selects_unidad',
        data:{
          'id_municipio':id_municipio
              },
        dataType: 'html',
        beforeSend: function () 
                {
                    //$("#id_rol").html('disabled', true);
                },

        success: function(data){
            var valores = JSON.parse(data);

          $.each(valores.inmuebles,function(key, unidad) {
            
             $("#unidad_fuap").append('<option value='+unidad.id+'>'+ unidad.inmueble + " " + unidad.ubicacion  +'</option>');
             
          });      
        },


        error: function(resp_success){ alerta('Alerta!','Error Modal Editar Usuario 3');}
      });


   });














// $(document).ready(function(){
//  $("button").click(function() {
//  alert($(this).attr("id"))
//  });
//  });
//  
//  variables globales
  var id_usuario;
//  fin variables globaales  






//funcion para gregar un nuevo usuario *********************************************************************************************
$('#agregarUsuario').click(function(){ 
var msj_error="";
 //----------------------------------------nombre de la clase <strong class=text_nombrre
  ( $('#nombre').get(0).value == "" ) ?   $(".text_nombre").html("Olvidó ingresar el nombre").css(" #FA5858"): $(".text_nombre").html("");
 ( $('#puesto').get(0).value == "" ) ?   $(".text_puesto").html("Olvidó ingresar el puesto").css(" #FA5858"): $(".text_puesto").html("");
  ( $('#email').get(0).value == "" ) ?   $(".text_email").html("Olvidó ingresar el email").css(" #FA5858"): $(".text_email").html("");
    var regex = /[\w-\.]{2,}@issste.gob.mx/;
    if (!regex.test($('#email').val().trim())) msj_error+='El correo tiene un formato inválido.<br />';
 // ( $('#municipio').get(0).value == "" ) ?   $(".text_municipio").html("Olvidó Ingresar Municipio").css(" #FA5858"): $(".text_municipio").html("");
 // ( $('#estado').get(0).value == "" ) ?   $(".text_estado").html("Olvidó Ingresar Estado").css(" #FA5858"): $(".text_estado").html("");
  // ( $('#unidad').get(0).value == "" ) ?   $(".text_unidad").html("Olvidó Ingresar Unidad").css(" #FA5858"): $(".text_unidad").html("");
  ( $('#password').get(0).value == "" ) ?   $(".text_password").html("Olvidó ingresar password").css(" #FA5858"): $(".text_password").html("");
 

    if( !msj_error == "" ){
         swal("Error",msj_error,"error");

          return false;
        }



                $.ajax({
                  type:'post',
                  url: '/personalUsuario/registroUsuario',
                  data:{
                    '_token':$('input[name=_token').val(),
                    'nombre':$('input[name=nombre').val(),
                   'puesto':$('input[name=puesto').val(),
                    'email':$('input[name=email').val(),
                   'estado':$('#estado').val(),
                    // 'unidad':$('#unidad').val(),
                //    'municipio':$('#municipio').val(),                    
                    'password':$('input[name=password').val(),
                    //'ubicacion_usuario':$('#ubicacion_usuario').val(),
                  },
                  success:function(data){

                    swal("Bien",data,"success");
                    carga_pagina('contenedor_principal','/personalUsuario/datos_personal');
         
                  },
               });
  });//////////////////////////////////////////////////////////////////////////////////////////////////////////////////





    function limpiaCampos(){//funcion para limpiar los campos del model nuevo personal

    $("#nombre").val("");
    $("#apellidoPaterno").val("");
    $("#apellidoMaterno").val("");
    $("#email").val("");
    // $("#edad").val("");
    // $('#curp').val('');
    $('#rfc').val('');
     $('#password').val('');
    // $('#direccion').val('');
    // $('#telefono').val('');
    // $('#comentarios').val('');

    $(".text_nombre").html("");
    $(".text_apellidoPaterno").html("");
    $(".text_apellidoMaterno").html("");
    $(".text_email").html("");
    // $(".text_edad").html("");
    // $(".text_curp").html("");
    $(".text_rfc").html("");
    $(".text_password").html("");
    // $(".text_direccion").html("");
    // $(".text_telefono").html("");

    }///////////////////////////////////////////////////////////////////////////////////


//llenado de select de la modal nuevo usuario ************************************************************************

  $('#reg_nue').on ('click', function() {//funcion que ejecuta la funcion de limpia campos
    var id_entidad = 0;
    var nombreSelect = '#estado'
             llenadoSelectEntidad(id_entidad,nombreSelect);
            //limpiaCampos();
  });///////////////////////////////////////////////////////////////////////////////////////////////


  $("body").on("click", "#cerrar_modal", function(){ //funcion para limpiar los select 

    var es = '#estado';
    var mun = '#municipio';
    var uni = '#unidad';

    limpiarSelect(es,mun,uni);

  });//////////////////////////////////////////////////////

  $("body").on("click", "#cerrar_editar", function(){ //funcion para limpiar los select 
  
    var es = '#estados';
    var mun = '#municipios';
    var uni = '#unidades';
        limpiarSelect(es,mun,uni);
  });



function limpiarSelect(es,mun,uni){

    $(es)
    .empty()
    .append('<option  value="" selected="selected">Seleccione...</option>');

         $(mun)
    .empty()
    .append('<option  value="" selected="selected">Seleccione...</option>');


      $(uni)
      .empty()
      .append('<option  value="" selected="selected">Seleccione...</option>');


}





function llenadoSelectEntidad(id_entidad,nombreSelect){//funcion para llenar los select 


      $.ajax({
  headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/user/selects_entidad',
        dataType: 'html',
        beforeSend: function () 
                {
                    //$("#id_rol").html('disabled', true);
                },

        success: function(data){
          var valores = JSON.parse(data);

        $.each(valores.entidad,function(key, registro) {
          var cadena = registro.entidad.toUpperCase();
           $(nombreSelect).append('<option value='+registro.id_entidad+'>'+ cadena +'</option>');

           if(id_entidad !=0 ){

              $(nombreSelect+' > option[value='+id_entidad+']').attr('selected', 'selected');

           }
           
        }); 
        
    },


        error: function(resp_success){ alerta('Alerta!','Error Modal Editar Usuario 1');}
      });



}/////////////////////////////////////////////////////////////////////////////


   $('#estado').change(function(){ //select estado nuevo usuario

     $('#municipio')
    .empty()
    .append('<option  value="" selected="selected">Seleccione...</option>');

      $('#unidad')
      .empty()
      .append('<option  value="" selected="selected">Seleccione...</option>');

    var id_entidad = $(this).val();
    $.ajax({
        headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/user/selects_municipio',
        data:{
          'id_entidad':id_entidad
              },
        dataType: 'html',
        beforeSend: function () 
                {
                    //$("#id_rol").html('disabled', true);
                },

        success: function(data){
            var valores = JSON.parse(data);

          $.each(valores.municipios,function(key, muni) {
            
             $("#municipio").append('<option value='+muni.id+'>'+ muni.municipio +'</option>');
             
          });      
        },


        error: function(resp_success){ alerta('Alerta!','Error Modal Editar Usuario 2');}
      });


   });


      $('#estadosedit').change(function(){//select para modal editar estados edit

     $('#municipioedit')
    .empty()
    .append('<option  value="" selected="selected">Seleccione...</option>');

      $('#unidad')
      .empty()
      .append('<option  value="" selected="selected">Seleccione...</option>');

    var id_entidad = $(this).val();
    $.ajax({
        headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/user/selects_municipio',
        data:{
          'id_entidad':id_entidad
              },
        dataType: 'html',
        beforeSend: function () 
                {
                    //$("#id_rol").html('disabled', true);
                },

        success: function(data){
            var valores = JSON.parse(data);

          $.each(valores.municipios,function(key, muni) {
            
             $("#municipioedit").append('<option value='+muni.id+'>'+ muni.municipio +'</option>');
             
          });      
        },


        error: function(resp_success){ alerta('Alerta!','Error Modal Editar Usuario 2');}
      });


   });




       $('#municipioedit').change(function(){//select modal editar municipio

         $('#unidadedit')
        .empty()
        .append('<option  value="" selected="selected">Seleccione...</option>');

    var id_municipio = $(this).val();
    $.ajax({
        headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/user/selects_unidad',
        data:{
          'id_municipio':id_municipio
              },
        dataType: 'html',
        beforeSend: function () 
                {
                    //$("#id_rol").html('disabled', true);
                },

        success: function(data){
            var valores = JSON.parse(data);

          $.each(valores.inmuebles,function(key, unidad) {
            
             $("#unidadedit").append('<option value='+unidad.id+'>'+ unidad.inmueble + " " + unidad.ubicacion  +'</option>');
             
          });      
        },


        error: function(resp_success){ alerta('Alerta!','Error Modal Editar Usuario 3');}
      });


   });

       $('#municipio').change(function(){

         $('#unidad')
        .empty()
        .append('<option  value="" selected="selected">Seleccione...</option>');

    var id_municipio = $(this).val();
    $.ajax({
        headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/user/selects_unidad',
        data:{
          'id_municipio':id_municipio
              },
        dataType: 'html',
        beforeSend: function () 
                {
                    //$("#id_rol").html('disabled', true);
                },

        success: function(data){
            var valores = JSON.parse(data);

          $.each(valores.inmuebles,function(key, unidad) {
            
             $("#unidad").append('<option value='+unidad.id+'>'+ unidad.inmueble + " " + unidad.ubicacion  +'</option>');
             
          });      
        },


        error: function(resp_success){ alerta('Alerta!','Error Modal Editar Usuario 3');}
      });


   });
//ternmina llenado de select de la modal nuevo usuario ************************************************************************


$(function () {//funcion para cambiar la imagen de perfil
    var $avatarImage, $avatarInput, $avatarForm;

    $avatarImage = $('#avatarImage');
    $avatarImageP=$('#avatarImageP');
    $avatarInput = $('#avatarInput');
    $avatarForm = $('#avatarForm');

    $avatarImage.on('click', function () {
        $avatarInput.click();
    });

    $avatarInput.on('change', function () {
         var formData = new FormData();
    formData.append('avatar', $avatarInput[0].files[0]);//subir archivo 1
     $.ajax({
        url: $avatarForm.attr('action') + '?' + $avatarForm.serialize(),//traes la direccion del action que declaras en el formulario y concatenas la imagen
        method: $avatarForm.attr('method'),//llamas el metodo que declaraste en el formulario
        data: formData,
        processData: false,
        contentType: false
    }).done(function (data) {
        if (data.success)
            $avatarImage.attr('src', data.path);//id del div donde se recraga la imagen subida
            $avatarImageP.attr('src', data.path);
            swal("BIEN",'la imagen de perfil ha sido actualizada correctamente','success');

    }).fail(function () {
        swal("Error",'el archivo que intento subir no es una imagen','error');
    });
    });
});///////////////////////////////////////////////////////////////////////////////////////


//-- funcion para recuperar los datos y pintarlos en el modal de editar usuario --//
    $('#editt').on('show.bs.modal', function (event) {
    
    var target = event.relatedTarget;
    var tr = $( target ).closest( 'tr' );
    var tds = tr.find( 'td' );

    var dtTable = $('#user_table').DataTable();
    var numeroFila = tr.index();

    var id_entid = dtTable.cells({ row: numeroFila, column: 4 }).data()[0];
    id_usuario = dtTable.cells({ row: numeroFila, column: 0 }).data()[0];
    //llenadoSelectEntidadEdit(id_entid);
    var nombreSelect = '#estadosedit';
    llenadoSelectEntidad(id_entid,nombreSelect);

       var modal = $(this)
         nombre = modal.find('.modal-body #nombre').val(tds.eq(1).text());
         puesto = modal.find('.modal-body #puesto').val(tds.eq(2).text());
         ubicacion = modal.find('.modal-body #ubicacion').val(tds.eq(4).text());
         email = modal.find('.modal-body #email').val(tds.eq(3).text());
         unidad_administrativa = modal.find('.modal-body #unidad_administrativa').val(tds.eq(5).text());
     });
//--- fin de la funcion ---//
    
// //---  funcion que hace el editar usuarios ---///   
//      $('#usr_editar').click(function(){ 
//          $.ajax({
//            type:'POST',
//            url: '/personalUsuario/updateDataPersonal',
//            data:{
//              '_token':$('input[name=_token').val(),
//              // 'id':id,
//              'nombre':nombre.val(),
//              'puesto':nombre.val(),
//              'estado':ubicacion.val(),
//              'municipio':puesto.val(),
//              'unidad':email.val(),
//              'email': unidad_administrativa.val(),
//            },
//            success:function(data){
//               swal("Su registro se actualizo satisfactoriamente","success")
//               .then((value) => {
//                 icon: "success";
//               });
//               recarga_pagina('contenedor_principal','/personalUsuario/datos_personal');
//            },
//          });
//       });
// //--- fin de la funcion ---//




// //funcion para dar de baja a una persona
//  $('#usr_eliminar').click(function(){ 
//       Swal.fire({
//         title: 'Eliminar?',
//         text: "Desea eliminar la persona!",
//         type: 'warning',
//         showCancelButton: true,
//         confirmButtonColor: '#5d0923',
//         cancelButtonColor: '#d33',
//         confirmButtonText: 'Si Eliminar!'
//       }).then((result) => {
//         if (result.value) {
//              id = button.data('id')
//                $.ajax({
//                  type:'DELETE',
//                  url: '/personalUsuario/deletePersonal' + 'id',
//                  data:{
//                    '_token':$('input[name=_token').val(),
//                    'id':id,
//                  },

//              success:function(data){
//                Swal.fire(
//                'Borrado!',
//                'La persona fue eliminada.',
//                'success'
//                ),
//                recarga_pagina('contenedor_principal','/personalUsuario/datos_personal');

//              },

//            });
//         }
//     })
//  });//--- fin de la funcion ---//




       $('#pasaUsuario').on('show.bs.modal', function (event) {
          llenadoSelect();
       button = $(event.relatedTarget)
       nombre = button.data('nombre')
       apellidoPaterno = button.data('apellidop')
       apellidoMaterno = button.data('apellidom')
             var modal = $(this)
        nombre = modal.find('.modal-body #nombre').val(nombre +" "+ apellidoPaterno + " "+ apellidoMaterno);
     
        modal.find('.modal-body #id').val(id);
     })//--- fin de la funcion ---//





$('#usr_add').click(function(){ //////////funcion para dar de alta  a un usuario

                 var msj_error="";

  if( $('#email').get(0).value == "" )  msj_error+='Olvidó ingresar Correo.<br />';
    var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
    if (!regex.test($('#email').val().trim())) msj_error+='El correo tiene un formato inválido.<br />';
  if(($('#password').get(0).value != "")&&($('#password2').get(0).value != "")){
      if( $('#password').get(0).value == "" ) msj_error+='Olvidó ingresar Contraseña.<br />';
      if( $('#password2').get(0).value == "") msj_error+='Olvidó ingresar Confimación de contraseña.<br />';
      if( $('#password').get(0).value != $('#password2').get(0).value) msj_error+='Las contraseñas no coinciden.<br />';
       }
       if( $('#rol_usuario').get(0).value == "" )  msj_error+='Olvidó seleccionar un rol.<br />';
       if( $('#ubicacion_usuario').get(0).value == "" )  msj_error+='Olvidó seleccionar una ubicacion.<br />';
  if( !msj_error == "" ){
    swal("Error",msj_error,"error");

    //alerta('Faltan datos', msj_error);
    return false;
  }



          id = button.data('id')
         $.ajax({
           type:'post',
           url: '/personalUsuario/altaUsuario',
           data:{
             '_token':$('input[name=_token').val(),
             'id_personal':id,
              'email':$('input[name=email').val(),
              'password':$('input[name=password').val(),
              'rol_usuario': $('#rol_usuario').val(),
              'ubicacion_usuario':$('#ubicacion_usuario').val(),

           },
                success:function(data){

                    swal("Bien",'la persona fue creada correctamente',"success");
   
        
           },
        });
      });//--- fin de la funcion ---//



  function recarga_pagina(div_contenedor,ruta,parametros){ //funcion que permite el llamado de las rutas a un div sin recargar

    $('#'+div_contenedor).load(ruta,parametros, function(){
   
    });
  }


//comente esto para lo se users
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// $("body").on("click", "#guarda_perfil", function(){
//   var msj_error="";
//   if( $('#nombre_perfil').get(0).value == "" )  msj_error+='Olvidó ingresar Nombre.<br />';
//   if( $('#apellido_paterno_perfil').get(0).value == "")  msj_error+='Olvidó ingresar el apellido paterno.<br />';
//   if( $('#apellido_materno_perfil').get(0).value == "")  msj_error+='Olvidó ingresar el apellido materno.<br />';


//   if( !msj_error == "" ){
//     swal("Error",msj_error,'error');
//     //alerta('Faltan datos', msj_error);
//     return false;
//   }

//     $.ajax({
//       headers: {
//             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//       },
//       url: app_url + 'usuarios/editar_perfil',
//       type: 'POST',
//      data:{
//              '_token':$('input[name=_token').val(),
//              'id_perfil':id,
//              'nombre_perfil':$('input[name=nombre_perfil').val(),
//              'apellido_paterno_perfil':$('input[name=apellido_paterno_perfil').val(),
//              'apellido_materno_perfil':$('input[name=apellido_materno_perfil').val(),

//            },
//       dataType: 'json',
//       success: function(resp_success){
//         if (resp_success['resp'] == true) {
//           $('#name_top').html(resp_success['new_name']);
//           alerta('Anuncio!', resp_success['mensaje']);
//         }else{
//           alerta('Alerta!', resp_success['mensaje']);
//         }
//       },
//       error: function(respuesta){ alerta('Alerta!','Error de conectividad de red USR-02');}
//     });

// });



//******************************************************************************************************

// $("body").on("click", "#guarda_perfil", function(){
//   var msj_error="";
//   if( $('#nombre_perfil').get(0).value == "" )  msj_error+='Olvidó ingresar Nombre.<br />';
//   if( $('#apellido_paterno_perfil').get(0).value == "")  msj_error+='Olvidó ingresar el apellido paterno.<br />';
//   //if( $('#apellido_materno_perfil').get(0).value == "")  msj_error+='Olvidó ingresar el apellido materno.<br />';


//   if( !msj_error == "" ){
//     swal("Error",msj_error,'error');
//     //alerta('Faltan datos', msj_error);
//     return false;
//   }

//     $.ajax({
//       headers: {
//             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//       },
//       // url: app_url + 'usuarios/editar_perfil',
//       url: '/personalUsuario/updateData',
//       type: 'POST',
//       data:{
//              '_token':$('input[name=_token').val(),
//              'id_perfil':id,
//              'nombre_perfil':$('input[name=nombre_perfil').val(),
//              'apellido_paterno_perfil':$('input[name=apellido_paterno_perfil').val(),
//              'apellido_materno_perfil':$('input[name=apellido_materno_perfil').val(),

//            },
//       dataType: 'json',
//       success: function(resp_success){
//         if (resp_success['resp'] == true) {
//           $('#name_top').html(resp_success['new_name']);
//           alerta('Anuncio!', resp_success['mensaje']);
//         }else{
//           alerta('Alerta!', resp_success['mensaje']);
//         }
//       },
//       error: function(respuesta){ alerta('Alerta!','Error de conectividad de red USR-02');}
//     });

// });

  $('#guarda_perfil').click(function(){ 

        ( $('#passwordd').get(0).value == "" ) ?   $(".txt_passworda").html("Campo Requerido").css(" #FA5858"): $(".txt_passworda").html("");
        ( $('#newpassword').get(0).value == "" ) ?   $(".txt_passwordd").html("Campo Requerido").css(" #FA5858"): $(".txt_passwordd").html("");
        ( $('#newpasswordf').get(0).value == "" ) ?   $(".txt_passwordd").html("Campo Requerido").css(" #FA5858"): $(".txt_passwordd").html("");
 
          if($('#passwordd').val() != '' && $('#newpassword').val() != '' && $('#newpasswordf').val() != ''){

            if($('#newpassword').val() == $('#newpasswordf').val()){

            $.ajax({
                type:'POST',
                url: '/user/updateDataPersonal',
                data:{
                  '_token':$('input[name=_token').val(),

                  'passwordAct':$('#passwordd').val(),
                  'passwordnew':$('#newpassword').val(),
                  'passwordneww':$('#newpassword').val(),
            },
            success:function(data){
                swal("Actualizada", "Contraseña Actualizada", "success")
                .then((value) => {
                    icon: "success";
                });
                   recarga_pagina('contenedor_principal','/user/datos_personal');
           },
         });
      }
      else{
            swal ( "Error" ,  "Las contraseñas no coinciden" ,  "error" )
      }
    }else{
      swal({
            title: "Verifica",
            text: "Campos Requeridos",
            type: "info",
          });
    }
});


  //---  funcion que hace el editar usuarios ---///   
     $('#usr_editar').click(function(){ 
         $.ajax({
             type:'post',
             url: '/personalUsuario/updateData',
             data:{
               '_token':$('input[name=_token').val(),
               'id':id_usuario,
               'nombre':nombre.val(),
               'puesto':puesto.val(),

               'estado':$('#estado').val(),
               'unidad':$('#unidad').val(),
               'municipio':$('#municipio').val(), 
               
               'email': email.val(),
           },
           success:function(data){
              swal("Su registro se actualizo satisfactoriamente","success")
              .then((value) => {
                icon: "success";
              });
              recarga_pagina('contenedor_principal','/user/datos_personal');
           },
         });
      });
//--- fin de la funcion ---//

//funcion para dar de baja a una persona
 $('#usr_eliminar').click(function(){ 
               $.ajax({
                 type:'DELETE',
                 url: '/personalUsuario/deletePersonal',
                 data:{
                   '_token':$('input[name=_token').val(),
                   'id':id_usuario,
                 },

             success:function(data){
               Swal.fire(
               'Borrado!',
               'La persona fue eliminada.',
               'success'
               ),
               recarga_pagina('contenedor_principal','/user/datos_personal');

             },
    });
 });
 //--- fin de la funcion ---//





