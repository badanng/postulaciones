<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContratacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contrataciones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_persona')->nullable();
            $table->string('numeroDePlaza')->nullable();
            $table->string('numeroDeEmpleado')->nullable();
            $table->integer('unidadMedicaAdscrita')->nullable();            
            $table->date('fechaInicioContrato')->nullable();
            $table->date('fechaFinContrato')->nullable();
            $table->string('motivoBaja')->nullable();
            $table->integer('contrato')->nullable();
            $table->boolean('status')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contrataciones');
    }
}
