<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatosPostulado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datos_postulados', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('folio');
            $table->string('curp',18)->unique();  
            $table->string('rfc',13);      
            $table->string('aPaterno', 100);
            $table->string('aMaterno', 100);
            $table->string('nombre', 100);
            $table->date('fechaNacimiento');
            $table->string('nacionalidad', 50);
            $table->string('condicion', 50)->nullable();
            $table->string('especificar', 50)->nullable();            
            $table->integer('entidadMunicipio');
            $table->string('municipio');
            $table->string('nivelTecnico',50)->nullable();
            // $table->string('licenciatura', 100)->nullable();
            $table->string('noCedula')->nullable();
            $table->string('especialidad')->nullable();
            $table->string('otraEspecialidad')->nullable();
            $table->string('telefono')->nullable();
            $table->string('celularUno');
            $table->string('celularDos')->nullable();
            $table->string('correo')->unique();
            $table->string('rfcFile')->nullable();
            $table->string('ineFile')->nullable();    
            $table->string('tituloFile')->nullable();
            $table->string('cedulaFile')->nullable();

            $table->string('certificadoFile')->nullable();
            $table->string('diplomaFile')->nullable(); 

            $table->string('direccion',150);
            $table->integer('codigoPostal')->unsigned();
            $table->boolean('laboraDependencia');
            
            $table->boolean('terminosDeLabora');
            $table->boolean('manifiesto');
            $table->boolean('disponibilidadViajar');
            $table->boolean('seleccionado')->default(false);
            $table->boolean('estadoPostulado')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::dropIfExists('datos_postulados');
    }
}
