<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CatEntidadUngrup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('cat_entidad_ungrup', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->integer('id_entidad')->nullable();
            $table->string('entidad')->nullable();
            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_entidad_ungrup');
    }
}
