<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToCatMunicipioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cat_municipio', function (Blueprint $table) {
            $table->renameColumn('id_municipio', 'cve_municipio');            
            $table->boolean('status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cat_municipio', function (Blueprint $table) {
            $table->renameColumn('cve_municipio', 'id_municipio');
            $table->dropColumn('status');
        });
    }
}
