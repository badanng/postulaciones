<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
 
class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
         $table->bigIncrements('id')->unsigned();
         $table->string('nombre', 100);
         $table->string('puesto', 100);
         $table->string('email');//;->unique();
         $table->timestamp('email_verified_at')->nullable();
         $table->string('password');
         $table->tinyinteger('estatus')->default(1);
         $table->string('avatar')->default('avatar.png');
         $table->integer('id_rol')->unsigned();
         $table->string('estado')->nullable();
         $table->string('id_entidad')->nullable();
         $table->string('subdireccion')->nullable();
         $table->string('telefono')->nullable();
         $table->string('red')->nullable();
         $table->string('unidad_laboral', 100)->nullable();
         $table->rememberToken();
         $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
