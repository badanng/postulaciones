<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnidadesActivasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unidades_activas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_establecimiento');
            $table->integer('zona_metropolitana');
            $table->integer('estatus');
            $table->string('clave_adscripcion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unidades_activas');
    }
}
