<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\DatosUsuariosModel;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(DatosUsuariosModel::class, function (Faker $faker) {


    return [
        'curp' => $faker->randomNumber($nbDigits = 13, $strict = true),
        'rfc' => $faker->randomNumber($nbDigits = 13, $strict = true),
        'aPaterno' => $faker->word,
        'aMaterno' => $faker->word,
        'nombre' => $faker->name,
        'fechaNacimiento' => '1990-01-01',
        'nacionalidad' => $faker->word,
        'hospitalDelegacion' => $faker->word,
        'entidadMunicipio' => $faker->word,
        'nivelTecnico' => $faker->word,
        'noCedula' => $faker->word,
        'otraEspecialidad' => $faker->word,
        'tituloFile' => $faker->word,
        'cedulaFile' => $faker->word,
        'rfcFile' => $faker->word,
        'ineFile' => $faker->word,
        'correo' => $faker->email,
        'correoDos' => $faker->email,
        'direccion' => $faker->word,
        'codigoPostal' => $faker->postcode,
        'laboraDependencia' => $faker->word,
        'manifiesto' => true,
        'disponibilidadViajar' => $faker->boolean($chanceOfGettingTrue = 2),
        

    ];
});
