<?php

use Keboola\Csv\CsvFile;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Log;
use App\Models\EstablecimientoSalud;

class EstablecimientosSaludSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $csv= new CsvFile(base_path().'/database/seeds/csv/establecimientos_salud.csv',',');
        foreach($csv AS $row) {
            $establecimiento = new EstablecimientoSalud;
            
            Log::error('Info log EstablecimientoSalud'. $row[0]);
            $establecimiento->create([
                'clues'=>$row[0],
                'entidad_id'=> (int)$row[1],                    
                'municipio_id'=>(int)$row[2],
                'localidad_id'=>(int)$row[3],
                'jurisdiccion_id'=>(int)$row[4],
                'institucion_id'=>(int)$row[5],
                'tipo_establecimiento_id'=>(int)$row[6],
                'tipologia_id'=>(int)$row[7],
                'consultoriosmed_gral'=>(int)$row[8],
                'consultoriosareas'=>(int)$row[9],
                'camas_hosp'=>(int)$row[10],
                'camas_areas'=>(int)$row[11],
                'nombre_unidad'=>$row[12],
                'vialidad'=>$row[13],
                'numero_exterior'=>$row[14],
                'numero_interior'=>$row[15],
                'asentamiento'=>$row[16],
                'observaciones_de_la_direccion'=>$row[17],
                'codigo_postal'=>$row[18],
                'status_id'=>(int)$row[19],
                'rfc_del_establecimiento'=>$row[20],
                'id_clave_presupuestal'=>(int)$row[21],                        
                'nivelatencion_id'=>(int)$row[22],
                'estratounidad_id'=>(int)$row[23],
                'lat'=> $row[24],
                'lon'=> $row[25]
            ]);            
        }        

    }
}
