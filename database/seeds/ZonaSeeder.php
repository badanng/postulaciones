<?php

use Illuminate\Database\Seeder;
use Keboola\Csv\CsvFile;
use App\Models\Zona;

class ZonaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    DB::table('zonas')->insert(
       array(
       'nombre_zona'=>'AREAS CENTRALES',
       'id_ubicacion'=>1,
     ) );

       DB::table('zonas')->insert(
       array(
       'nombre_zona'=>'DESCONCENTRADO',
       'id_ubicacion'=>1,
     ) );

       DB::table('zonas')->insert(
       array(
       'nombre_zona'=>'REGIONALES',
       'id_ubicacion'=>1,
     ) );

       DB::table('zonas')->insert(
       array(
       'nombre_zona'=>'H.R.',
       'id_ubicacion'=>1,
     ) );
       
        DB::table('zonas')->insert(
       array(
       'nombre_zona'=>'DELEGACIONES',
       'id_ubicacion'=>1,
     ) );
    }
    }

