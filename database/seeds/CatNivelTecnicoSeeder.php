<?php

use Illuminate\Database\Seeder;
use Keboola\Csv\CsvFile;
use App\Models\CatNivelTecnico;
use Illuminate\Support\Facades\Log;

class CatNivelTecnicoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv= new CsvFile(base_path().'/database/seeds/csv/cat_nivel_tecnico.csv', ',');
        foreach($csv AS $row) {
          $nivel = new CatNivelTecnico;
        Log::error('Info log test'. $row[0]);
          $nivel->create([
            'detalle'=> $row[0]
          ]);
        }
    }
}
