<?php

use App\Models\CatMunicipio;
use Keboola\Csv\CsvFile;
use Illuminate\Database\Seeder;

class CatMunicipioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $csv= new CsvFile(base_path().'/database/seeds/csv/cat_municipio_cmando.csv', ';');
        foreach($csv AS $row) {
    	   $municipio= new CatMunicipio;
           $municipio->create([
       		'cve_municipio'=> $row[0],
			'municipio'=> trim(preg_replace('/[\x00-\x1f\x7f\xa0]/u', '', $row[1])),
      'id_entidad'=> $row[2],
      'status'=> $row[3]
       	  ]);
        }
    }
}
