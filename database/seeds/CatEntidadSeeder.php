<?php

use Illuminate\Database\Seeder;
use Keboola\Csv\CsvFile;
use App\Models\Entidades;
use Illuminate\Support\Facades\Log;

class CatEntidadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
       public function run()
    {
      $csv= new CsvFile(base_path().'/database/seeds/csv/cat_entidad.csv', ',');
        foreach($csv AS $row) {
          $asentamiento = new Entidades;
        Log::error('Info log test'. $row[0]);
          $asentamiento->create([
            'id_entidad'=> (int)$row[0],
            'entidad'=> $row[1],
            'clave'=> $row[2],
            'clave_elector'=> $row[3],
            'lat'=> (int)$row[4],
            'lon'=> (int)$row[5]
          ]);
        }
    }
}
