<?php

use Illuminate\Database\Seeder;
use Keboola\Csv\CsvFile;
use App\Models\CatBajas;
use Illuminate\Support\Facades\Log;

class CatBajasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv= new CsvFile(base_path().'/database/seeds/csv/cat_bajas.csv', ',');
        foreach($csv AS $row) {
          $baja = new CatBajas;
        Log::error('Info log test'. $row[0]);
          $baja->create([
            'nombre'=> $row[0]
          ]);
        }        
    }
}
