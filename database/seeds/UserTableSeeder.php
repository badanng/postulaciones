<?php

use Illuminate\Database\Seeder;
use Keboola\Csv\CsvFile;
use App\Models\User;

class UserTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){

      DB::table('users')->insert(
        array(
          'id'=>1,
          'nombre'=>'Administrador',
          'puesto'=>'Admin Sistema',
          'email'=>'root@issste.gob.mx',
          'email_verified_at'=>'2019-07-02 03:50:18',
          'password' => '$2y$10$.FZgGGzG6EdvTCAbVbknW.Yf.amT3dapMthFBJYaBA24gJzbOHfrm',
          'estatus'=>1,
          'id_rol'=>1,
          'estado'=>'Ciudad de México',
          'id_entidad'=>'32',
          'subdireccion'=>'SCP',
          'telefono'=>'5513568979',
          'red'=>'35227',
          'unidad_laboral'=>'Buenavista',
          'remember_token'=>'',
          'created_at'=>'2019-07-02 03:50:18',
          'updated_at'=>'2019-07-02 03:50:18',
        )
      );
       $csv= new CsvFile(base_path().'/database/seeds/csv/usuarios2.csv', ',');
        foreach($csv AS $row) {
          $Users = new User;
          $Users->create([
            'nombre'=> $row[2],
            'puesto'=> $row[6],
            'email'=> $row[3],
            'password'=>bcrypt('12345678'),
            'estatus'=>1,
            'id_rol'=>1,
            'estado'=> $row[0],
            'id_entidad'=>$row[7],
            'subdireccion'=> $row[1],
            'telefono'=> $row[4],
            'red'=> $row[5]
          ]);
        }

    }
}
