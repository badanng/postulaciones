<?php

use Illuminate\Database\Seeder;
use Keboola\Csv\CsvFile;
use App\Models\UnidadesActivas;

class UnidadesActivasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
    	$csv= new CsvFile(base_path().'/database/seeds/csv/unidadesActivas.csv', ',');
        foreach($csv AS $row) {
          $unidades = new UnidadesActivas;
          $unidades->create([
            'id_establecimiento'=> $row[0],
            'zona_metropolitana'=> $row[1],
            'estatus'=> $row[2],
            'clave_adscripcion'=> $row[3]
          ]);
        }

    }
    
}
