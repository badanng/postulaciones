<?php

use App\Models\CatMaster;
use Keboola\Csv\CsvFile;
use Illuminate\Database\Seeder;

class CatMasterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $csv= new CsvFile(base_path().'/database/seeds/csv/cat_master.csv', ',');
        foreach($csv AS $row) {
    	   $master= new CatMaster;
           $master->create([
            'parent_id'=>(int)$row[0],
            'catalogo'=>$row[1],
            'etiqueta'=>$row[2],
            'cve'=>$row[3],
            'activo'=>(int)$row[4],
            'orden'=>(int)$row[5],
            'valor'=>$row[6]
       	  ]);
        }
    }
}
