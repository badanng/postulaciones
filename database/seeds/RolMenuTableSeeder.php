<?php

use Illuminate\Database\Seeder;

class RolMenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('rol_menu')->insert(
       array(
       'id'=>1,
       'id_rol'=>1,
       'id_menu'=>1,
       'created_at'=>'2019-07-02 03:50:18',
       'updated_at'=>'2019-07-02 03:50:18',
     ) );

       DB::table('rol_menu')->insert(
       array(
       'id'=>2,
       'id_rol'=>1,
       'id_menu'=>2,
       'created_at'=>'2019-07-02 03:50:18',
       'updated_at'=>'2019-07-02 03:50:18',
     ) );
       DB::table('rol_menu')->insert(
       array(
       'id'=>3,
       'id_rol'=>1,
       'id_menu'=>3,
       'created_at'=>'2019-07-02 03:50:18',
       'updated_at'=>'2019-07-02 03:50:18',
     ) );
       DB::table('rol_menu')->insert(
       array(
       'id'=>4,
       'id_rol'=>1,
       'id_menu'=>4,
       'created_at'=>'2019-07-02 03:50:18',
       'updated_at'=>'2019-07-02 03:50:18',
     ) );
       DB::table('rol_menu')->insert(
       array(
       'id'=>5,
       'id_rol'=>1,
       'id_menu'=>5,
       'created_at'=>'2019-07-02 03:50:18',
       'updated_at'=>'2019-07-02 03:50:18',
     ) );
       DB::table('rol_menu')->insert(
       array(
       'id'=>6,
       'id_rol'=>1,
       'id_menu'=>6,
       'created_at'=>'2019-07-02 03:50:18',
       'updated_at'=>'2019-07-02 03:50:18',
     ) );
       DB::table('rol_menu')->insert(
       array(
       'id'=>7,
       'id_rol'=>1,
       'id_menu'=>7,
       'created_at'=>'2019-07-02 03:50:18',
       'updated_at'=>'2019-07-02 03:50:18',
     ) );
       DB::table('rol_menu')->insert(
       array(
       'id'=>8,
       'id_rol'=>1,
       'id_menu'=>8,
       'created_at'=>'2019-07-02 03:50:18',
       'updated_at'=>'2019-07-02 03:50:18',
     ) );
      DB::table('rol_menu')->insert(
       array(
       'id'=>9,
       'id_rol'=>1,
       'id_menu'=>9,
       'created_at'=>'2019-07-02 03:50:18',
       'updated_at'=>'2019-07-02 03:50:18',
     ) );
     //  DB::table('rol_menu')->insert(
     //   array(
     //   'id'=>10,
     //   'id_rol'=>1,
     //   'id_menu'=>10,
     //   'created_at'=>'2019-07-02 03:50:18',
     //   'updated_at'=>'2019-07-02 03:50:18',
     // ) );

    }
}
