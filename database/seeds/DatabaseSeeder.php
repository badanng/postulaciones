<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
 
        $this->call(CatMunicipioSeeder::class);
        
        $this->call(CatEntidadSeeder::class);
          
        $this->call(UserTableSeeder::class);
        $this->call(RolMenuTableSeeder::class);
        $this->call(MenuTableSeeder::class);

        $this->call(RolTableSeeder::class);

        $this->call(CatNivelTecnicoSeeder::class); 
        
        $this->call(UnidadesActivasSeeder::class);
        $this->call(EstablecimientosSaludSeeder::class);
        $this->call(CatMasterSeeder::class);

        $this->call(CatBajasSeeder::class);
       
    }
}
