<?php

use Illuminate\Database\Seeder;

class RolTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rol')->insert(
            array(
       
            // 'name'=>'root',
            'rol'=>'root',
            'nombre_rol'=>'administrador',
            'descripcion'=>'rol para el usuario que tiene acceso a todo el sistema'
          ) );


        DB::table('rol')->insert(
            array(                  
            'rol'=>'reclutador',
            'nombre_rol'=>'RECLUTADOR',
            'descripcion'=>'rol para el usuario Admin',  
          ) );

          DB::table('rol')->insert(
            array(                  
            'rol'=>'admin-entidad',
            'nombre_rol'=>'JEFE DE ENTIDAD',
            'descripcion'=>'Jefe de entidad',  
          ) );  
          
          DB::table('rol')->insert(
            array(                  
            'rol'=>'contratacion',
            'nombre_rol'=>'Admin Contratacion',
            'descripcion'=>'Adminstrador Contratacion',  
          ) );            

    }
}
