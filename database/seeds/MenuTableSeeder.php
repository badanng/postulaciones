<?php

use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
 {


      DB::table('menu')->insert(
        array(
          'id'=>1,
          'etiqueta'=>'Mi Perfil',
          'nombre_menu'=>'Framedev',
          'prioridad' => 1,
          'padre'=>0,
          'url'=>'user/perfil',
          'clase'=>'kt-menu__link-icon flaticon2-user',
          'status'=>1,
          'created_at'=>'2019-07-02 03:50:18',
          'updated_at'=>'2019-07-02 03:50:18',
        ) 
      );

      DB::table('menu')->insert(
        array(
          'id'=>2,
          'etiqueta'=>'Control de Usuarios',
          'nombre_menu'=>'Framedev',
          'prioridad' => 1,
          'padre'=>0,
          'url'=>'user/datos_personal',
          'clase'=>'kt-menu__link-icon flaticon-user-settings',
          'status'=>0,
          'created_at'=>'2019-07-02 03:50:18',
          'updated_at'=>'2019-07-02 03:50:18',
        ) 
      );

      DB::table('menu')->insert(
        array(
          'id'=>8,
          'etiqueta'=>'Postulaciones',
          'nombre_menu'=>'Framedev',
          'prioridad' => 1,
          'padre'=>0,
          'url'=>'/postulaciones',
          'clase'=>'kt-menu__link-icon flaticon-interface-9',
          'status'=>0,
          'created_at'=>'2019-07-02 03:50:18',
          'updated_at'=>'2019-07-02 03:50:18',
        ) 
      );



      // DB::table('menu')->insert(
      //   array(
      //     'id'=>10,
      //     'etiqueta'=>'Información Covid',
      //     'nombre_menu'=>'Framedev',
      //     'prioridad' => 10,
      //     'padre'=>0,
      //     'url'=>'http://www.issste.gob.mx/intranet/informacion2020/index.html',
      //     'clase'=>'kt-menu__link-icon flaticon-globe',
      //     'status'=>1,
      //     'created_at'=>'2019-07-02 03:50:18',
      //     'updated_at'=>'2019-07-02 03:50:18',
      //   )
      // );


    }
}
