<?php

///// CONFIGURACION DE RUTAS CON LOGIN
Route::get('/', function () {
    if(Auth::check()){
        if(Auth::user()->id_rol==4){
            return redirect("/contratacion");
        }else{
            return redirect("/home");
        }
    }else{
        return view('auth/login');
        // return view('/');
    }
    
});   // ----> VISTA INICIAL

///rutas para acceso
Auth::routes();



Route::group(['middleware' => 'reglas'], function () {
    Route::group(['middleware' => 'auth'], function () {

        Route::get('/sessiondestroy', 'SessionHandler@destroy');//ruta para destruir la sesión
        Route::get('/home', 'HomeController@index')->name('home');
        // Route::view('/contratacion', 'Registro.contratacion');
        Route::get('/contratacion', 'HomeController@index')->name('contratacion');
        Route::post('/guardar_contratacion', 'RegistroController@guardarContratacion')->name('guardarContratacion');
        // Route::get('/obtener_contratacion/{id}', 'RegistroController@obtenerContratacion')->name('obtenerContratacion');
        Route::post('/subir_imagen','ImageUploadController@store');// ruta de actulizar imagen de perfil
        


        Route::group(['prefix' => '/user'], function(){
            Route::get('/perfil', 'ProfileUserController@perfil')->name('perfil');
            Route::get('/datos_personal', 'UsuariosController@datosPersonal');         
            Route::get('/datos_usuarios', 'UsuariosController@listAll');
            Route::get('/selects_entidad', 'UsuariosController@listEntidades');//ruta para llenar el selec de entidades
            Route::get('/selects_municipio', 'UsuariosController@listMunicipios');//ruta para llenar el selec de municipios
            // Route::get('/selects_unidad', 'UsuariosController@listUnidades');//ruta para llenar el select de unidades
            Route::get('/action', 'UsuariosController@userList');//ruta para tabla 

            Route::post('/updateDataPersonal','RegistroUsuarioController@updateDataPersonal');//ruta sin validar
        });  

        Route::group(['prefix' => '/roles'], function(){
            Route::get('/control_roles', 'RolController@listRol');
            Route::post('/rol_register', 'RolController@insertDataRol');
        });

        Route::group(['prefix' => '/personalUsuario'], function(){
            Route::post('/menu_rol', 'RolController@permisosMenuRol');
            Route::post('/updateData','RegistroUsuarioController@updateData');
            Route::post('/registroUsuario','RegistroAltaUsuarioController@registerUser');//ruta para insertar nuevo usuario
            Route::post('/deletePersonal','RegistroUsuarioController@deleteData');
            Route::post('/updateDataPassword','RegistroUsuarioController@updatePassword');
        });        
        
    });
});


Route::fallback(function() {
    return abort(404);
});

///// CONFIGURACION DE RUTAS CON LOGIN

//////////////////AGREGAR MAS RUTAS CUSTOM

Route::group(['prefix' => '/registro'], function(){    
    Route::get('/', 'RegistroController@registro');
    Route::post('/', 'RegistroController@insertarRegistro');
});


Route::get('/municipios/{name}','RegistroController@getMunicipios');
Route::get('/entidades','RegistroController@getEntidades');

Route::group(['prefix' => '/seguimiento'], function(){
    Route::get('/', 'RegistroController@seguimientoView')->name('seguimietoView');
    Route::post('/', 'RegistroController@seguimientoPostulado');
    Route::patch('/', 'RegistroController@edicionPostulado');
});


Route::get('/postulado', function () {
    return view('Registro/postulado');
})->name('postulado');

Route::group(['prefix' => '/postulado'], function(){
    Route::get('/{id}', 'RegistroController@mostrarDetalle');
    Route::post('/', 'RegistroController@accionesPostulado');    
});

Route::get('/reporte','RegistroController@reportePostulados')->name('reporte');//pruebas
Route::get('/puestounidad','ExportController@reportePuestoUnidad');
Route::get('/porusuarios','ExportController@reportePorUsuarios');

///seleccionados
Route::get('/seleccionados','RegistroController@getSeleccionados');