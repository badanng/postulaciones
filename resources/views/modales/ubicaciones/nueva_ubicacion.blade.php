<div id="add_place" class="modal fade" role="dialog" data-backdrop="false">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="nombreHeader">
             Nueva Sede
             <?php// echo $value['usuario']['nombre'].' '.$value['usuario']['apellidoPaterno'].' '.$value['usuario']['apellidoMaterno']; ?>
             </h5>
              <button type="button" class="flaticon2-delete btn btn-sm btn-label-primary btn-bold" id="close_modal_nub" data-dismiss="modal" aria-label="Close" style="font-size: 0.8rem;"></button>
          </div>
          <div class="modal-body" id="modal_content">
            <form class="form-horizontal" role="form" id="nuevo_usuario">
         <div class="modal-body">
           <div class="row form-group col-md-12">
             <label for="sede" class="control-label col-sm-4 col-xs-12">Sede: </label>
             <div class="col-sm-5 col-xs-6">
               <input type="text" class="form-control" id="sede" name="sede" required>
           </div>
          </div>
            <div class="row form-group col-md-12">
              <label for="calle" class="control-label col-sm-4 col-xs-12">Calle: </label>
              <div class="col-sm-5 col-xs-6">
                <input type="text" class="form-control" id="calle" name="calle" required>
              </div>
            </div>
            <div class="row form-group col-md-12">
              <label for="municipio-alcaldia" class="control-label col-sm-4 col-xs-12">Municipo/Alcaldía: </label>
              <div class="col-sm-5 col-xs-6">
                <input type="text" class="form-control" id="municipio_alcaldia" name="municipio_alcaldia" required>
              </div>
            </div>
            <div class="row form-group col-md-12">
              <label for="estado" class="control-label col-sm-4 col-xs-12">Estado: </label>
              <div class="col-sm-5 col-xs-6">
                <input type="text" class="form-control" id="estado" name="estado" required>
              </div>
            </div>
         </div>
         <div class="modal-footer">
           <button type="button" id="input_location" name="input_location" class="btn btn-sm btn-label-primary btn-bold">
             <span class="fa fa-save"></span>
              <span class="hidden-xs"> Guardar</span>
           </button>   {{ csrf_field() }}
         </div>
       </form>
      </div>
    </div>
  </div>
</div>
