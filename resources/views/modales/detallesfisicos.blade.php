<div class="modal fade" id="fisicos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
          <div class="modal-header">
           <button type="button" id="cerrar_editar" class="flaticon2-delete btn btn-sm btn-label-primary btn-bold" style="font-size: 0.8rem;" data-dismiss="modal" aria-label="Close"></button>
          </div>
         
          <div class="modal-body" id="modal_content">

              <div id="contenido">
              </div>
            
          </div>
    </div>
  </div>
</div>
