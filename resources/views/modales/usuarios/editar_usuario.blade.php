<div class="modal fade" id="show_modal_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="false">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="nombreHeader">Editar Usuario</h5>
            <button type="button" id="cerrar_editar" class="flaticon2-delete btn btn-sm btn-label-primary btn-bold" style="font-size: 0.8rem;" data-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body" id="modal_content">
            <form class="form-horizontal" role="form" id="edita_usuario">
              <input type = "hidden" name = "user_idm" id = "user_idm">
       				<div class="modal-body">
                <div class="form-row">
                  <input type="hidden" id="idm">
         					<div class="form-group col-md-6" style="text-align: left;">
         						<label for="nombrem" >Nombre (s)</label>
         						<input type="text" class="form-control" id="nombrem" name="nombrem">
         					</div>
                  <div class="form-group col-md-6" style="text-align: left;">
                    <label for="puestom" >Puesto</label>
                    <input type="text" class="form-control" id="puestom" name="puestom">
                  </div>
                  <div class="form-group col-md-6" style="text-align: left;">
                    <label for="emailm" >Email</label>
                    <input type="text" class="form-control" id="emailm" name="emailm">
                  </div>
                  <div class="form-group col-md-6" style="text-align: left;">
                    <label for="estadosedit" >Entidad</label>
                    <select id="estadosedit" name="estadosedit" class="form-control">
                      <option value="" selected>Seleccione...</option>
                    </select>
                    <strong class="text_estado"></strong>
                  </div>   
                  <!-- <div class="form-group col-md-6" style="text-align: left;">
                    <label for="subdireccionm" class="control-label">Subdirección</label>
                    <input type="text" class="form-control" id="subdireccionm" name="subdireccionm">
                  </div> -->
                  <div class="form-group col-md-6" style="text-align: left;">
                    <label for="telefonom" class="control-label">Teléfono</label>
                    <input type="text" class="form-control" id="telefonom" name="telefonom">
                  </div>

                  <div class="form-group col-md-6" style="text-align: left;">
                    <label for="rol_usuarioedit">Rol del usuario</label>
                  
                      <select id="rol_usuarioedit" name="rol_usuarioedit" class="form-control">
                        <option value="" selected>Seleccione...</option>
                        @foreach ($roles as $rol)
                          <option value="{{$rol->id}}">{{$rol->nombre_rol}}</option>
                        @endforeach                                                
                      </select>
                      <strong class="text_rol"></strong>
                  </div>

                  <!-- <div class="form-group col-md-6" style="text-align: left;">
                    <label for="redm" class="control-label">Red</label>
                    <input type="text" class="form-control" id="redm" name="redm">
                  </div>
                  <div class="form-group col-md-6" style="text-align: left;">
                    <label for="unidad_laboralm" class="control-label">Unidad Laboral</label>
                    <input type="text" class="form-control" id="unidad_laboralm" name="unidad_laboralm">
                  </div> -->
       				  </div>
         				<div class="modal-footer">
                  <button type="button" id="usr_editar" name="usr_editar" class="btn btn-sm btn-label-primary btn-bold" style="font-size: 1.4rem;">
         						<span class="fa fa-save"></span>
                     <span class="hidden-xs"> Guardar</span>
         					</button>

                   <button type="button" id="delete_usr" name="delete_usr" class="btn btn-sm btn-label-danger btn-bold btn-ok" style="font-size: 1.4rem;">
         						<span class="fa fa-save"></span>
                     <span class="hidden-xs"> Eliminar</span>
         					</button>
                  {{ csrf_field() }}
         				</div>
              </div>
       			</form>
          </div>
    </div>
  </div>
</div>
