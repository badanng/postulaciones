<div class="modal fade" id="show_modal_password_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="false">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="nombreHeader">Cambiar Contraseña</h5>
            <button type="button" id="cerrar_editar" class="flaticon2-delete btn btn-sm btn-label-primary btn-bold" style="font-size: 0.8rem;" data-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body" id="modal_content">
            <form class="form-horizontal" role="form" id="edita_password_usuario">
              <input type = "hidden" name = "user_id" id = "user_id">
       				<div class="modal-body">
                <div class="form-row">
                  <input type="hidden" id="id">
                  <div class="form-group col-md-6" style="text-align: left;">
                    <label for="new_pass" class="control-label">Nueva Contraseña</label>
                    <input type="password" autocomplete="cc-number" class="form-control" id="new_pass" name="new_pass" min="8">
                  </div>
                  <div class="form-group col-md-6" style="text-align: left;">
                    <label for="new_pass_confirm" class="control-label">Confirmar Contraseña</label>
                    <input type="password" autocomplete="cc-number" class="form-control" id="new_pass_confirm" name="new_pass_confirm" min="8">
                  </div>
       				  </div>
         				<div class="modal-footer">
                   <button type="button" id="save_pass_button" name="save_pass_button" class="btn btn-sm btn-label-danger btn-bold btn-ok" style="font-size: 1.4rem;">
         						<span class="fa fa-save"></span>
                     <span class="hidden-xs">Guardar</span>
         					</button>
                  {{ csrf_field() }}
         				</div>
              </div>
       			</form>
          </div>
    </div>
  </div>
</div>