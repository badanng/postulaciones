<div class="modal fade" id="editt" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="false">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="nombreHeader">Editar</h5>
            <button type="button" id="cerrar_editar" class="flaticon2-delete btn btn-sm btn-label-primary btn-bold" style="font-size: 0.8rem;" data-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body" id="modal_content">
            <form class="form-horizontal" role="form" id="nuevo_usuario">
              <input type = "hidden" name = "user_id" id = "user_id">
       				<div class="modal-body">
                <div class="form-row">
                  <input type="hidden" id="id">
         					<div class="form-group col-md-6" style="text-align: left;">
         						<label for="nombre" >Nombre (s)</label>
         						<input type="text" class="form-control" id="nombre" name="nombre">
         					</div>
                  <div class="form-group col-md-6" style="text-align: left;">
                    <label for="puesto" >Puesto</label>
                    <input type="text" class="form-control" id="puesto" name="puesto">
                  </div>
                  <div class="form-group col-md-6" style="text-align: left;">
                    <label for="email" >Email</label>
                    <input type="text" class="form-control" id="email" name="email">
                  </div>
                  <div class="form-group col-md-6" style="text-align: left;">
                    <label for="estadosedit" >Delegación</label>
                    <select id="estadosedit" name="estadosedit" class="form-control">
                      <option value="" selected>Seleccione...</option>
                    </select>
                    <strong class="text_estado"></strong>
                  </div>   
                  <div class="form-group col-md-6" style="text-align: left;">
                    <label for="subdireccion" class="control-label">Subdirección</label>
                    <input type="text" class="form-control" id="subdireccion" name="subdireccion">
                  </div>
                  <div class="form-group col-md-6" style="text-align: left;">
                    <label for="telefono" class="control-label">Teléfono</label>
                    <input type="text" class="form-control" id="telefono" name="telefono">
                  </div>
                  <div class="form-group col-md-6" style="text-align: left;">
                    <label for="red" class="control-label">Red</label>
                    <input type="text" class="form-control" id="red" name="red">
                  </div>
                  <div class="form-group col-md-6" style="text-align: left;">
                    <label for="unidad" class="control-label">Unidad Laboral</label>
                    <input type="text" class="form-control" id="unidad" name="unidad">
                  </div>
       				  </div>
         				<div class="modal-footer">
                  <button type="button" id="usr_editar" name="usr_editar" class="btn btn-sm btn-label-primary btn-bold" style="font-size: 1.4rem;">
         						<span class="fa fa-save"></span>
                     <span class="hidden-xs"> Guardar</span>
         					</button>
                  <button type="button" id="usr_eliminar" name="usr_eliminar" class="btn btn-danger">
                    <span class="la la-trash-o" ></span>
                     <span class="hidden-xs"> Eliminar</span>
                  </button> {{ csrf_field() }}
         				</div>
              </div>
       			</form>
          </div>
    </div>
  </div>
</div>
<script>
   $(document).ready(function(){
     $("#agregar_comentario").click(function(){
       $("#add_comments").modal();
     });
   });
</script>
