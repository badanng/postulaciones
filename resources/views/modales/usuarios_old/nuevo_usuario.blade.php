<div class="modal fade" id="nuevo" tabindex="-1" data-backdrop="false" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel" >Nuevo Usuario</h5>
                <button type="button" id="cerrar_modal"  data-dismiss="modal" aria-label="Close" class="flaticon2-delete btn btn-sm btn-label-primary btn-bold" style="font-size: 0.8rem;"></button>
            </div>
            <div class="modal-body" id="modal_content">
              
              <form class="form-horizontal" role="form" id="nuevo_usuario">
          				<div class="modal-body">
                    <div class="form-row">
            					<div class="form-group col-md-6" style="text-align: left;">
            						<label  for="nombre">Nombre (s)</label>
            						<input type="text" class="form-control" id="nombre" name="nombre" required>
                        <strong class="text_nombre"></strong>
            					</div>
                      <div class="form-group col-md-6" style="text-align: left;">
                        <label for="puesto"  >Puesto</label>
                        <input type="text" class="form-control" id="puesto" name="puesto" required>
                        <strong class="text_puesto"></strong>
                      </div>
                      <div class="form-group col-md-6" style="text-align: left;">
                        <label for="email" class="control-label">Email</label>
                        <input type="text" class="form-control" id="email" name="email">
                          <strong class="text_email"></strong>
                      </div>
                      <div class="form-group col-md-6" style="text-align: left;">
                        <label for="estado">Entidad</label>
                        <select id="estado" name="estado" class="form-control">
                          <option value="" selected>Seleccione...</option>
                        </select>
                        <strong class="text_estado"></strong>
                      </div>  
                      <div class="form-group col-md-6" style="text-align: left;">
                        <label for="subdireccion" class="control-label">Subdirección</label>
                        <input type="text" class="form-control" id="subdireccion" name="subdireccion">
                          <strong class="text_subdireccion"></strong>
                      </div>
                      <div class="form-group col-md-6" style="text-align: left;">
                        <label for="telefono" class="control-label">Teléfono</label>
                        <input type="text" class="form-control" id="telefono" name="telefono">
                          <strong class="text_telefono"></strong>
                      </div>
                      <div class="form-group col-md-6" style="text-align: left;">
                        <label for="red" class="control-label">Red</label>
                        <input type="text" class="form-control" id="red" name="red">
                          <strong class="text_red"></strong>
                      </div>
                      <div class="form-group col-md-6" style="text-align: left;">
                        <label for="unidad" class="control-label">Unidad Laboral</label>
                        <input type="text" class="form-control" id="unidad" name="unidad">
                          <strong class="text_unidad"></strong>
                      </div>
                      <div class="form-group col-md-6" style="text-align: left;">
                        <label for="password" class="control-label">Contraseña</label>
                        <input type="password" class="form-control" id="password" name="password">
                        <strong class="text_password"></strong>
                      </div>
                    </div>
                  </div>
            			<div class="modal-footer" >
            				<button type="button" id="agregarUsuario" name="agregarUsuario" class="btn btn-sm btn-label-primary btn-bold">
            				<span class="fa fa-save"></span>
                    <span class="hidden-xs"> Guardar</span>
            				</button>   {{ csrf_field() }}
            			</div>
    			    </form>
            </div>
        </div>
    </div>
</div>
