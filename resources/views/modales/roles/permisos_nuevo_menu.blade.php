
<div data-backdrop="false" class="modal fade" id="nuevo_permisos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <style>
      #descripcion{
          margin-top: 0px !important;
          padding-top: 6px !important;
      }
      #rls_js_fn_03{
        position:relative;
        top: 12px;
        left: 20px;
      }

       .form-row {
    display: flex;
    flex-wrap: wrap;
    margin-right: -261px;
    margin-left: 43px;
    }
      </style>
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Permisos : <?=env('APP_NAME')?></h5>
                <button type="button" class="flaticon2-delete btn btn-sm btn-label-primary btn-bold" style="font-size: 0.8rem;" data-dismiss="modal" aria-label="Close"></button>
            </div>
          <!--INICIO DE DOBLE COLUMNA-->
            <div class="modal-body" id="modal_content">
              <form class="form-horizontal" role="form" id="permisos_administrador">
          				<div class="modal-body">
                    <div class="form-row">
            					<div class="form-group col-md-6" style="text-align: left;">
            					@foreach(Session::get('listamenu') as $value)

          					<div class="form-row">
                    <div class="form-group col-md-5" style="text-align: left;">
                    <label for="curp" class="control-label">{{$value->etiqueta}}</label>
                  
                    </div>

                     <div class="form-group col-md-2">
                    </div>

                    <div class="form-group col-md-5">
                      <span class="kt-switch kt-switch--outline kt-switch--icon kt-switch--danger">
                      <label>
                      <input type="checkbox"  name="{{$value->etiqueta}}" value="{{$value->id}}" id='{{$value->id}}'>
                      <span></span>
                      </label>
                      </span>
                    </div>
                    </div>

                 @endforeach
                    
    			    </form>
            </div>
          </div>

          <!--FIN DE DOBLE COLUMNA-->
      
           <div class="modal-footer">
             <button type="button" id="save_rol" name="save_rol" class="btn btn-sm btn-label-primary btn-bold" onClick='GuardaPermisosRol({{Session::get('listamenu')}});'>
              <span class="fa fa-save"></span>
               <span class="hidden-xs"> Guardar</span>
            </button>   {{ csrf_field() }}
					  </div>
          
    </div>
 </div>
</div>
