<div data-backdrop="false" class="modal fade" id="add_roll" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <style>
        #descripcion{
            margin-top: 0px !important;
            padding-top: 6px !important;
        }
        #rls_js_fn_03{
          position:relative;
          top: 12px;
          left: 20px;
        }
      </style>
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="myModalLabel">Gestionar Roles para <?=env('APP_NAME')?></h5>
          <button type="button" class="flaticon2-delete btn btn-sm btn-label-primary btn-bold" style="font-size: 0.8rem;" data-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body" id="modal_content">
          <form class="form-horizontal" role="form" id="nuevo_rol">
            <div class="modal-body">
              <div class="row form-group col-md-12" style="text-align: right;">
                <label for="rol" style="text-aling:right;" class="control-label col-sm-4 col-xs-12"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Rol </font></font></label>
                <div class="col-sm-5 col-xs-6">
                  <input type="text" class="form-control" id="nom_rol" name="nom_rol">
                </div>
              </div>
              <div class="row form-group col-md-12" style="text-align: right">
                <label for="desc_rol" class="control-label col-sm-4 col-xs-12"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Descripción del rol </font></font></label>
                <div class="col-sm-5 col-xs-6">
                  <input type="text" class="form-control" id="desc_rol" name="desc_rol">
                </div>
              </div>
              <div class="row form-group col-md-12" style="text-align: right">
                <label for="tipo_rol" class="control-label col-sm-4 col-xs-12"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Tipo de Rol</font></font></label>
                <div class="col-sm-5 col-xs-6">
                  <input type="text" class="form-control" id="tipo_rol" name="tipo_rol">
                </div>
              </div>
            </div>
          </form>
        </div>
				<div class="modal-footer">
          <button type="button" id="add_rol" name="add_rol" class="btn btn-sm btn-label-primary btn-bold">
            <span class="fa fa-save"></span>
            <span class="hidden-xs"> Guardar</span>
          </button>   {{ csrf_field() }}
				</div>
      </div>
    </div>
</div>
