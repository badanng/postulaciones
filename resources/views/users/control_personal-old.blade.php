@extends('plantilla.plantilla')
@section('control-usuarios')
	@if (Auth::user()->id_rol == 1)
		<script>
		$("#breadcrumb-title").html('<?=env('APP_NAME')?>');
		$("#breadcrumb-title").append(' Control de Usuarios  ');
		</script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

		<div class="m-portlet m-portlet--mobile">
		 	<div class="m-portlet__head">
		      <div class="m-portlet__head-caption">
		        <div class="col-xl-12 order-1 order-xl-2  m--align-right" >
		          <a data-toggle="modal" data-target="#nuevo" id="reg_nue" href="javascript:;" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
		            <span><i class="fa fa-users left"></i><span>Nuevo Usuario</span></span>
		          </a>
		           @include('modales/usuarios/nuevo_usuario')
		          <div class="m-separator m-separator--dashed d-xl-none"></div>
		        </div>
		      </div>
		  	</div>
		  	<div class="m-portlet__body">
			    <table id="user_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
			      <thead >
			        <tr>
			          {{-- <th>ID</th> --}}
			          <th>Nombre</th>
			          <th>Puesto</th>
			          <th>Email</th>
			          <th>Delegación</th>
			          <th>Subdirección</th>
			          <th>Teléfono</th>
			          <th>Red</th>
			          <th>Unidad Laboral</th>            
			          <th>Editar</th>
			        </tr>
			      </thead>
			    </table>  
		  	</div>
		</div>
		@include('modales/usuarios/editar_usuario')

	@else
	    No tienes los permisos
	@endif

@endsection