@extends('plantilla.plantilla')
@section('control-usuarios')
<script>
$("#breadcrumb-title").html('<?=env('APP_NAME')?>');
$("#breadcrumb-title").append(' Control de Usuarios  ');
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<div class="m-portlet m-portlet--mobile" id="contenedor_personal">
 	<div class="m-portlet__head">
      <div class="m-portlet__head-caption">
        <div class="col-xl-12 order-1 order-xl-2  m--align-right" >
          <a data-toggle="modal" data-target="#nuevo" id="reg_nue" href="javascript:;" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
            <span><i class="fa fa-users left"></i><span>Nuevo Usuario</span></span>
          </a>
           @include('modales/usuarios/nuevo_usuario')
          <div class="m-separator m-separator--dashed d-xl-none"></div>
        </div>
      </div>
  	</div>
  	<div class="m-portlet__body">
	    <table id="user_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
	      <thead >
	        <tr>
	          <th>ID</th>
	          <th>Nombre</th>
	          <th>Puesto</th>
	          <th>Email</th>
            <th>Entidad</th>
            <th>Telefono</th>




            <th>Rol del usuario</th>
            <th>Editar</th>
            <th>Cambiar contraseña</th>
	        </tr>
	      </thead>
	      <tbody>
            @foreach ($tabla as $user)
                <tr class="m-datatable__row m-datatable__row--even">
                  <td class="m-datatable__cell">{{$user->id}}</td>
                  <td class="m-datatable__cell">{{strtoupper($user->nombre)}}</td>
                  <td class="m-datatable__cell">{{strtoupper($user->puesto)}}</td>
                  <td class="m-datatable__cell">{{$user->email}}</td>
                  <td class="m-datatable__cell">{{$user->entidad['entidad']}}</td>
                  <td class="m-datatable__cell">{{$user->telefono}}</td>

                  
                  <td class="m-datatable__cell">{{strtoupper($user->rol->nombre_rol)}}</td>
                  <td class="m-datatable__cell"><button  class="btn-edit btn btn-info btn-outline-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" data-toggle="modal" data-target="#show_modal_edit" data-idestado="{{ $user->id_entidad }}" data-idrol="{{ $user->id_rol }}"><i class="fas fa-cog left"></i></button></td>
                  <td class="m-datatable__cell"><button  class="btn btn-info btn-outline-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" data-toggle="modal" data-target="#show_modal_password_edit" ><i class="fas fa-lock center"></i></button></td>
                </tr>
             @endforeach
            </tbody>
	    </table>  
  	</div>
</div>
@include('modales/usuarios/editar_usuario')
@include('modales/usuarios/editar_password')
@endsection
