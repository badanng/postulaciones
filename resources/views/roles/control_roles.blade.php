@extends('plantilla.plantilla')
@section('roles')

<script>
$("#breadcrumb-title").html('<?=env('APP_NAME')?>');
$("#breadcrumb-title").append(' / Control de Roles  ');
</script> 
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="assets/js/script_crud.js"></script>
<div class="m-portlet m-portlet--mobile">
  <div class="m-portlet__head">
    <div class="m-portlet__head-caption">
      <div class="col-xl-12 order-1 order-xl-2  m--align-right" >
        <a data-toggle="modal" data-target="#add_roll" id="" href="javascript:;" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
          <span><i class="fa fa-users left"></i><span>Nuevo Rol</span></span>
        </a>
        @include('modales/roles/nuevo_rol')
        <div class="m-separator m-separator--dashed d-xl-none"></div>
      </div>
    </div>
  </div>
  <div class="m-portlet__body">
    <table id="roles" class="table table-striped table-bordered" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th>ID</th>
          <th>Rol</th>
          <th>Tipo</th>
          <th>Descripción</th>
          <th>Permisos</th>
        </tr>
      </thead>
      @foreach($allRol as $value)
        @include('roles/tabla_rol')
      @endforeach
    </table>
  </div>
    @include('modales/roles/permisos_nuevo_menu')    
</div>
@endsection