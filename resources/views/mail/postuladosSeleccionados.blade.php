<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html;" charset="utf-8" />
    <!-- view port meta tag -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
    <title>Postulaciones</title>
    <style type="text/css">
      /* hacks */
      * { -webkit-text-size-adjust:none; -ms-text-size-adjust:none; max-height:1000000px;}
      table{border-collapse: collapse !important;}
      #outlook a {padding:0;}
      .ReadMsgBody { width: 100%; }
      .ExternalClass { width: 100%; }
      .ExternalClass * { line-height: 100%; }
      .ios_geo a { color:#1c1c1c !important; text-decoration:none !important; }
      .imagen{
      width: 200px;
      }
      /* responsive styles */
      @media only screen and (max-width: 600px) { 
      /* global styles */
      .hide{ display:none !important; display:none;}
      .blockwrap{ display:block !important; }
      .showme{ display:block !important; width: auto !important; overflow: visible !important; float: none !important; max-height:inherit !important; max-width:inherit !important; line-height: auto !important; margin-top:0px !important; visibility:inherit !important;}
      *[class].movedown{ display: table-footer-group !important;}
      *[class].moveup{ display: table-header-group !important; }
      /* font styles */
      *[class].textright{ text-align: right !important; }
      *[class].textleft{ text-align: left !important; }
      *[class].textcenter{ text-align: center !important; }
      *[class].font27{ font-size: 27px !important; font-weight:normal !important; line-height:27px !important; }
      /* width and heights */
      *[class].hX{ height:Xpx !important; }
      *[class].w360{ width:380px !important; }
      /* spacios */
      *[class].mtop10{ margin-top: 10px !important; }
      *[class].mbt10{ margin-bottom: 10px !important; }
      *[class].mbt20{ margin-bottom: 20px !important; }
      *[class].pleft15{ padding-left: 15px !important; }
      *[class].pright15{ padding-right: 15px !important; }
      }
    </style>
  </head>
  <body style="margin:0; padding:0;" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
    <!--[if gte mso 15]>
    <style type="text/css" media="all">
      tr { font-size:1px; mso-line-heiht-alt:0; mso-margin-top-alt:1px; }
    </style>
    <![endif]--> 
    <table width="100%" border="0" cellspacing="0" cellpadding="0" cellmargin="0">
    <tr>
      <td align="center" valign="top">
        <!-- tabla de contenido centrado -->
        <table cellpadding="0" border="0" cellmargin="0" width="720" align="center" class="w360">
          <tr>
            <td width="720" class="w380 textcenter">
              <table width="100%" border="0" cellspacing="0" cellpadding="0" cellmargin="0">
                <tr>
                  <td>
                  </td>
                </tr>
                <tr>
                  <td>
                    <!-- hero issste -->
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" cellmargin="0">
                      <tr>
                        <td>
                          <img src="https://convocatoria.issste.gob.mx/images/convocatoria.jpg" alt="" style="width: 100%;" />
                        </td>
                      </tr>
                    </table>
                    <!-- hero issste /-->
                  </td>
                </tr>
                <tr>
                  <td>
                    <!-- bloque 3 issste -->
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" cellmargin="0" bgcolor="#ffffff">
                      <tr>
                        <td>
                          <img src="https://convocatoria.issste.gob.mx/images/spacer.gif" width="1" height="48" alt="" />
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" cellmargin="0">
                            <tr>
                              <td>
                                <img src="https://convocatoria.issste.gob.mx/images/spacer.gif" width="45" height="1" alt="" />
                              </td>
                              <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" cellmargin="0">
                                  <tr>
                                    <td>
                                      <span style="color: #5d0923; font-family: arial; font-size: 27px; font-weight: bold">
                                      Estimado (a): {{ $msj['name'] }}
                                      </span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <img src="https://convocatoria.issste.gob.mx/images/spacer.gif" width="1" height="45px" alt="" />
                                    </td>
                                  </tr>
                                  <tr>
                                    <tr>
                                      <td>
                                        <span style="color: #5d0923; font-family: arial; font-size: 21px;">
                                            Nos es muy grato saber de su interés por sumar esfuerzos en esta lucha Nacional contra el COVID19, contar con profesionales de salud comprometidos es esencial para nuestro México.
                                        </span>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td><img src="https://convocatoria.issste.gob.mx/images/spacer.gif" width="1" height="10px" alt="" /></td>
                                    </tr>
                                    <tr>
                                      <td>
                                        <span style="color: #5d0923; font-family: arial; font-size: 21px;">
                                            Nos ponemos en contacto con usted para pedirle amablemente complemente el registro que inició con nosotros a través del siguiente vínculo ingresando el numero de folio.
                                        </span>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td><img src="https://convocatoria.issste.gob.mx/images/spacer.gif" width="1" height="10px" alt="" /></td>
                                    </tr>
                                    <tr>
                                        <td>
                                          <span style="color: #5d0923; font-family: arial; font-size: 21px;">
                                            <b><a href="https://convocatoria.issste.gob.mx/seguimiento" target="_blank" style="text-decoration: none; color: #5d0923;">https://convocatoria.issste.gob.mx/seguimiento</a></b>
                                          </span>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td><img src="https://convocatoria.issste.gob.mx/images/spacer.gif" width="1" height="10px" alt="" /></td>
                                      </tr>
                                    <tr>
                                      <td>
                                        <span style="color: #5d0923; font-family: arial; font-size: 21px;">
                                            Agradecemos de antemano el esfuerzo invertido.
                                        </span>
                                      </td>
                                    </tr>
                                  </tr>
                                </table>
                              </td>
                              <td>
                                <img src="https://convocatoria.issste.gob.mx/images/spacer.gif" width="45" height="1" alt="" />
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <img src="https://convocatoria.issste.gob.mx/images/spacer.gif" width="1" height="48" alt="" />
                        </td>
                      </tr>
                    </table>
                    <!-- bloque 3 issste /-->
                  </td>
                </tr>
                <tr>
                  <td>
                    <!-- bloque 5 escotiabank issste -->
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" cellmargin="0" bgcolor="#ffffff">
                      <tr>
                        <td>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" cellmargin="0">
                            <tr>
                              <td>
                                <img src="https://convocatoria.issste.gob.mx/images/spacer.gif" width="45" height="1" alt="" />
                              </td>
                              <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" cellmargin="0">
                                  <tr align="center" valign="middle">
                                    <td>
                                      <img src="images/ic_pago2.png" alt="" />
                                    </td>
                                    <td>
                                      <img src="https://convocatoria.issste.gob.mx/images/spacer.gif" width="20" height="1" alt="" />
                                    </td>
                                    <td>
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0" cellmargin="0">
                                        <tr>
                                          <td>
                                            <span style="color: #676767;font-family: arial; font-size: 18px;">
                                            Folio <b>{{ $msj['folio'] }}</b>
                                            </span>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td>
                                            <img src="https://convocatoria.issste.gob.mx/images/spacer.gif" width="1" height="10" alt="" />
                                          </td>
                                        </tr>
                                        
                                        <tr>
                                          <td>
                                            <img src="https://convocatoria.issste.gob.mx/images/spacer.gif" width="1" height="10" alt="" />
                                          </td>
                                        </tr>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td>
                                <img src="https://convocatoria.issste.gob.mx/images/spacer.gif" width="70" height="1" alt="" />
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <img src="https://convocatoria.issste.gob.mx/images/spacer.gif" width="1" height="48" alt="" />
                        </td>
                      </tr>
                    </table>
                    <!-- bloque 5  issste /-->
                  </td>
                </tr>
                <tr>
                  <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" cellmargin="0" bgcolor="#ffffff">
                      <tr>
                        <td>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" cellmargin="0" bgcolor="#ffffff">
                            <tr>
                              <td>
                                <img src="https://convocatoria.issste.gob.mx/images/spacer.gif" width="45" height="1" alt="" />
                              </td>
                              <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" cellmargin="0" bgcolor="#ffffff">
                                  <tr align="center">
                                    <td>
                                      <span style="color: #5d0923; font-family: arial; font-size: 21px;">
                                        
                                      </span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <img src="https://convocatoria.issste.gob.mx/images/spacer.gif" width="1" height="20" alt="" />
                                    </td>
                                  </tr>
                                 
                                  <tr>
                                    <td>
                                      <img src="https://convocatoria.issste.gob.mx/images/spacer.gif" width="1" height="20" alt="" />
                                    </td>
                                  </tr>
                                  <tr align="center">
                                    <td>
                                      <span style="color: #00233A; font-family: arial; font-size: 21px;">
                                      <a href="https://convocatoria.issste.gob.mx" target="_blank" style="text-decoration: none; color: #5d0923;">https://convocatoria.issste.gob.mx</a>
                                      </span>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                              <td>
                                <img src="https://convocatoria.issste.gob.mx/images/spacer.gif" width="45" height="1" alt="" />
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <img src="https://convocatoria.issste.gob.mx/images/spacer.gif" width="1" height="48" alt="" />
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td>
                    <!-- footer bloque -->
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" cellmargin="0" bgcolor="#5d0923">
                      <tr>
                        <td>
                          <img src="https://convocatoria.issste.gob.mx/images/spacer.gif" width="1" height="20" alt="" />
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" cellmargin="0">
                            <tr>
                              <td>
                                <img src="https://convocatoria.issste.gob.mx/images/spacer.gif" width="45" height="1"  alt="" />
                              </td>
                              <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" cellmargin="0">
                                  <tr>
                                    <td width="100%">
                                      <span  style="font-size: 19px; text-align: center; margin: 0; color: #ffffff; font-family: arial; line-height: 31px;">
                                      
                                      </span>
                                    </td>
                                   
                                  </tr>
                                </table>
                              </td>
                              <td>
                                <img src="https://convocatoria.issste.gob.mx/images/spacer.gif" width="45" height="1"  alt="" />
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <img src="https://convocatoria.issste.gob.mx/images/spacer.gif" width="1" height="20" alt="" />
                        </td>
                      </tr>
                    </table>
                    <!-- footer bloque /-->
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
  </body>
</html>