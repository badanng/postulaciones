
@extends('plantilla.plantilla')
@section('content')

@if( Auth::user()->id_rol==4 )
  <div id="contenedor_principal">
    <h2 class="text-center">NO TIENES PERMISO PARA VER ESTE CONTENIDO</h2>
    <H3 class="text-center">VE A LA SIGUIENTE LIGA PARA CONTINUAR <a href="/"> CLICK AQUI</a> </H3>
  </div>
@else

<style>
@media (min-width: 576px){
  .modal-dialog {
      max-width: 70%;
      margin: 1.75rem auto;
  }
}
table{
  margin-left: auto;
  margin-right: auto;
  display: block;
}
</style>
 
 <h5 style="margin-top: 78px; margin-bottom: 15px;">
 Lista de postulados - Mostrando: {{ $paginacion->total() }}
 </h5>

 <div class="container">
  <div class="row">
    <div class="col-sm">

      <form>
        <div class="form-group" style="text-align: left;">          
          <div class="btn-group" role="group">      
            <input type="text" class="form-control" placeholder="BUSCAR POR CURP" id="curp" name="curp" required >
            <button type="button" class="btn btn-secondary limpiar">X</button>
            <button type="submit" class="btn btn-primary"><i class="la flaticon-search-1 "></i> BUSCAR CURP</button>
          </div>
        </div>
      </form>   

    </div>
     
    <div class="col-sm">

    <form>
        <div class="form-group" style="text-align: left;">          
          <div class="btn-group" role="group">      
            <input type="text" class="form-control" placeholder="BUSCAR POR NOMBRE" id="nombre" name="nombre" required >
            <button type="button" class="btn btn-secondary limpiar">X</button>
            <button type="submit" class="btn btn-primary"><i class="la flaticon-search-1 "></i> BUSCAR NOMBRE</button>
          </div>
        </div>
      </form>   

    </div>
     
  </div>

  @if( Auth::user()->id_rol==1 )
  <div class="btn-group float-right d-none" role="group">    
      <button type="button" id="exportarExcel" data-texto="EXCEL" class="btn btn-primary d-none">EXCEL POST</button>
      <button type="button" id="exportarExcel1" data-texto="CSV PUESTO Y UNIDAD" class="btn btn-success"><i class="la flaticon-download "></i> CSV PUESTO Y UNIDAD</button>
      <button type="button" id="exportarExcel2" data-texto="CSV USUARIOS" class="btn btn-dark"><i class="la flaticon-download "></i> CSV USUARIOS</button>
  </div>
  @endif

</div>



<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">CURP</th>
      <th scope="col">Nombre</th>

      <th scope="col">Nivel Tecnico
        <select class="form-control filtro" id="nivelTecnico" name="nivelTecnico">
            <option value="">TODOS</option>            
            <optgroup label="Secundaria/Bachillerato con curso afín">
                <option value="1">Camillero</option>
                <option value="2">Asistente de cocina</option>
            </optgroup>
            <optgroup label="Bachillerato con carrera técnica">
                <option value="3">Asistente de cocina</option>
                <option value="4">Técnico en Inhaloterapia</option>
                <option value="5">Técnico Radiólogo</option>
                <option value="6">Técnico Laboratorista</option>
                <option value="7">Técnico en Enfermería con Postécnico</option>
            </optgroup>
            <optgroup label="Licenciatura">
                <option value="8">Médico general</option>
                <option value="9">Médico general con especialidad</option>
                <option value="10">Enfermería</option>
                <option value="11">Enfermería con especiadidad/postécnico</option>
            </optgroup>
            <optgroup label="Ingeniero">
                <option value="12">Biomédico</option>
                <option value="13">Químico</option>
            </optgroup>
            <optgroup label="Nivel de Escolaridad">
              <option value="14">Cardiólogo</option>
              <option value="15">Médico Familiar</option>
              <option value="16">Epidemiólogo</option>
            </optgroup>
        </select>
      </th>

      <th scope="col">Especialidad
        <select class="form-control filtro" id="especialidad" name="especialidad">
        <option value="">TODOS</option>
        <option value="Urgencias">Urgencias</option><option value="Terapia Intensiva">Terapia Intensiva</option><option value="Medicina Interna ">Medicina Interna </option><option value="Neumología">Neumología</option><option value="Infectología">Infectología</option><option value="Pediatría">Pediatría</option><option value="Anestesiología">Anestesiología</option></select>
      </th>

      
        <th scope="col">Entidad
        @if( Auth::user()->id_rol==1 )
          <select class="form-control mayusculas filtro" name="entidadMunicipio" id="entidadMunicipio">
            <option value="">TODOS</option>
              @foreach ($entidades as $entidad)
                <option value="{{$entidad->id_entidad}}">{{$entidad->entidad}}</option>
              @endforeach          
          </select>
        @endif
        </th>
     

      <th scope="col">Municipio
          <select class="form-control filtro" name="municipio" id="municipio">
            <option value="">TODOS</option>      
          </select>       
      </th>

      <th scope="col">Status
        <select class="form-control filtro" id="accion">
            <option value="">TODOS</option>            
            <option value="5">Seleccionado</option>
            <option value="6">Descartado</option>
            @if( Auth::user()->id_rol==2 )
              <option value="7">PROPIOS</option>
            @endif
            <option value="1">Proceso de contratacion</option>
            <option value="3">Contratado</option>
            <option value="2">Declino Aspirante</option>
            <option value="4">Baja</option>
          </select>      
      </th>
    </tr>
  </thead>
  <tbody>


  @forelse ($paginacion as $registro)

    @php
    
      $descartaron=explode( " ",$registro['descartado_por'] );
      $selecciono=explode( " ",$registro['seleccionado_por'] );   

    @endphp

    <tr>
        <td>{{$registro['id']}}</td>
        <td>{{$registro['curp']}}</td>
        <td>{{$registro['nombre']}} {{$registro['aPaterno']}} {{$registro['aMaterno']}}</td>
        <td>{{$registro->getNivel->detalle}}</td>
        <td>{{$registro['especialidad']}}</td>
                
        <td class="mayusculas">{{ $registro->getEntidad->entidad }}</td>
                            
        <td>{{ $registro->getMunicipio->municipio }}</td>

        <td class='btn-{{$registro["id"]}}'>{!! botonesPostulados(Auth::user()->id_rol,$registro) !!}</td>

    </tr>
  @empty
<tr>
    <td>No hay coincidencias</td>
</tr>
@endforelse

    
</tbody>
</table>

<div class="container">
  <div class="row">
    <div class="col-2">
      Mostrando: {{ $paginacion->total() }}
    </div>

    <div class="col-10">
      {{ $paginacion->links() }}
    </div>
  </div>
</div>


  <!-- Modal -->
  <div class="modal fade bd-example-modal-xl" id="myModalDatosPostulados" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Datos del postulado</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="kt-portlet__head">
          <div class="kt-portlet__head-label">
              <h5 class="kt-portlet__head-title">Datos personales</h5>
          </div>
        </div>
        <hr>        
        <div class="form-row" style="margin-top:20px">
            <div class="col-md-4 mb-3">
                <b>Nombre:</b>
                <div class="nombre">
                  
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <b>Folio:</b>
                <div class="folio">
                  
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <b>Fecha de nacimiento:</b>
                <div class="fechaNacimiento">
                  
                </div>
            </div>
        </div>
        <div class="kt-portlet__head">
          <div class="kt-portlet__head-label">
              <h5 class="kt-portlet__head-title">Entidad en la que desea aplicar</h5>
          </div>
        </div>
        <hr>
        <div class="form-row" style="margin-top:20px">
            <div class="col-md-4 mb-3">
                <b>Entidad:</b>
                <div class="entidad mayusculas">
                  
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <b>Disponibilidad para viajar de forma inmediata a otros municipios:</b>
                <div class="disponibilidad">
                  
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <b>Municipio:</b>
                <div class="municipio">
                  
                </div>
            </div>
        </div>
        <div class="form-row" style="margin-top:20px">
            <div class="col-md-4 mb-3">
                <b>RFC:</b>
                <div class="rfc">
                  

                </div>
            </div>
            <div class="col-md-4 mb-3">
                <b>Curp:</b>
                <div class="curp">
                  
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <b>Nacionalidad:</b>
                <div class="nacionalidad">
                  
                </div>
            </div>            
        </div>
        <div class="kt-portlet__head" style="margin-top:30px">
          <div class="kt-portlet__head-label">
              <h5 class="kt-portlet__head-title">Datos profesionales</h5>
          </div>
        </div>
        <hr>
        <div class="form-row" style="margin-top:20px">
            <div class="col-md-4 mb-3">
                <b>Nivel de escolaridad:</b>
                <div class="nivelEscolaridad">
                  
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <b>Especialidad:</b>
                <div class="especialidad">
                  
                </div>
            </div>      
            <div class="col-md-4 mb-3">
                <b>Otra especialidad:</b>
                <div class="otraEspecialidad">
                  
                </div>
            </div>
        </div>
        <div class="form-row" style="margin-top:20px">
            <div class="col-md-4 mb-3">
                <b>Número de Cédula:</b>
                <div class="cedula">
                  
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <b>Actualmente labora en otra institución o dependencia:</b>
                <div class="labora">
                  
                </div>
            </div>
        </div>
        <div class="kt-portlet__head" style="margin-top:30px">
          <div class="kt-portlet__head-label">
              <h5 class="kt-portlet__head-title">Datos de contacto</h5>
          </div>
        </div>
        <hr>
        <div class="form-row" style="margin-top:20px">
            <div class="col-md-4 mb-3">
                <b>Correo electrónico:</b>
                <div class="correo">
                  
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <b>Télefono Particular:</b>
                <div class="telefonoParticular">
                  
                </div>
            </div>      
            <div class="col-md-4 mb-3">
                <b>Telefono Celular 1</b>
                <div class="celular">
                  
                </div>
            </div>
        </div>
        <div class="form-row" style="margin-top:20px">
            <div class="col-md-4 mb-3">
                <b>Dirección:</b>
                <div class="direccion">
                  
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <b>Codigo postal:</b>
                <div class="codigoPostal">
                  
                </div>
            </div>
        </div>
        <div class="kt-portlet__head" style="margin-top:30px">
          <div class="kt-portlet__head-label">
              <h5 class="kt-portlet__head-title">Archivos</h5>
          </div>
        </div>
        <hr>
        <div class="form-row" style="margin-top:20px">
            <div class="col-md-4 mb-3">
                <b>Ine:</b>
                <div class="ineFile">
               
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <b>Rfc:</b>
                <div class="rfcFile">
                  
                </div>
            </div>      
            <div class="col-md-4 mb-3">
                <b>Cedula</b>
                <div class="cedulaFile">
                  
                </div>
            </div>
        </div>
        <div class="form-row" style="margin-top:20px">
            <div class="col-md-4 mb-3">
                <b>Titulo:</b>
                <div class="tituloFile">
                  
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <b>Certificado:</b>
                <div class="certificadoFile">
                  
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <b>Diploma:</b>
                <div class="diplomaFile">
                  
                </div>
            </div>
        </div>
        
      </div>
      
      <div class="modal-footer">

      <div class="seleccionado-activar d-none">
        <button class="btn btn-focus btn-sm m-btn m-btn--pill btn-success activar postulado"  type="button" >ACTIVAR</button>
      </div>

      <div class="seleccionado-contrato d-none">
      @if( Auth::user()->id_rol==99 )
        <button class="btn btn-focus btn-sm m-btn m-btn--pill btn-success contrato postulado"  type="button" >Contratar</button>
      @endif
        <button class="btn btn-focus btn-sm m-btn m-btn--pill btn-danger descartar postulado" type="button" >Descartar</button>
        <button class="btn btn-focus btn-sm m-btn m-btn--pill btn-warning declino postulado" type="button" >Declinación del aspirante</button>
      </div>

      <div class="seleccionado-proceso d-none">
      @if(Auth::user()->id_rol==1 || Auth::user()->id_rol==3 )
        <button class="btn btn-focus btn-sm m-btn m-btn--pill btn-success proceso postulado"  type="button" >En proceso de Contratacion</button>
      @endif
        <button class="btn btn-focus btn-sm m-btn m-btn--pill btn-danger descartar postulado" type="button" >Descartar</button>
        <button class="btn btn-focus btn-sm m-btn m-btn--pill btn-warning declino postulado" type="button" >Declinación del aspirante</button>
      </div>
      <div class="seleccionado-botones">
        <button class="btn btn-focus btn-sm m-btn m-btn--pill seleccionar postulado" type="button" >Seleccionar</button>
        <button class="btn btn-focus btn-sm m-btn m-btn--pill descartar postulado" type="button" >Descartar</button>
      </div>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-sm" id="modalMensajes" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm" style="max-width: 40%;">
    <div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Modal body text goes here.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary closemessaje" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

  <!-- Modal -->

<script>
$(document).ready(function () {
  $('#dtBasicExample').DataTable();
  $('.dataTables_length').addClass('bs-select');

/////////////rellenar selects AGUSTIN
const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
$("#nivelTecnico").val(urlParams.get('nivel'));
$("#entidadMunicipio").val(urlParams.get('entidad'));
$("#especialidad").val(urlParams.get('especialidad'));
$("#accion").val(urlParams.get('accion'));
$("#nombre").val(urlParams.get('nombre'));

  
@if( Auth::user()->id_rol==1 )
  llenarMunicipio( urlParams.get('entidad') );
@elseif ( Auth::user()->id_rol>1 )
  llenarMunicipio( {{Auth::user()->id_entidad}} );
@endif

setTimeout(function(){ $("#curp").val(urlParams.get('curp')); }, 800);

///////////// AGUSTIN


});

function obtenerDatosPostulado(idPost){
    const id = idPost;
    $.get("postulado/"+id, function(data, status){
        // console.log(data);
        //renderDatosPostulado(data)
       
        $(".nombre").text("").text(data[0].nombre + " " + data[0].aPaterno + " " + data[0].aMaterno)
        $(".folio").text("").text(data[0].folio)
        $(".fechaNacimiento").text("").text(data[0].fechaNacimiento)
        $(".entidad").text("").text(data[0].get_entidad['entidad'])
        if(data[0].disponibilidadViajar){
          $(".disponibilidad").text("").text("Si")
        }else{
          $(".disponibilidad").text("").text("No")
        }
        $(".municipio").text("").text(data[0].get_municipio['municipio'])
        $(".rfc").text("").text(data[0].rfc)
        $(".curp").text("").text(data[0].curp)
        $(".nacionalidad").text("").text(data[0].nacionalidad)
        $(".nacionalidad").text("").text(data[0].nacionalidad)
        $(".nivelEscolaridad").text("").text(data[0].get_nivel['detalle'])
        $(".especialidad").text("").text(data[0].especialidad)
        $(".otraEspecialidad").text("").text(data[0].otraEspecialidad)
        $(".cedula").text("").text(data[0].noCedula)
        $(".labora").text("").text(data[0].noCedula)
        if(data[0].laboraDependencia){
          $(".labora").text("").text("Si")
        }else{
          $(".labora").text("").text("No")
        }
        $(".correo").text("").text(data[0].correo)
        $(".telefonoParticular").text("").text(data[0].telefono)
        $(".celular").text("").text(data[0].celularUno)
        $(".direccion").text("").text(data[0].direccion)
        $(".codigoPostal").text("").text(data[0].codigoPostal)
     /*    $(".ineFile a").attr('href',"").attr('href', "https://convocatoria.issste.gob.mx/storage/"+data[0].ineFile);
        $(".rfcFile a").attr('href',"").attr('href', "https://convocatoria.issste.gob.mx/storage/"+data[0].rfcFile); */
        /* $(".cedulaFile a").attr('href',"").attr('href', "https://convocatoria.issste.gob.mx/storage/"+data[0].cedulaFile); */
/*         $(".tituloFile a").attr('href',"").attr('href', "https://convocatoria.issste.gob.mx/storage/"+data[0].tituloFile);
 */       /*  $(".certificadoFile a").attr('href',"").attr('href', "https://convocatoria.issste.gob.mx/storage/"+data[0].certificadoFile); */
        $(".diplomaFile a").attr('href',"").attr('href', "https://convocatoria.issste.gob.mx/storage/"+data[0].diplomaFile);
        $(".seleccionar").attr("data-id", "").attr("data-id", data[0].id)
        $(".descartar").attr("data-id", "").attr("data-id", data[0].id)
        $(".proceso").attr("data-id", "").attr("data-id", data[0].id)
        $(".declino").attr("data-id", "").attr("data-id", data[0].id)
        $(".contrato").attr("data-id", "").attr("data-id", data[0].id)
        $(".activar").attr("data-id", "").attr("data-id", data[0].id)
        
        
        
        var archivoine = data[0].ineFile; 
        if(archivoine){
          if(archivoine.includes('/tmp/')){
            $(".ineFile").empty()
          }else{
            $(".ineFile").empty()
            // console.log("si traigo INE")
            $(".ineFile").append("<a href='https://convocatoria.issste.gob.mx/storage/" +data[0].ineFile+"' target='_blank'><img alt='Logo' src='./images/documentopdf.png'></a>");
          }
        }else{
          $(".ineFile").empty()
        }

        var archivorfc = data[0].rfcFile; 
        if(archivorfc){
          if(archivorfc.includes('/tmp/')){
            $(".rfcFile").empty()
          }else{
            $(".rfcFile").empty()
            // console.log("si traigo Rfc")
            $(".rfcFile").append("<a href='https://convocatoria.issste.gob.mx/storage/" +data[0].rfcFile+"' target='_blank'><img alt='Logo' src='./images/documentopdf.png'></a>");
          }
        }else{
          $(".rfcFile").empty()
        }

        var archivocedula = data[0].cedulaFile; 
        if(archivocedula){
          if(archivocedula.includes('/tmp/')){
            $(".cedulaFile").empty()
          }else{
            $(".cedulaFile").empty()
            // console.log("si traigo cedula")
            $(".cedulaFile").append("<a href='https://convocatoria.issste.gob.mx/storage/" +data[0].cedulaFile+"' target='_blank'><img alt='Logo' src='./images/documentopdf.png'></a>");
          }
        }else{
          $(".cedulaFile").empty()
        }

        var archivotitulo = data[0].tituloFile; 
        if(archivotitulo){
          if(archivotitulo.includes('/tmp/')){
            $(".tituloFile").empty()
          }else{
            $(".tituloFile").empty()
            // console.log("si traigo titulo")
            $(".tituloFile").append("<a href='https://convocatoria.issste.gob.mx/storage/" +data[0].tituloFile+"' target='_blank'><img alt='Logo' src='./images/documentopdf.png'></a>");
          }
        }else{
          $(".tituloFile").empty()
        }

        var archivocertificado = data[0].certificadoFile; 
        if(archivocertificado){
          if(archivocertificado.includes('/tmp/')){
            $(".certificadoFile").empty()
          }else{
            $(".certificadoFile").empty()
            // console.log("si traigo Certificado")
            $(".certificadoFile").append("<a href='https://convocatoria.issste.gob.mx/storage/" +data[0].certificadoFile+"' target='_blank'><img alt='Logo' src='./images/documentopdf.png'></a>");
          }
        }else{
          $(".certificadoFile").empty()
        }

        var archivodiploma = data[0].diplomaFile; 
        if(archivocertificado){
          if(archivocertificado.includes('/tmp/')){
            $(".diplomaFile").empty()
          }else{
            $(".diplomaFile").empty()
            // console.log("si traigo diploma")
            $(".diplomaFile").append("<a href='https://convocatoria.issste.gob.mx/storage/" +data[0].diplomaFile+"' target='_blank'><img alt='Logo' src='./images/documentopdf.png'></a>");
          }
        }else{
          $(".diplomaFile").empty()
        }
        
        

        $('#myModalDatosPostulados').modal('show')
    });
  } 

$(".postulado, .verPostulado").bind('click', function(e) {        
  e.stopPropagation();
    e.preventDefault();
    e.stopImmediatePropagation();
      
  if( $(e.currentTarget).hasClass('verPostulado') ){
    var seleccionAg=e.currentTarget.dataset.descartado?4:(e.currentTarget.dataset.contratado?3:(e.currentTarget.dataset.proceso?2:(e.currentTarget.dataset.seleccionado?1:0)));

    switch(seleccionAg) {
      case 1:
        $(".seleccionado-botones,.seleccionado-contrato,.seleccionado-activar").addClass("d-none");
        $(".seleccionado-proceso").removeClass("d-none");        
      break;
      case 2:
        $( ".seleccionado-contrato" ).removeClass( "d-none" );
        $( ".seleccionado-botones,.seleccionado-proceso,.seleccionado-activar" ).addClass( "d-none" );
      break;
      case 3:        
        $( ".seleccionado-botones,.seleccionado-proceso,.seleccionado-contrato,.seleccionado-activar" ).addClass( "d-none" );        
      break;  
      case 4:        
        $( ".seleccionado-botones,.seleccionado-proceso,.seleccionado-contrato" ).addClass( "d-none" );
        $( ".seleccionado-activar" ).removeClass( "d-none" );      
      break;                 
      default:
        $( ".seleccionado-botones" ).removeClass( "d-none" );
        $( ".seleccionado-proceso,.seleccionado-contrato,.seleccionado-activar").addClass( "d-none" );
    }

    obtenerDatosPostulado(e.currentTarget.dataset.id)
  }else{
    

$(".closemessaje").bind('click', function(e) {     
  $('#modalMensajes').modal('hide')
  location.reload(); 
});
///////////AGUSTIN
    var estadoPostulado = $(e.currentTarget).hasClass('activar')?5:($(e.currentTarget).hasClass('contrato')?4:($(e.currentTarget).hasClass('descartar')?0:($(e.currentTarget).hasClass('proceso')?2:($(e.currentTarget).hasClass('declino')?3:1))));    
    var clase=e.currentTarget.dataset.id;    
    $.ajax({
      url: 'postulado',
      data:{"_token": "{{ csrf_token() }}","id":e.currentTarget.dataset.id,'estadoPostulado':estadoPostulado},
      dataType: 'json',
      type:'POST',
			success: function(response){
        // console.log(response);
        $(".btn-"+clase).empty();
        $('#myModalDatosPostulados').modal('hide')

        if(response==2){
          $('#modalMensajes .modal-title').text("").html("Atencion <i class='flaticon-warning-sign'></i>")
          $('#modalMensajes .modal-body').text("").text("Ya fue seleccionado por alguien mas")
          $('#modalMensajes').modal('show')
          /* alert('Ya fue seleccionado por alguien mas'); */
          setTimeout(function(){ 
            location.reload(); 
          }, 1300);
        }else{          
            location.reload();          
        }

        
        
			},
			error: function(response){ 
        // console.log('error');
      }
		});
  }
 
  


}); 

$( "em" ).attr( "title" );

/////////////////fitros AGUSTIN
$(".filtro").change(function(){
  var nivel=$("#nivelTecnico").val()?'nivel='+$("#nivelTecnico").val()+'&':'';
  var entidad=$("#entidadMunicipio").val()?'entidad='+$("#entidadMunicipio").val()+'&':'';
  var municipio=$("#municipio").val()?'municipio='+$("#municipio").val()+'&':'';
  var especialidad=$("#especialidad").val()?'especialidad='+$("#especialidad").val()+'&':'';
  var accion=$("#accion").val()?'accion='+$("#accion").val():'';
  window.location.replace(location.protocol + '//' + location.host + location.pathname+'?'+nivel+entidad+municipio+especialidad+accion);
});

function llenarMunicipio(entidad){

  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);

  $.get("municipios/"+entidad, function(data, status){
      $('#municipio').empty()
      $("#municipio").append("<option value=''>TODOS</option>");
      $.each(data, function( index, value ) {        
        $("#municipio").append("<option value='"+value['id']+"'>"+value['municipio']+"</option>");        
      });  

      $("#municipio").val(urlParams.get('municipio'));      

  });
}

$(".limpiar").click(function(){
    // console.log('limpiar');
    window.location.replace(location.protocol + '//' + location.host + location.pathname);
});

// $("#exportarExcel,#exportarExcel1,#exportarExcel2").click(function(e){  
//   var elemento = e.currentTarget.id;
//   var url =elemento=='exportarExcel1'?'puestounidad'+window.location.search:(elemento=='exportarExcel2'?'porusuarios':'reporte'+window.location.search);
//   var texto=e.currentTarget.dataset.texto;
//   // console.log(url);
//   e.stopPropagation();
//     e.preventDefault();
//     e.stopImmediatePropagation();
  
//   $("#"+elemento).attr("disabled","disabled");
//   $("#"+elemento).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> GENERANDO CSV');
  
//   $.ajax({
//         url: url,
//         method: 'GET',
//         xhrFields: {
//             responseType: 'blob'
//         },
//         success: function (data) {
//             var a = document.createElement('a');
//             var url = window.URL.createObjectURL(data);
//             a.href = url;
//             a.download = texto+'.csv';
//             document.body.append(a);
//             a.click();
//             a.remove();
//             window.URL.revokeObjectURL(url);
//             $("#"+elemento).html(texto);
//             $("#"+elemento).removeAttr("disabled");
//         },
//         error: function(){
//           $("#"+elemento).html('Error, intenta de nuevo');
//           $("#"+elemento).removeAttr("disabled");
//         }       
//     });

// });

/////////////////fin filtros AGUSTIN

</script>
@endif
@endsection

