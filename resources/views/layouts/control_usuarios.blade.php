<script>
$("#breadcrumb-title").html('<?=env('APP_NAME')?>');
$("#breadcrumb-title").append(' /Control de Usuario  ');
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="assets/js/scrip_usuarios.js"></script>
<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
      <div class="m-portlet__head-caption">
        <div class="col-xl-12 order-1 order-xl-2  m--align-right" >
         <!--  <a data-toggle="modal" data-target="#edit" id="reg_nue" href="javascript:;" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
            <span><i class="fa fa-users left"></i><span>Nuevo Usuario</span></span>
          </a>
           @include('modales/usuarios/nuevo_usuario') -->
          <div class="m-separator m-separator--dashed d-xl-none"></div>
        </div>
      </div>
    </div>
    <div class="m-portlet__body">
      <table id="usuarios" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Email</th>
            <th>Rol</th>
            <th>Sede</th>
            <th>Eliminar</th>
          </tr>
        </thead>
        
      </table>
     <!--  @include('modales/usuarios/editar_usuario') -->
    </div>

</div>
