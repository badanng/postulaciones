<script>
$("#breadcrumb-title").html('<?=env('APP_NAME')?>');
$("#breadcrumb-title").append(' / Perfil  ');
</script>
<script src="assets/js/scrip_usuarios.js"></script>
<script src="assets/js/tablas_script.js"></script>
<div class="row">
  <div class="col-xl-4 col-lg-4">
    <div class="m-portlet m-portlet--full-height  ">
      <div class="m-portlet__body">
        <div class="m-card-profile">
          <div class="m-card-profile__title m--hide">
            Su perfil
          </div>
          <div class="m-card-profile__pic">
            <!-- imagen perfil -->222
            <div class="kt-avatar kt-avatar--outline kt-avatar--circle" id="kt_apps_user_add_avatar">
              <div class="profile-userpic" id="avatar_actual">              
                <img src="{{ Storage::url($user[0]->avatar)}}" id="avatarImage">
              </div>
              <form action="{{ url('/subir_imagen') }}"  method="post"  id="avatarForm" >
              {{ csrf_field() }}
                <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change avatar">
                <i class="flaticon-photo-camera"></i>
                <input type="file" id="avatarInput" name="avatar" style="display: none">
                </label>
              </form>
            </div>
            <!--fin imagen perfil -->
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xl-8 col-lg-9">
    <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
      <div class="m-portlet__head">
        <div class="m-portlet__head-tools">
          <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
            <li class="nav-item m-tabs__item">
              <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
                <i class="flaticon-share m--hide"></i>
                Configuración de cuenta
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div class="tab-content">
        <div class="tab-pane active" id="m_user_profile_tab_1">
          <fornombre_perfilm class="m-form m-form--fit m-form--label-align-right" id="editar_perfil">
            <div class="m-portlet__body">
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-2 col-form-label" style="line-height: 25px;">
                  Nombre (s)
                </label>
                <div class="col-7">
                  <input class="form-control m-input" type="text" disabled id="nombre_perfil" id="nombre_perfil" name="nombre_perfil" value="{{ ($user[0]->nombre)}}">
                  <span class="m-form__help">
                    El nombre no puede ser editado
                  </span>
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-2 col-form-label" style="line-height: 25px; ">Puesto
                </label>
                <div class="col-7">
                  <input class="form-control m-input" type="text" disabled id="puesto" id="puesto" name="puesto" value="{{($user[0]->puesto)}}">
                  <span class="m-form__help">
                    El puesto no puede ser editado
                  </span>
                </div>
              </div>
              <!-- <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-2 col-form-label" style="line-height: 25px;"></label>
                <div class="col-7">
                  <label>Ubicacion de Dependencia</label>
                </div>
              </div> -->
              <!-- <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-2 col-form-label" style="line-height: 25px;"></label>
                <div class="col-7">
                  <select id="entidad" name="entidad" class="form-control" name="id_entidad" >
                    <option selected value="">Estado</option>           
                    @foreach($ubicaciones as $entidad)
                      <option  value="{{$entidad->id_entidad}}" required>{{ $entidad->entidad}}</option>
                    @endforeach
                  </select>
                </div>
              </div> -->
              <!-- <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-2 col-form-label" style="line-height: 25px;"></label>
                <div class="col-7">
                  <select id="municipio" name="municipio" class="form-control">
                    <option selected value="">Municipio</option>
                  </select>
                </div>
              </div> -->
              <!-- <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-2 col-form-label" style="line-height: 25px;"></label>
                <div class="col-7">
                  <select id="unidad" name="unidad" class="form-control">
                    <option selected value="">Unidad</option>
                  </select>
                </div>
              </div> -->
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-2 col-form-label">
                  Correo
                </label>
                <div class="col-7">
                  <input class="form-control m-input" type="email" disabled id="email" name="email" value="{{($user[0]->email)}}">
                     <span class="m-form__help">
                    El correo no puede ser editado
                  </span>
                </div>
              </div>
               <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-2 col-form-label">
                  Rol
                </label>
                <div class="col-7">
                  <input class="form-control m-input" type="email" disabled id="email" name="email" value="{{($user[0]->rol->nombre_rol)}}" required>
                     <span class="m-form__help">
                    El rol no puede ser editado
                  </span>
                </div>
              </div>

                  <h5>Cambiar Contraseña</h5>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-2 col-form-label">
                  Password Actual
                </label>
                <div class="col-7">
                  <input class="form-control m-input" type="password" id="passwordd" name="passwordd" required>
                  <strong class="txt_passworda"></strong>
                </div>
              </div>

              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-2 col-form-label">
                  Nueva password
                </label>
                <div class="col-7">
                  <input class="form-control m-input" type="password" id="newpassword" name="password" >
                  <strong class="txt_passwordd"></strong>
                </div>
              </div>

              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-2 col-form-label">
                  Confirmar password
                </label>
                <div class="col-7">
                  <input class="form-control m-input" type="password" id="newpasswordf" name="password" >
                  <strong class="txt_passwordd"></strong>
                </div>
              </div>

            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">
              <div class="m-form__actions">
                <div class="row"> &nbsp;&nbsp;
                  <div class="col-7">
                      <a id="guarda_perfil" class="btn btn-sm btn-label-primary btn-bold">
                        Cambiar Contraseña
                      </a>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
