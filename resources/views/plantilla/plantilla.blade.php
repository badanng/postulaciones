<!DOCTYPE html>
<html lang="es-mx" >
    <!-- begin::Head -->
    <head>
        <base href="../">
        <meta charset="utf-8"/>

        <title>Postulaciones</title>
        <meta name="description" content="Latest updates and statistic charts">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        @include('plantilla/script')
    </head>
    <!-- end::Head -->
    <!-- begin::Body -->
    <body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading kt-body" >

        <div id="initpreloader">
           <div id="initloader"></div>
            
        </div>
        <div class="kt-page-loader kt-page-loader--base kt-page-loader--non-block" style="margin-left: -80px; margin-top: -20px;">
            <div class="kt-blockui">
                <span>
                    Cargando ...
                </span>
                <span>
                    <div class="kt-loader m-loader--brand"></div>
                </span>
            </div>
        </div>
        
        <!-- begin:: Page -->
        <!--[html-partial:include:{"file":"partials\/_header-base-mobile.html"}]/-->
            <div id="kt_header_mobile" class="kt-header-mobile kt-header-mobile--fixed " >
                <div class="kt-header-mobile__logo">
                    <a >
                        <img alt="Logo" src="./assets/media/logos/gobierno.png"/>
                    </a>
                </div>
                <div class="kt-header-mobile__toolbar">
                    <button class="kt-header-mobile__toolbar-toggler kt-header-mobile__toolbar-toggler--left" id="kt_aside_mobile_toggler" href="javascript:;"  ><span></span></button>
                </div>
            </div>

            <div class="kt-grid kt-grid--hor kt-grid--root">
                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
                    <!--[html-partial:include:{"file":"partials\/_aside-base.html"}]/-->
                    @include('plantilla/aside-base')
                    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
                        <!--[html-partial:include:{"file":"partials\/_header-base.html"}]/-->
                         @include('plantilla/header-base')
                            <!--[html-partial:include:{"file":"partials\/_subheader-subheader-v1.html"}]/-->
                            <!-- begin:: Content -->
                            <!--[html-partial:include:{"file":"partials\/_content-base.html"}]/-->
                            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
                                <!-- begin:: Content -->
                                <!-- <div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content">
                                    <div >
                                        @yield('content')
                                        @yield('content_perfil')
                                    </div>                            
                                </div> -->
                                @include('plantilla/content-base')
                            <!-- end:: Content -->
                            </div>
                        <!--[html-partial:include:{"file":"partials\/_footer-base.html"}]/-->
                        @include('plantilla/footer-base')
                    </div>
                </div>
            </div>
            <!-- end:: Page -->
            @include('plantilla.script-global')
    </body>
    <!-- end::Body -->
</html>
