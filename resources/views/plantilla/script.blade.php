<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
<script>
      WebFont.load({
        google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
        active: function() {
            sessionStorage.fonts = true;
        }
      });
</script>
 <link href="assets/css/demo12/style.bundle.css" rel="stylesheet" type="text/css" />
<link href="assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

<link href="assets/css/demo12/pages/general/wizard/wizard-4.css" rel="stylesheet" type="text/css" />

<!--Calendario-->
<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />

<!--JQuery-->
<script src="assets/plugins/jquery-3.2.1.min.js"></script>

<!--Plugins JQuery-->
<link href="assets/plugins/bootstrap-touchspin/bootstrap.touchspin.min.css" rel="stylesheet" type="text/css" />
<link href="assets/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />

<!--APP-->
<!-- <link href="css/app.css" rel="stylesheet" type="text/css"> -->

<!--calendar-->
<link href="assets/vendors/custom/jquery-ui/jquery-ui.bundle.css" rel="stylesheet" type="text/css" />
<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />

<!--iconos-->
<!-- <link href="css/svg.css" rel="stylesheet" type="text/css"> -->
<link href="./assets/vendors/custom/vendors/flaticon/flaticon.css" rel="stylesheet" type="text/css" />
<link href="./assets/vendors/custom/vendors/flaticon2/flaticon.css" rel="stylesheet" type="text/css" />

<!--Loader-->
<link href="css/loader.css" rel="stylesheet" type="text/css" />
<script src="assets/js/loader.js"></script>

<!--perfect-scrollbar-->
<link href="assets/vendors/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />

<!--fontawesome5-->
<link href="assets/vendors/vendors/fontawesome5/css/all.min.css" rel="stylesheet" type="text/css" />

<!--Datatables-->
<link href="assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />

<link href="assets/vendors/custom/custom.css" rel="stylesheet" type="text/css" />

<link rel="shortcut icon" href="assets/demo/default/media/img/logo/ISSSTE_logo.png" />
