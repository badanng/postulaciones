<!-- begin:: Content -->
<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    
	      		@yield('content')
	      		@yield('inventario')
	      		@yield('ubicaciones')		
	      		@yield('perfil')
	      		@yield('control-usuarios')	
	      		@yield('roles')	
	      		@yield('fuapDetalles')	
	      		@yield('altaFuap')	
		                         
</div>
<!-- end:: Content -->