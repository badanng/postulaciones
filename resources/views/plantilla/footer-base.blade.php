<!-- begin:: Footer -->
<div class="kt-footer kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
    <div class="kt-footer__copyright">2020 &copy;
    	<a target="_blank" class="kt-link">ISSSTE></a>
    </div>
    <div class="kt-footer__menu">
    </div>
</div>
<!-- end:: Footer -->