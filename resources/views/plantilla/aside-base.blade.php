<style>
.menssaje-general{
	color:#fff;
	padding: 10px;
	word-break: break-word;
	font-weight: bold;
}
</style>
<!-- begin:: Aside -->
<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
<div class="kt-aside kt-aside--fixed kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">
<!-- begin:: Aside -->
	<div class="kt-aside__brand kt-grid__item " id="kt_aside_brand">
	    <div class="kt-aside__brand-logo">
	        <a href="/home">
	            <img alt="Logo" src="./assets/media/logos/gobierno.png">
	        </a>
	    </div>
	    <div class="kt-aside__brand-tools">
	        <button  href="javascript:;" class="kt-aside__brand-aside-toggler" id="kt_aside_toggler"><span></span></button>
	    </div>
	</div>
<!-- end:: Aside -->
<!-- begin:: Aside Menu -->
	<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
	    <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" >
		@auth
	        <ul class="kt-menu__nav ">
	            <li class="kt-menu__section kt-menu__section--first " aria-haspopup="true">
	                <h4 class="kt-menu__section-text">
	                    Componentes
	                </h4>
	                <i class="kt-menu__section-icon flaticon-more-v2"></i>
	            </li>
	            <li class="kt-menu__item kt-menu__item--submenu kt-menu__item--active" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
	                <a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon-network"></i><span class="kt-menu__link-text">Postulaciones</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
	                <div class="kt-menu__submenu ">
	                    <span class="kt-menu__arrow"></span>
	                    <ul class="kt-menu__subnav">
	                        <li class="kt-menu__item kt-menu__item--parent" aria-haspopup="true" >
	                            <span class="kt-menu__link"><span class="kt-menu__link-text">Postulaciones</span></span>
	                        </li>
	                    {{--     {{dd(Auth::user()->menuP())}} --}}
	          	
{{-- 		                    @foreach (Auth::user()->menuPrincipal() as $key => $item)
			                    @if ($item['padre'] != 0)
			                        @break
			                    @endif
												                    	
			                    	@if( $item['prioridad']==1 )
			                    		@include('layouts.sub_menus', ['item' => $item])
			                    	@endif

		                	@endforeach   --}}


						@if( Auth::user()->id_rol!=4 )
	                        <li class="kt-menu__item " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
		                       <a class="kt-menu__link "  href="home">
		                         <i class="kt-menu__link-icon flaticon-interface-9"><span></span></i>
		                         <span class="kt-menu__link-text">Postulaciones</span>
		                       </a>
	                       	</li>
						@endif


							@if( Auth::user()->id_rol==1 || Auth::user()->id_rol==4 )

							<li class="kt-menu__item " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
								<a class="kt-menu__link "  href="contratacion">
								  <i class="kt-menu__link-icon flaticon2-group"><span></span></i>
								  <span class="kt-menu__link-text">Seguimiento</span>
								</a>
							</li>							

							@endif

							<li class="kt-menu__item " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
		                       <a class="kt-menu__link "  href="user/perfil">
		                         <i class="kt-menu__link-icon flaticon2-user"><span></span></i>
		                         <span class="kt-menu__link-text">Mi Perfil</span>
		                       </a>
	                       </li>

	                       
	                       @if( Auth::user()->id_rol==1 )
								<li class="kt-menu__item " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
			                       <a class="kt-menu__link "  href="user/datos_personal">
			                         <i class="kt-menu__link-icon flaticon2-group"><span></span></i>
			                         <span class="kt-menu__link-text">Control de Usuarios</span>
			                       </a>
		                       </li>

							   <!-- <li class="kt-menu__item " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
									<a class="kt-menu__link "  href="reporte">
										<i class="kt-menu__link-icon flaticon2-sheet"><span></span></i>
										<span class="kt-menu__link-text">Reporte</span>
									</a>
								</li> -->

	                       @endif
	                       
                		</ul>
						@endauth	
	                </div>
	            </li>
	        </ul>
@auth			
			<div class="menssaje-general">
				Estimados doctores y doctoras, por favor les pedimos envíen los formatos FM1 a
			 	Brenda Hernández Sánchez al correo: <a href="mailto:brenda.hernandezs@issste.gob.mx">brenda.hernandezs@issste.gob.mx</a> 
			</div>

@if( Auth::user()->id_rol==1 )
    
      <button type="button" id="exportarExcel" data-texto="EXCEL" class="btn btn-primary d-none">EXCEL POST</button>
      <button type="button" id="exportarExcel1" data-texto="CSV PUESTO Y UNIDAD" class="btn btn-success btn-block"><i class="la flaticon-download "></i> CSV PUESTO Y UNIDAD</button>
      <button type="button" id="exportarExcel2" data-texto="CSV USUARIOS" class="btn btn-dark btn-block"><i class="la flaticon-download "></i> CSV USUARIOS</button>
  
@endif
@endauth
	    </div>
	</div>
<!-- end:: Aside Menu -->
</div>
<!-- end:: Aside -->

<script>

$("#exportarExcel,#exportarExcel1,#exportarExcel2").click(function(e){  
  var elemento = e.currentTarget.id;
  var url =elemento=='exportarExcel1'?'puestounidad'+window.location.search:(elemento=='exportarExcel2'?'porusuarios':'reporte'+window.location.search);
  var texto=e.currentTarget.dataset.texto;
  // console.log(url);
  e.stopPropagation();
    e.preventDefault();
    e.stopImmediatePropagation();
  
  $("#"+elemento).attr("disabled","disabled");
  $("#"+elemento).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> GENERANDO CSV');
  
  $.ajax({
        url: url,
        method: 'GET',
        xhrFields: {
            responseType: 'blob'
        },
        success: function (data) {
            var a = document.createElement('a');
            var url = window.URL.createObjectURL(data);
            a.href = url;
            a.download = texto+'.csv';
            document.body.append(a);
            a.click();
            a.remove();
            window.URL.revokeObjectURL(url);
            $("#"+elemento).html(texto);
            $("#"+elemento).removeAttr("disabled");
        },
        error: function(){
          $("#"+elemento).html('Error, intenta de nuevo');
          $("#"+elemento).removeAttr("disabled");
        }       
    });

});

</script>