<!-- begin::Demo Panel -->
<div id="kt_demo_panel" class="kt-demo-panel" >
	<div class="kt-demo-panel__head">
		<h3 class="kt-demo-panel__title">
			Historial
			<!--<small>5</small>-->
		</h3>
		<a id="kt_demo_panel_close" class="kt-demo-panel__close" ><i class="flaticon2-delete"></i></a>
	</div>
	<div class="kt-demo-panel__body">
        <div class="kt-portlet kt-portlet--tabs">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-toolbar">
					<ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-left nav-tabs-line-primary" role="tablist">
						<li class="nav-item">
							<a class="nav-link  active" data-toggle="tab" href="#kt_builder_page" role="tab">
								Historial Equipo
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link " data-toggle="tab" href="#kt_builder_header" role="tab">
								Historial Usuario
							</a>
						</li>
					</ul>
				</div>
			</div>
			<!--begin::Form-->
			<form class="kt-form kt-form--label-right" action="" method="POST">
				<div class="kt-portlet__body">
					<div class="tab-content">
						<div class="tab-pane  active" id="kt_builder_page">
							<div class="kt-section kt-margin-t-30">
								<div class="kt-section__body">
									@foreach($historico as $value)
										<div class="kt-demo-panel__item ">
									        <div class="kt-demo-panel__item-title">                 
									        </div>
									        <div class="kt-demo-panel__item-preview">
									       		<div class="kt-portlet__body">
													<div class="kt-widget4">
														<div class="kt-widget4__item">
															<span class="kt-widget4__icon">
																<i class="flaticon-pie-chart-1 kt-font-info"></i>
															</span>	
															<a  class="kt-widget4__title kt-widget4__title--light">
																Tipo de Movimiento: {{$value->tipoMovimiento->etiqueta}} por  {{$value->tipoBaja->etiqueta}} 
															</a> 
														    <a  class="kt-widget4__title kt-widget4__title--light">
																Numero de Reporte: {{$value->numero_reporte}} 
															</a> 
														     <a  class="kt-widget4__title kt-widget4__title--light">
																Fecha: {{$value->created_at}} 
															</a> 
															
															
															
														</div>
														 <div class="kt-widget4__item">
															<a  class="kt-widget4__title kt-widget4__title--light">
																Equipo: {{$value->equipo_tipo[0]->tipo}} 	</a>
															<a  class="kt-widget4__title kt-widget4__title--light">	Numero de Serie:{{$value->equipo->serie}}</a>
															<a  class="kt-widget4__title kt-widget4__title--light">	Modelo:{{$value->equipo->modelo}} </a> 
															</a>
															@if($value->archivo != null)
															<a  class="flaticon2-file kt-font-success" href="{{ Storage::url($value->archivo)}}" download>
																 Archivo Adjunto
															</a> 
															@endif 																
														</div>
												         <div class="kt-widget4__item">
															<a  class="kt-widget4__title kt-widget4__title--light">
																Usuario Respondable del Movimiento: {{$value->user->nombre}} 
															</a>																
														</div>
													</div>
												</div>

											</div>
										</div>
           							@endforeach
						        </div>
						    </div>
						</div>
						<div class="tab-pane " id="kt_builder_header">
							<div class="kt-section kt-margin-t-30">
						    	<div class="kt-section__body">

						    		@foreach($historico_usuario as $value)
										<div class="kt-demo-panel__item ">
									        <div class="kt-demo-panel__item-title">                 
									        </div>
									        <div class="kt-demo-panel__item-preview">
									       		<div class="kt-portlet__body">
													<div class="kt-widget4">
														<div class="kt-widget4__item">
															<span class="kt-widget4__icon">
																<i class="flaticon-pie-chart-1 kt-font-info"></i>
															</span>	
															<a  class="kt-widget4__title kt-widget4__title--light">
																
															</a> 
														    <a  class="kt-widget4__title kt-widget4__title--light">
																Le Pertenecio a: {{$value->usuario->nombre}} 
															</a> 
														     <a  class="kt-widget4__title kt-widget4__title--light">
																Fecha: {{$value->fecha_fin}} 
															</a> 
															
															
															
														</div>
													
													</div>
												</div>

											</div>
										</div>
           							@endforeach
								
						        </div>
						    </div>
						</div>
					</div>
				</div>
				<div class="kt-portlet__foot">
					<div class="kt-form__actions">

					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>
		<!--end::Portlet-->
	</div>
</div>
<!-- end::Demo Panel -->
