<!--Inicio::Base Scripts -->
<script src="{{asset('assets/vendors/base/vendors.bundle.js')}}" />
<script src="{{asset('assets/demo/default/base/scripts.bundle.js')}}" />
<script src="{{asset('assets/js/demo12/pages/dashboard.js')}}" />
<script src="{{asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" />

<!--Internacionalizacion para calendario datepicker -->
<script src="assets/vendors/base/spanish_ui.js" type="text/javascript"></script>
<!--Inicio::Base Scripts -->
<script src="{{asset('assets/vendors/perfect-scrollbar/dist/perfect-scrollbar.js')}}" />
<!--Slim Scroll-->
<script src="{{asset('assets/plugins/jquery-slimscroll/jquery.slimscroll.js')}}" />

<script src="{{asset('assets/plugins/bootstrap-sessiontimeout/bootstrap-session-timeout.js')}}" />
<script src="{{asset('assets/plugins/bootstrap-touchspin/bootstrap.touchspin.min.js')}}" />
<script src="{{asset('assets/plugins/buzz/buzz.min.js')}}" />
<script src="{{asset('assets/plugins/bootstrap-toastr/toastr.min.js')}}" />
<!--Sweet Alert-->
<script src="{{asset('assets/plugins/bootstrap-sweetalert/sweetalert.min.js')}}" />

<!--Dropzone-->
<script src="assets/js/dropzone.js"></script>

@auth
<!--Datatables-->
<script src="assets/plugins/datatables/datatables.js" type="text/javascript"></script>

<!-- script src="assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script> -->
<!-- <script src="assets/demo/default/custom/crud/datatables/basic/basic.js" type="text/javascript"></script> -->
<script src="assets/js/tablas_script.js"></script>
@endauth

<!--fontawesome-pro-5.0.0 load everything!!-->
<script src="{{asset('assets/fontawesome-pro-5.0.0/svg-with-js/js/fontawesome-all.js')}}" />

<!--Autocomplete-->
<script src="{{asset('assets/js/jquery.autocomplete.js')}}" />
<script src="{{asset('https://js.pusher.com/4.1/pusher.min.js')}}" />

<!--Select Boostrap-->
<script src="assets/demo/default/custom/components/forms/widgets/bootstrap-select.js" type="text/javascript"></script>


<!--begin::Global Theme Bundle(used by all pages) -->
<script src="{{asset('./assets/js/demo12/scripts.bundle.js')}}" />
<!--end::Global Theme Bundle -->

<!--begin::Page Vendors(used by this page) -->
<!-- <script src="{{asset('//maps.google.com/maps/api/js?key=AIzaSyBTGnKT7dt597vo9QgeQ7BFhvSRP4eiMSM')}}" />
<script src="{{asset('./assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" />
<script src="{{asset('./assets/vendors/custom/gmaps/gmaps.js')}}" /> -->

<!--end::Page Scripts -->
<!--mis Librerias-->
@auth
<script src="assets/js/controllers.js"></script>
<script src="assets/js/generales.js"></script>

    <!-- <script src="assets/js/inicio.js"></script> -->
    <script src="assets/js/roles.js"></script>
    <script src="assets/js/usuario.js"></script>

<script src="assets/js/common.js"></script>
<script src="assets/js/catalogo.js"></script>
<!-- <script src="assets/js/sistemas.js"></script> -->
<script src="assets/js/wow.min.js"></script>
<script src="assets/js/pdf.js"></script>
<script src="assets/js/estadisticas.js"></script>
<!-- <script src="assets/js/scrip_usuarios.js"></script> -->
<script src="assets/js/abc_usuario.js"></script>
<script src="assets/js/mi_perfil.js"></script>

<script src="assets/js/inventario.js"></script>
<script src="assets/js/detallesunidades.js"></script>
@endauth
