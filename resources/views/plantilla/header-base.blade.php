<!-- begin:: Header -->
<div id="kt_header" class="kt-header kt-grid__item kt-header--fixed " >
<!-- begin: Header Menu -->
	<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
	<div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
	    <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile kt-header-menu--layout-default " >
	        <ul class="kt-menu__nav ">
	            <li class="kt-menu__item kt-menu__item--active " aria-haspopup="true">
	                <a  class="kt-menu__link "><span id="breadcrumb-title" class="kt-menu__link-text"></span></a>
	            </li>
	        </ul>
	    </div>
	</div>
<!-- end: Header Menu -->
<!-- begin:: Header Topbar -->
	<div class="kt-header__topbar">
<!--begin: User Bar -->
	    <div class="kt-header__topbar-item kt-header__topbar-item--user">
	        <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
	            <div class="kt-header__topbar-user">
	                <span class="kt-hidden kt-header__topbar-welcome kt-hidden-mobile">ISSSTE/Centro Mando,</span> <span class="kt-hidden kt-header__topbar-username kt-hidden-mobile">
	                </span>
					@auth
	                <img  id="avatarImageP" alt="Pic" class="kt-radius-100" src="{{ Auth::user()->getAvatarUrl()}}" />
					@endauth
	                    <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
	                <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold kt-hidden">S</span>
	            </div>
	        </div>
	        <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">
				<div class="kt-user-card kt-user-card--skin-light kt-notification-item-padding-x">
				@auth
				    <div class="kt-user-card__name">{{Auth::user()->nombre}}</div>
				@endauth
				    <div class="kt-notification__custom kt-space-between">
					{{ csrf_field() }}
					    <a class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air loginfnxx"
					    href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="la la-power-off "></i>
					    </a>
					    <form id="logout-form" action="{{ route('logout') }}"
					    method="POST"
					    style="display: none;">
					        @csrf
					    </form>
  					</div>
				</div>
	        </div>
	    </div>    
<!--end: User Bar -->
	</div>
<!-- end:: Header Topbar -->
</div>
<!-- end:: Header -->