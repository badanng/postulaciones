
@extends('plantilla.plantilla')
@section('content')

<style>
@media (min-width: 576px){
  .modal-dialog {
      max-width: 70%;
      margin: 1.75rem auto;
  }
}
table{
  margin-left: auto;
  margin-right: auto;
  display: block;
}
</style>
 
 <h5 style="margin-top: 78px; margin-bottom: 15px;">
 Lista de postulados - Mostrando: {{ $paginacion->total() }}
 </h5>

<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nombre</th>
      <th scope="col">Nivel Tecnico
        <select class="form-control filtro" id="nivelTecnico" name="nivelTecnico">
            <option value="">TODOS</option>
            <optgroup label="Secundaria/Bachillerato con curso afín">
                <option value="Camillero">Camillero</option>
            </optgroup>
            <optgroup label="Bachillerato con carrera técnica">
                <option value="Técnico en Inhaloterapia">Técnico en Inhaloterapia</option>
                <option value="Técnico Radiólogo">Técnico Radiólogo</option>
                <option value="Técnico Laboratorista">Técnico Laboratorista</option>
                <option value="Tecnico en Enfermeria con Postecnico">Técnico en Enfermería con Postécnico</option>
            </optgroup>
            <optgroup label="Licenciatura">
                <option value="Médico general">Médico general</option>
                <option value="Medico general con especialidad">Médico general con especialidad</option>
                <option value="Enfermería">Enfermería</option>
                <option value="Enfermeria con especiadidad/postecnico">Enfermería con especiadidad/postécnico</option>
            </optgroup>
            <optgroup label="Ingeniero">
                <option value="Biomédico">Biomédico</option>
                <option value="Químico">Químico</option>
            </optgroup>
        </select>
      </th>
      <th scope="col">Entidad
        <select class="form-control filtro" name="entidadMunicipio" id="entidadMunicipio">
          <option value="">TODOS</option>
            @foreach ($entidades as $entidad)
              <option value="{{$entidad->id_entidad}}">{{$entidad->entidad}}</option>
            @endforeach          
        </select>      
      </th>
      <th scope="col">Especialidad
        <select class="form-control filtro" id="especialidad" name="especialidad">
        <option value="">TODOS</option>
        <option value="Urgencias">Urgencias</option><option value="Terapia Intensiva">Terapia Intensiva</option><option value="Medicina Interna ">Medicina Interna </option><option value="Neumología">Neumología</option><option value="Infectología">Infectología</option><option value="Pediatría">Pediatría</option><option value="Anestesiología">Anestesiología</option></select>
      </th>

      <th scope="col">STATUS
        <select class="form-control filtro" id="accion">
          <option value="">TODOS</option>
          <option value="1">Seleccionado</option>
          <option value="2">Descartado</option>          
        </select>
      </th>
    </tr>
  </thead>
  <tbody>

@forelse ($registros as $registro)
  <tr>

      <td>{{$registro['id'] }} </td>
      <td>{{$registro['nombre']}} {{$registro['aPaterno']}} {{$registro['aMaterno']}}</td>
      <td>{{$registro['nivelTecnico']}}</td>
      <td>{{$registro['entidadMunicipio']}}</td>
      <td>{{$registro['especialidad']}}</td>

    

      @php    
        $botonag=$registro['seleccionado']==1?'<button type="button" class="btn bg-success">Seleccionado</button>':($registro['estadoPostulado']==0?'<button type="button" class="btn bg-warning">Descartado</button>':"<button  class='btn btn-focus btn-sm m-btn m-btn--pill verPostulado' data-id=". $registro['id'] ." type='button'>Ver</button>"); 
      @endphp
      <td class='btn-{{$registro["id"]}}'>{!! $botonag !!}</td>

  </tr>
@empty
<tr>
    <td>No hay coincidencias</td>
</tr>
@endforelse    

    
</tbody>
</table>

{{ $paginacion->links() }}

Mostrando: {{ $paginacion->total() }}

  <!-- Modal -->
  <div class="modal fade bd-example-modal-xl" id="myModalDatosPostulados" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Datos del postulado</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="kt-portlet__head">
          <div class="kt-portlet__head-label">
              <h5 class="kt-portlet__head-title">Datos personales</h5>
          </div>
        </div>
        <hr>
        <div class="form-row" style="margin-top:20px">
            <div class="col-md-4 mb-3">
                <b>Nombre:</b>
                <div class="nombre">
                  
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <b>Folio:</b>
                <div class="folio">
                  
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <b>Fecha de nacimiento:</b>
                <div class="fechaNacimiento">
                  
                </div>
            </div>
        </div>
        <div class="kt-portlet__head">
          <div class="kt-portlet__head-label">
              <h5 class="kt-portlet__head-title">Entidad en la que desea aplicar</h5>
          </div>
        </div>
        <hr>
        <div class="form-row" style="margin-top:20px">
            <div class="col-md-4 mb-3">
                <b>Entidad:</b>
                <div class="entidad">
                  
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <b>Disponibilidad para viajar de forma inmediata a otros municipios:</b>
                <div class="disponibilidad">
                  
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <b>Municipio:</b>
                <div class="municipio">
                  
                </div>
            </div>
        </div>
        <div class="form-row" style="margin-top:20px">
            <div class="col-md-4 mb-3">
                <b>RFC:</b>
                <div class="rfc">
                  

                </div>
            </div>
            <div class="col-md-4 mb-3">
                <b>Curp:</b>
                <div class="curp">
                  
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <b>Nacionalidad:</b>
                <div class="nacionalidad">
                  
                </div>
            </div>            
        </div>
        <div class="kt-portlet__head" style="margin-top:30px">
          <div class="kt-portlet__head-label">
              <h5 class="kt-portlet__head-title">Datos profesionales</h5>
          </div>
        </div>
        <hr>
        <div class="form-row" style="margin-top:20px">
            <div class="col-md-4 mb-3">
                <b>Nivel de escolaridad:</b>
                <div class="nivelEscolaridad">
                  
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <b>Especialidad:</b>
                <div class="especialidad">
                  
                </div>
            </div>      
            <div class="col-md-4 mb-3">
                <b>Otra especialidad:</b>
                <div class="otraEspecialidad">
                  
                </div>
            </div>
        </div>
        <div class="form-row" style="margin-top:20px">
            <div class="col-md-4 mb-3">
                <b>Número de Cédula:</b>
                <div class="cedula">
                  
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <b>Actualmente labora en otra institución o dependencia:</b>
                <div class="labora">
                  
                </div>
            </div>
        </div>
        <div class="kt-portlet__head" style="margin-top:30px">
          <div class="kt-portlet__head-label">
              <h5 class="kt-portlet__head-title">Datos de contacto</h5>
          </div>
        </div>
        <hr>
        <div class="form-row" style="margin-top:20px">
            <div class="col-md-4 mb-3">
                <b>Correo electrónico:</b>
                <div class="correo">
                  
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <b>Télefono Particular:</b>
                <div class="telefonoParticular">
                  
                </div>
            </div>      
            <div class="col-md-4 mb-3">
                <b>Telefono Celular 1</b>
                <div class="celular">
                  
                </div>
            </div>
        </div>
        <div class="form-row" style="margin-top:20px">
            <div class="col-md-4 mb-3">
                <b>Dirección:</b>
                <div class="direccion">
                  
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <b>Codigo postal:</b>
                <div class="codigoPostal">
                  
                </div>
            </div>
        </div>
        <div class="kt-portlet__head" style="margin-top:30px">
          <div class="kt-portlet__head-label">
              <h5 class="kt-portlet__head-title">Archivos</h5>
          </div>
        </div>
        <hr>
        <div class="form-row" style="margin-top:20px">
            <div class="col-md-4 mb-3">
                <b>Ine:</b>
                <div class="ineFile">
               
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <b>Rfc:</b>
                <div class="rfcFile">
                  
                </div>
            </div>      
            <div class="col-md-4 mb-3">
                <b>Cedula</b>
                <div class="cedulaFile">
                  
                </div>
            </div>
        </div>
        <div class="form-row" style="margin-top:20px">
            <div class="col-md-4 mb-3">
                <b>Titulo:</b>
                <div class="tituloFile">
                  
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <b>Certificado:</b>
                <div class="certificadoFile">
                  
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <b>Diploma:</b>
                <div class="diplomaFile">
                  
                </div>
            </div>
        </div>
        
      </div>
      
      <div class="modal-footer">
      <button class="btn btn-focus btn-sm m-btn m-btn--pill seleccionar" data-id=""  type="button">Seleccionar</button>
      <button class="btn btn-focus btn-sm m-btn m-btn--pill descartar" data-id="" data-estadoPostulado="0" type="button">Descartar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
  <!-- Modal -->

<script>
$(document).ready(function () {
  $('#dtBasicExample').DataTable();
  $('.dataTables_length').addClass('bs-select');

/////////////rellenar selects AGUSTIN
const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
$("#nivelTecnico").val(urlParams.get('nivel'));
$("#entidadMunicipio").val(urlParams.get('entidad'));
$("#especialidad").val(urlParams.get('especialidad'));
$("#accion").val(urlParams.get('accion'));
///////////// AGUSTIN

});

function obtenerDatosPostulado(idPost){
    const id = idPost;
    $.get("postulado/"+id, function(data, status){
        console.log(data);
        //renderDatosPostulado(data)
       
        $(".nombre").text("").text(data[0].nombre + " " + data[0].aPaterno + " " + data[0].aMaterno)
        $(".folio").text("").text(data[0].folio)
        $(".fechaNacimiento").text("").text(data[0].fechaNacimiento)
        $(".entidad").text("").text(data[0].entidadMunicipio)
        if(data[0].disponibilidadViajar){
          $(".disponibilidad").text("").text("Si")
        }else{
          $(".disponibilidad").text("").text("No")
        }
        $(".municipio").text("").text(data[0].municipio)
        $(".rfc").text("").text(data[0].rfc)
        $(".curp").text("").text(data[0].curp)
        $(".nacionalidad").text("").text(data[0].nacionalidad)
        $(".nacionalidad").text("").text(data[0].nacionalidad)
        $(".nivelEscolaridad").text("").text(data[0].nivelTecnico)
        $(".especialidad").text("").text(data[0].especialidad)
        $(".otraEspecialidad").text("").text(data[0].otraEspecialidad)
        $(".cedula").text("").text(data[0].noCedula)
        $(".labora").text("").text(data[0].noCedula)
        if(data[0].laboraDependencia){
          $(".labora").text("").text("Si")
        }else{
          $(".labora").text("").text("No")
        }
        $(".correo").text("").text(data[0].correo)
        $(".telefonoParticular").text("").text(data[0].telefono)
        $(".celular").text("").text(data[0].celularUno)
        $(".direccion").text("").text(data[0].direccion)
        $(".codigoPostal").text("").text(data[0].codigoPostal)
     /*    $(".ineFile a").attr('href',"").attr('href', "https://convocatoria.issste.gob.mx/storage/"+data[0].ineFile);
        $(".rfcFile a").attr('href',"").attr('href', "https://convocatoria.issste.gob.mx/storage/"+data[0].rfcFile); */
        /* $(".cedulaFile a").attr('href',"").attr('href', "https://convocatoria.issste.gob.mx/storage/"+data[0].cedulaFile); */
/*         $(".tituloFile a").attr('href',"").attr('href', "https://convocatoria.issste.gob.mx/storage/"+data[0].tituloFile);
 */       /*  $(".certificadoFile a").attr('href',"").attr('href', "https://convocatoria.issste.gob.mx/storage/"+data[0].certificadoFile); */
        $(".diplomaFile a").attr('href',"").attr('href', "https://convocatoria.issste.gob.mx/storage/"+data[0].diplomaFile);
        $(".seleccionar").attr("data-id", "").attr("data-id", data[0].id)
        $(".descartar").attr("data-id", "").attr("data-id", data[0].id)
        
        var archivoine = data[0].ineFile; 
        if(archivoine){
          if(archivoine.includes('/tmp/')){
            $(".ineFile").empty()
          }else{
            $(".ineFile").empty()
            console.log("si traigo INE")
            $(".ineFile").append("<a href='https://convocatoria.issste.gob.mx/storage/" +data[0].ineFile+"' target='_blank'><img alt='Logo' src='./images/documentopdf.png'></a>");
          }
        }else{
          $(".ineFile").empty()
        }

        var archivorfc = data[0].rfcFile; 
        if(archivorfc){
          if(archivorfc.includes('/tmp/')){
            $(".rfcFile").empty()
          }else{
            $(".rfcFile").empty()
            console.log("si traigo Rfc")
            $(".rfcFile").append("<a href='https://convocatoria.issste.gob.mx/storage/" +data[0].rfcFile+"' target='_blank'><img alt='Logo' src='./images/documentopdf.png'></a>");
          }
        }else{
          $(".rfcFile").empty()
        }

        var archivocedula = data[0].cedulaFile; 
        if(archivocedula){
          if(archivocedula.includes('/tmp/')){
            $(".cedulaFile").empty()
          }else{
            $(".cedulaFile").empty()
            console.log("si traigo cedula")
            $(".cedulaFile").append("<a href='https://convocatoria.issste.gob.mx/storage/" +data[0].cedulaFile+"' target='_blank'><img alt='Logo' src='./images/documentopdf.png'></a>");
          }
        }else{
          $(".cedulaFile").empty()
        }

        var archivotitulo = data[0].tituloFile; 
        if(archivotitulo){
          if(archivotitulo.includes('/tmp/')){
            $(".tituloFile").empty()
          }else{
            $(".tituloFile").empty()
            console.log("si traigo titulo")
            $(".tituloFile").append("<a href='https://convocatoria.issste.gob.mx/storage/" +data[0].tituloFile+"' target='_blank'><img alt='Logo' src='./images/documentopdf.png'></a>");
          }
        }else{
          $(".tituloFile").empty()
        }

        var archivocertificado = data[0].certificadoFile; 
        if(archivocertificado){
          if(archivocertificado.includes('/tmp/')){
            $(".certificadoFile").empty()
          }else{
            $(".certificadoFile").empty()
            console.log("si traigo Certificado")
            $(".certificadoFile").append("<a href='https://convocatoria.issste.gob.mx/storage/" +data[0].certificadoFile+"' target='_blank'><img alt='Logo' src='./images/documentopdf.png'></a>");
          }
        }else{
          $(".certificadoFile").empty()
        }

        var archivodiploma = data[0].diplomaFile; 
        if(archivocertificado){
          if(archivocertificado.includes('/tmp/')){
            $(".diplomaFile").empty()
          }else{
            $(".diplomaFile").empty()
            console.log("si traigo diploma")
            $(".diplomaFile").append("<a href='https://convocatoria.issste.gob.mx/storage/" +data[0].diplomaFile+"' target='_blank'><img alt='Logo' src='./images/documentopdf.png'></a>");
          }
        }else{
          $(".diplomaFile").empty()
        }
        
        

        $('#myModalDatosPostulados').modal('show')
    });
  } 

$(".descartar,.seleccionar, .verPostulado").bind('click', function(e) {        
  e.stopPropagation();
    e.preventDefault();
    e.stopImmediatePropagation();
  if (e.currentTarget.className === "btn btn-focus btn-sm m-btn m-btn--pill verPostulado"){
    obtenerDatosPostulado(e.currentTarget.dataset.id)
  }else{
    


Fuente: https://www.iteramos.com/pregunta/62156/jquery-click-se-dispara-dos-veces-al-hacer-clic-en-la-etiqueta
    var estadoPostulado = e.currentTarget.dataset.estadopostulado?0:1;
    var clase=e.currentTarget.dataset.id;
    $.ajax({
      url: 'postulado',
      data:{"_token": "{{ csrf_token() }}","id":e.currentTarget.dataset.id,'estadoPostulado':estadoPostulado},
      dataType: 'json',
      type:'POST',
			success: function(response){
        // console.log(response);
        $(".btn-"+clase).empty();
        $('#myModalDatosPostulados').modal('hide')
        if(response==1){
          $(".btn-"+clase).append('<button type="button" class="btn">Seleccionado</button>');
        }else{
          $(".btn-"+clase).append('<button type="button" class="btn">Descartado</button>');
        }
        
			},
			error: function(response){ 
        console.log('error');
      }
		});
  }
 
  


}); 

$( "em" ).attr( "title" );


/////////////////fitros AGUSTIN
$(".filtro").change(function(){
  var nivel=$("#nivelTecnico").val()?'nivel='+$("#nivelTecnico").val()+'&':'';
  var entidad=$("#entidadMunicipio").val()?'entidad='+$("#entidadMunicipio").val()+'&':'';
  var especialidad=$("#especialidad").val()?'especialidad='+$("#especialidad").val()+'&':'';
  var accion=$("#accion").val()?'accion='+$("#accion").val():'';
  window.location.replace(location.protocol + '//' + location.host + location.pathname+'?'+nivel+entidad+especialidad+accion);

});
/////////////////fin filtros AGUSTIN
</script>

@endsection

