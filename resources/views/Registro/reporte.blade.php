


@extends('plantilla.plantilla')
@section('content')

<div id="contenedor_principal" >
	<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
</head>
<body>
<h2>POSTULADOS</h2>
<table id="dtOrderExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
  <thead>
    <tr>
      <th class="th-sm">Folio
      </th>
      <th class="th-sm">Nombre
      </th>
      <th class="th-sm">Nivel tecnico
      </th>    
      <th class="th-sm">Estado
      </th>
      <th class="th-sm">Especialidad
      </th>              
      <th class="th-sm">RFC
        <img src="" alt="">
      </th>
      <th class="th-sm">INE
      </th>
      <th class="th-sm">TITULO
      </th>
      <th class="th-sm">CEDULA
      </th>
      <th class="th-sm">DIPLOMA
      </th>
      <th class="th-sm">CERTIFICADO
      </th>
    </tr>
  </thead>
  <tbody>
  @forelse($reportes as $reporte)	


	<tr>
		<td>{{$reporte['id']}}</td>
		<td>{{$reporte['nombre']}} {{$reporte['aPaterno']}} {{$reporte['aMaterno']}} </td>
    <td>{{$reporte['nivelTecnico']}}</td>
    <td>{{$reporte['entidadMunicipio']}}</td>
    <td>{{$reporte['especialidad']}}</td>
		<td>
        @isset($reporte['rfcFile'])
            <a href="https://convocatoria.issste.gob.mx/storage/{{$reporte['rfcFile']}}" target="_blank">
                <img alt="Logo" src="./images/documentopdf.png">
            </a>
        @endisset
        </td>
		<td>
            @isset($reporte['ineFile'])
            <a href="https://convocatoria.issste.gob.mx/storage/{{$reporte['ineFile']}}" target="_blank">
                <img alt="Logo" src="./images/documentopdf.png">
            </a>
            @endisset
        </td>
		<td>
            @isset($reporte['tituloFile'])
            <a href="https://convocatoria.issste.gob.mx/storage/{{$reporte['tituloFile']}}" target="_blank">
                <img alt="Logo" src="./images/documentopdf.png">
            </a>
            @endisset
        </td>
        <td>
            @isset($reporte['cedulaFile'])
                <a href="https://convocatoria.issste.gob.mx/storage/{{$reporte['cedulaFile']}}" target="_blank">
                    <img alt="Logo" src="./images/documentopdf.png">
                </a>
             @endisset
        </td>
        <td>
            @isset($reporte['diplomaFile'])
            <a href="https://convocatoria.issste.gob.mx/storage/{{$reporte['diplomaFile']}}" target="_blank">
                <img alt="Logo" src="./images/documentopdf.png">
            </a>
             @endisset
        </td>
        <td>
            @isset($reporte['certificadoFile'])
                <a href="https://convocatoria.issste.gob.mx/storage/{{$reporte['certificadoFile']}}" target="_blank">
                    <img alt="Logo" src="./images/documentopdf.png">
                </a>
             @endisset
        </td>
    </td>
  </tr>
	@empty

	<tr>
		<td>NO REGISTRO PARA EL REPORTE POSTULADOS</td>
	</tr>

  @endforelse
  </tbody>
  <tfoot>
    <tr>
    <th class="th-sm">Folio
      </th>
      <th class="th-sm">Nombre
      </th>
      <th class="th-sm">Nivel tecnico
      </th>
      <th class="th-sm">RFC
      </th>
      <th class="th-sm">INE
      </th>
      <th class="th-sm">TITULO
      </th>
      <th class="th-sm">CEDULA
      </th>
      <th class="th-sm">DIPLOMA
      </th>
      <th class="th-sm">CERTIFICADO
      </th>
    </tr>
  </tfoot>
</table>

</div>   


<script>


$(document).ready(function () {
  $('#dtOrderExample').DataTable({
    initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                // console.log( column.index() );
                if(column.index()==2 || column.index()==3 || column.index()==4){
                  var select = $('<select><option value=""></option></select>')
                      .appendTo(  $(column.header())  )
                      .on( 'change', function () {
                          var val = $.fn.dataTable.util.escapeRegex(
                              $(this).val()
                          );
  
                          column
                              .search( val ? '^'+val+'$' : '', true, false )
                              .draw();
                      } );
  
                  column.data().unique().sort().each( function ( d, j ) {
                      select.append( '<option value="'+d+'">'+d+'</option>' )
                  } );
                }
            } );
        },

  "order": [[ 3, "desc" ]]
  });
  $('.dataTables_length').addClass('bs-select');
});

$(".descartar,.seleccionar").bind('click', function(e) {        
  // console.log(e.currentTarget.dataset);    

  var estadoPostulado = e.currentTarget.dataset.estadopostulado?0:1;
  $.ajax({
      url: 'postulado',
      data:{"_token": "{{ csrf_token() }}","id":e.currentTarget.dataset.id,'estadoPostulado':estadoPostulado},
      dataType: 'json',
      type:'POST',
			success: function(response){
        console.log(response);
			},
			error: function(response){ 
        console.log('error');
      }
		});


}); 


</script>
@endsection



