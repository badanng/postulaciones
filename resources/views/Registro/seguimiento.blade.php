@extends('plantilla.plantilla')
@section('content')

@if( isset($postulado) )

    @if( $postulado[0]->editado==1 || $postulado[0]->id > 4318 )
        <h4 class="text-center">{{$postulado[0]->nombre}} {{$postulado[0]->aPaterno}} {{$postulado[0]->aMaterno}}</h4>
        <h3 class="text-center"> {{ $postulado[0]->seleccionado==1?'Fuiste seleccionado':'Tu perfil no lo han visto' }}</h3>
    @else

<style>

.no-mostrar,
.validaNacionalidad,
.no_mostrar_especialidad,
.no_mostrar_otra_especialidad,
.nomostrarfile{
    display: none;
}
input,
select{
    border-color: blue !important;
}

.checkLabora{
    display: none;
}
.bannerHome{
    display:block;
    width: 100%;          
}
.bannerHome img{    
    width: 55%;
    margin: 0 auto;
    display: block;
    padding-top: 36px;
    padding-bottom: 10px;
}
</style>
<pre>-</pre>
  

<form class="needs-validation" action="{{url('/seguimiento')}}" method="POST" enctype='multipart/form-data'>
    @csrf
    @method('PATCH')
    <input type="hidden" name="folio" value="{{$postulado[0]->folio}}">
    <div class="form-row" style="margin-top:20px">
        <div class="col-md-4 mb-3">
    @foreach($postulado as $pos)
       Nombre: {{$pos->nombre}} {{$pos->aPaterno}} {{$pos->aMaterno}} <br>
       Folio: {{$pos->folio}} <br>
       </div>
    @endforeach
    <div class="form-row" style="margin-top:20px">
        <h4>Favor de revalidar tus datos - ESTA ACCION SOLO SE PUEDE HACER UNA VEZ</h4>
    </div>
    </div>
    <div class="form-row" style="margin-top:20px">
        <div class="col-md-4 mb-3">
            <label for="">Seleccione la entidad para su registro </label>
            <select class="form-control" name="entidadMunicipio" id="entidadMunicipio" required>
                
            </select>
        </div>
        <div class="col-md-7 mb-3">
            <label for="telefono_contacto">Disponibilidad para viajar de forma inmediata a otros municipios.</label>
            <select class="form-control" name="disponibilidadViajar" required>
                <option value="">Seleccionar</option>
                <option value="1">si</option>
                <option value="0">no</option>
            </select>
        </div> 
    </div>
    <div class="form-row" style="margin-top:20px">
        <div class="col-md-4 mb-3">
            <label for="municipio">Municipio</label>
            <select class="form-control" name="municipio" id="municipio" required>
                <option value="">Seleccionar</option>
            </select>
            
        </div>
    </div>
    <div class="form-row" style="margin-top:20px">
        <div class="col-md-4 mb-3">
            <label for="nivelTecnico">Nivel de escolaridad</label>
            <select class="form-control" id="nivelTecnico" name="nivelTecnico" required>
                <option value="">Seleccionar</option>
                <optgroup label="Secundaria/Bachillerato con curso afín">
                    <option value="Camillero">Camillero</option>
                </optgroup>
                <optgroup label="Bachillerato con carrera técnica">
                    <option value="Técnico en Inhaloterapia">Técnico en Inhaloterapia</option>
                    <option value="Técnico Radiólogo">Técnico Radiólogo</option>
                    <option value="Técnico Laboratorista">Técnico Laboratorista</option>
                    <option value="Tecnico en Enfermeria con Postecnico">Técnico en Enfermería con Postécnico</option>
                </optgroup>
                <optgroup label="Licenciatura">
                    <option value="Médico general">Médico general</option>
                    <option value="Medico general con especialidad">Médico general con especialidad</option>
                    <option value="Enfermería">Enfermería</option>
                    <option value="Enfermeria con especiadidad/postecnico">Enfermería con especiadidad/postécnico</option>
                </optgroup>
                <optgroup label="Ingeniero">
                    <option value="Biomédico">Biomédico</option>
                    <option value="Químico">Químico</option>
                </optgroup>

            </select>
        </div>
        <div class="col-md-4 mb-3">
            <label for="licenciatura" class="no_mostrar_especialidad">Especialidad</label>
            <select class="form-control no_mostrar_especialidad" id="especialidad" name="especialidad">
                
            </select>
        </div>
        
    </div>
    <div class="form-row" style="margin-top:20px">
        <div class="col-md-4 mb-3">
            <div class="form-check">
                <input type="checkbox" class="form-check-input" id="enTramite">
                <label class="form-check-label" for="enTramite">En tramite <br> <b>(debe contar con documento de comprobación de cédula en trámite)</b></label>
            </div>
            <label for="noCedula">Número de Cédula</label>
            <input type="text" class="form-control"  id="noCedula" name="noCedula" value="" onkeyup="javascript:this.value=this.value.toUpperCase();" maxlength="10" disabled>
        </div>
    </div>

    <div class="form-row" style="margin-top:20px">
        <div class="col-md-4 mb-3">
            <label for="ineFile">INE</label>
            <input type="file" class="form-control-file" id="ineFile" name="ineFile" maxlength="18">
        </div>
        <div class="col-md-4 mb-3">
            <label for="rfcFile">RFC</label>
            <input type="file" class="form-control-file" id="rfcFile" name="rfcFile" maxlength=>
        </div>
        <div class="col-md-4 mb-3">
            <label for="cedulaFile">Cedula</label>
            <input type="file" class="form-control-file" id="cedulaFile" name="cedulaFile">
        </div>
    </div>  
    <div class="form-row" style="margin-top:20px">
        <div class="col-md-4 mb-3">
            <label for="tituloFile">Titulo</label>
            <input type="file" class="form-control-file" id="tituloFile" name="tituloFile">
        </div>
        <div class="col-md-4 mb-3">
            <label for="certificado" class="certificado">Certificado</label>
            <input type="file" class="form-control-file certificado" id="certificado" name="certificadoFile">
        </div>
        <div class="col-md-4 mb-3">
            <label for="diplomaDelCurso" class="diplomaDelCurso certificado">Diploma del curso</label>
            <input type="file" class="form-control-file certificado" id="diplomaDelCurso" name="diplomaFile">
        </div>
    </div>
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">7.- Manifiesto bajo protesta de decir verdad</h3>
        </div>
    </div>
    <div class="form-row" style="margin-top:20px">
        <div class="col-md-7 mb-3">
            <label for="codigoPostal">Hago constar, bajo protesta de decir verdad que no me encuentro inhabilitado para desempeñarme en el servicio público, que no he ejercido acción legal en contra del Instituto de Seguridad y Servicios Sociales de los Trabajadores de los Estados, ni soy trabajador de base o confianza en el mismo.</label>
            <div class="form-check">
            <input type="hidden" name="terminosDeLabora" value="0">
            <input class="form-check-input" type="checkbox" value="1" id="condicionmanifiesto" name="manifiesto">
            <label class="form-check-label" for="condicionmanifiesto">
                Acepto
            </label>
            </div>
        </div>
    </div>
    <div class="form-row" style="margin-top:20px">
        <div class="col-md-12 mb-3">
            <span style="font-size: 10px; text-align: justify; display: block">
                El ISSSTE será responsable del tratamiento de los datos personales proporcionados por usted, los cuales serán protegidos con base en lo dispuesto por la Ley General de Protección de Datos Personales en Posesión de Sujetos Obligados, y demás normatividad que resulte aplicable. Los datos personales recabados serán utilizados para identificar plenamente al participante y verificar que cumple con cada uno de los requisitos mencionados en la convocatoria. Se informa que no se recabarán datos personales sensibles ni se realizarán transferencias de datos personales, salvo aquellas que sean necesarias para atender requerimientos de información de una autoridad competente que se encuentren debidamente fundados y motivados.
            </span>
        </div>
    </div>
    <label for="comprobar"></label>
    
    <button class="btn btn-primary" id="comprobar" type="submit" disabled>Guardar</button>
</form>
@endif 
@else
        
    <form class="needs-validation" action="{{url('/seguimiento')}}" method="POST">
        @csrf
        <label for="comprobar">Comprobar Estatus</label>
        <input type="text" class="form-control" id="comprobar" name="comprobar" value="" required>
        <button class="btn btn-primary" type="submit">enviar</button>
    </form>

@endif    

<script>
$(document).ready(function() {
    var tecnicoenEnfermeriaConPostecnico = {
    val1 : 'Medicina Interna',
    val2 : 'Urgencias',
    val3 : 'Cuidados Intensivos',
    val4 : 'Pediatría',
    val5 : 'Quirúrgica',
    val6 : 'Terapia Intensiva',
    val7 : 'Anestesista Certificada'
}
medicoGeneralConEspecialidad = {
    val1: 'Urgencias',
    val2: 'Terapia Intensiva',
    val3: 'Medicina Interna ',
    val4: 'Neumología',
    val5: 'Infectología',
    val6: 'Pediatría',
    val7: 'Anestesiología',

};
enfermeriaConEspeciadidadPostecnico = {
    val1: 'Medicina Interna ',
    val2: 'Cuidados Intensivos',
    val3: 'Pediatría',
    val4: 'Quirúrgica',
    val5: 'Terapia Intensiva',
    val6: 'Anestesista Certificada',
    val7: 'Urgencias',
}
var selectNivelTecnico = $('#especialidad');
$('#nivelTecnico').change(function() {
        if ($('#nivelTecnico').val() == "Tecnico en Enfermeria con Postecnico")
        {
            $(".no_mostrar_especialidad, .no_mostrar_otra_especialidad").css("display", "block");
            $('#especialidad').empty()
            $.each(tecnicoenEnfermeriaConPostecnico, function(val, text) {
                selectNivelTecnico.append(
                    $('<option></option>').val(text).html(text)
                );
            });
            $("#diplomaDelCurso, #certificado").css("display", "block")
            $(".certificado").removeClass('nomostrarfile')
            $("#diplomaDelCurso, #certificado").css("display", "block")
            $("#noCedula").attr('disabled','disabled')
        };
        if ($('#nivelTecnico').val() == "Medico general con especialidad")
        {
            $(".no_mostrar_especialidad, .no_mostrar_otra_especialidad").css("display", "block");
            $('#especialidad').empty()
            $.each(medicoGeneralConEspecialidad, function(val, text) {
                
                selectNivelTecnico.append(
                    $('<option></option>').val(text).html(text)
                );
            }); 
        };
        if ($('#nivelTecnico').val() == "Enfermeria con especiadidad/postecnico")
        {
            $('#especialidad').empty()
            $(".no_mostrar_especialidad, .no_mostrar_otra_especialidad").css("display", "block");
            $.each(enfermeriaConEspeciadidadPostecnico, function(val, text) {
                
                selectNivelTecnico.append(
                    $('<option></option>').val(text).html(text)
                );
            }); 
            
        };
        if ($('#nivelTecnico').val() == "Camillero")
        {
            $('#especialidad').empty()
            $(".no_mostrar_especialidad, .no_mostrar_otra_especialidad").css("display", "none");  
            $("#noCedula").css("display", "").val("Curso relacionado con el puesto avalado por autoridad o institución con firma y sello").attr('disabled','disabled');
            $("#diplomaDelCurso, #certificado").css("display", "block") 
            $(".certificado").removeClass('nomostrarfile')
        };
        if ($('#nivelTecnico').val() == "Técnico en Inhaloterapia")
        {
            $('#especialidad').empty()
            $("#noCedula").css("display", "block").val("").attr('disabled','disabled');
            $(".no_mostrar_especialidad, .no_mostrar_otra_especialidad").css("display", "none");
            $("#diplomaDelCurso, #certificado").css("display", "block")
            $(".certificado").removeClass('nomostrarfile')
            
        };
        if ($('#nivelTecnico').val() == "Técnico Radiólogo")
        {
            $('#especialidad').empty()
            $("#noCedula").css("display", "block").val("").removeAttr('placeholder').attr('disabled','disabled');
            $(".no_mostrar_especialidad, .no_mostrar_otra_especialidad").css("display", "none");
            $("#diplomaDelCurso, #certificado").css("display", "block")
            $(".certificado").removeClass('nomostrarfile')
        };
        if ($('#nivelTecnico').val() == "Técnico Laboratorista")
        {
            $('#especialidad').empty()
            $("#noCedula").css("display", "block").val("").attr('disabled','disabled');
            $(".no_mostrar_especialidad, .no_mostrar_otra_especialidad").css("display", "none");
            $("#diplomaDelCurso, #certificado").css("display", "block")
            $(".certificado").removeClass('nomostrarfile')
        };
        if ($('#nivelTecnico').val() == "Médico general")
        {
            $('#especialidad').empty();
            $("#noCedula").css("display", "block").val("").removeAttr('disabled');
            $(".no_mostrar_especialidad").css("display", "none");
            $("#diplomaDelCurso, #certificado, .nomostrarfile").css("display", "none")
            $(".certificado").addClass('nomostrarfile')
            

        };
        if ($('#nivelTecnico').val() == "Enfermería")
        {
            $('#especialidad').empty();
            $("#noCedula").css("display", "block").val("").removeAttr('disabled');
            $(".no_mostrar_especialidad").css("display", "none");
            $("#diplomaDelCurso, #certificado, .nomostrarfile").css("display", "none")
            $(".certificado").addClass('nomostrarfile')
        };
        if ($('#nivelTecnico').val() == "Biomédico")
        {
            $('#especialidad').empty()
            $("#noCedula").css("display", "block").val("").removeAttr('disabled');
            $(".no_mostrar_especialidad, .no_mostrar_otra_especialidad").css("display", "none");
            $("#diplomaDelCurso, #certificado, .nomostrarfile").css("display", "none")
            $(".certificado").addClass('nomostrarfile')
        };
        if ($('#nivelTecnico').val() == "Químico")
        {
            $('#especialidad').empty()
            $("#noCedula").css("display", "block").val("").removeAttr('disabled');
            $(".no_mostrar_especialidad, .no_mostrar_otra_especialidad").css("display", "none");
            $("#diplomaDelCurso, #certificado, .nomostrarfile").css("display", "none")
            $(".certificado").addClass('nomostrarfile')
        };
    });

    $('#enTramite').click(function() {
        if ($("#enTramite").is(':checked') ) {
            $('#noCedula').attr('disabled','disabled').val("En tramite");
        } else {
            $('#noCedula').removeAttr('disabled').val("")
        }
    });
    /* Comprobar Cedula */
    $('#condicionmanifiesto').click(function() {
        if ($("#condicionmanifiesto").is(':checked') ) {
            $('#comprobar').removeAttr('disabled')
        } else {
            $('#comprobar').attr('disabled', 'disabled');
            
        }
    });
});

function renderSelectentidad(Entidad){
    var enditad = $('#entidadMunicipio');
    $('#entidadMunicipio').empty();
    // console.log(Entidad)

    enditad.append('<option value="" >Seleccionar</option>');

    $.each(Entidad, function(val, text) {
        enditad.append(
            $('<option></option>').val(text.id_entidad).html(text.entidad)
        );
        // console.log(text.id_entidad)
        // console.log(text.entidad)
    }); 

}


$.get("entidades", function(data, status){
    // console.log(data);
    renderSelectentidad(data)
});

/* Select municipio */
function renderSelectMunicipios(datosMunicipios){
        var municipios = $('#municipio');
        $('#municipio').empty()
        municipios.append('<option value="" >Seleccionar</option>');
        $.each(datosMunicipios, function(val, text) {
            municipios.append(
                $('<option></option>').val(text.municipio).html(text.municipio)
            );
        }); 

    }
    function pedirMunicipoEntidad(Entidad){
        const enditad = Entidad;
            $.get("municipios/"+enditad, function(data, status){
               // console.log(data);
                renderSelectMunicipios(data)
            });
    }

    $('#entidadMunicipio').change(function() {
        var entidadSeleccionada = $('#entidadMunicipio').val();
        pedirMunicipoEntidad(entidadSeleccionada);
    });


    $(".descartar,.seleccionar").bind('click', function(e) {        
    // console.log(e.currentTarget.dataset);    

    var estadoPostulado = e.currentTarget.dataset.estadopostulado?0:1;
    

    }); 
/* Select municipio */
</script>
@endsection