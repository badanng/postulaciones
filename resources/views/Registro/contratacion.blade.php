@extends('plantilla.plantilla')
@section('content')

@if( Auth::user()->id_rol==1 || Auth::user()->id_rol==4 )
 <h5 style="margin-top: 78px; margin-bottom: 15px;">
 Lista de postulados - Mostrando: {{ $paginacion->total() }}
 </h5>

 <div class="container">
  <div class="row">
    <div class="col-sm">      

      <form>
        <div class="form-group" style="text-align: left;">          
          <div class="btn-group" role="group">      
            <input type="text" class="form-control" placeholder="BUSCAR POR CURP" id="curp" name="curp" required >
            <button type="button" class="btn btn-secondary limpiar">X</button>
            <button type="submit" class="btn btn-primary"><i class="la flaticon-search-1 "></i> BUSCAR CURP</button>
          </div>
        </div>
      </form>   

    </div>
     
    <div class="col-sm">

    <form>
        <div class="form-group" style="text-align: left;">          
          <div class="btn-group" role="group">      
            <input type="text" class="form-control" placeholder="BUSCAR POR NOMBRE" id="nombre" name="nombre" required >
            <button type="button" class="btn btn-secondary limpiar">X</button>
            <button type="submit" class="btn btn-primary"><i class="la flaticon-search-1 "></i> BUSCAR NOMBRE</button>
          </div>
        </div>
      </form>   

    </div>
     
  </div>

  @if( Auth::user()->id_rol==1 )
  <div class="btn-group float-right d-none" role="group">    
      <button type="button" id="exportarExcel" data-texto="EXCEL" class="btn btn-primary d-none">EXCEL POST</button>
      <button type="button" id="exportarExcel1" data-texto="CSV PUESTO Y UNIDAD" class="btn btn-success"><i class="la flaticon-download "></i> CSV PUESTO Y UNIDAD</button>
      <button type="button" id="exportarExcel2" data-texto="CSV USUARIOS" class="btn btn-dark"><i class="la flaticon-download "></i> CSV USUARIOS</button>
  </div>
  @endif

</div>



<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">CURP</th>
      <th scope="col">Nombre</th>

      <th scope="col">Nivel Tecnico
        <select class="form-control filtro" id="nivelTecnico" name="nivelTecnico">
            <option value="">TODOS</option>            
            <optgroup label="Secundaria/Bachillerato con curso afín">
                <option value="1">Camillero</option>
                <option value="2">Asistente de cocina</option>
            </optgroup>
            <optgroup label="Bachillerato con carrera técnica">
                <option value="3">Asistente de cocina</option>
                <option value="4">Técnico en Inhaloterapia</option>
                <option value="5">Técnico Radiólogo</option>
                <option value="6">Técnico Laboratorista</option>
                <option value="7">Técnico en Enfermería con Postécnico</option>
            </optgroup>
            <optgroup label="Licenciatura">
                <option value="8">Médico general</option>
                <option value="9">Médico general con especialidad</option>
                <option value="10">Enfermería</option>
                <option value="11">Enfermería con especiadidad/postécnico</option>
            </optgroup>
            <optgroup label="Ingeniero">
                <option value="12">Biomédico</option>
                <option value="13">Químico</option>
            </optgroup>
        </select>
      </th>

      <th scope="col">Especialidad
        <select class="form-control filtro" id="especialidad" name="especialidad">
        <option value="">TODOS</option>
        <option value="Urgencias">Urgencias</option><option value="Terapia Intensiva">Terapia Intensiva</option><option value="Medicina Interna ">Medicina Interna </option><option value="Neumología">Neumología</option><option value="Infectología">Infectología</option><option value="Pediatría">Pediatría</option><option value="Anestesiología">Anestesiología</option></select>
      </th>

      
        <th scope="col">Entidad
        @if( Auth::user()->id_rol==1 )
          <select class="form-control mayusculas filtro" name="entidadMunicipio" id="entidadMunicipio">
            <option value="">TODOS</option>
              @foreach ($entidades as $entidad)
                <option value="{{$entidad->id_entidad }}">{{ $entidad->entidad }}</option>
              @endforeach          
          </select>
        @endif
        </th>
     

      <th scope="col">Municipio
          <select class="form-control filtro" name="municipio" id="municipio">
            <option value="">TODOS</option>      
          </select>       
      </th>

      <th scope="col">Accion
        <select class="form-control filtro" id="accion">
          <option value="">TODOS</option>            
          <option value="5">Seleccionado</option>        
          <option value="3">Contratado</option>
          <option value="4">Baja</option>
        </select>           
        
      </th>
    </tr>
  </thead>
  <tbody>


  @forelse ($paginacion as $registro)



    @php
    
      $descartaron=explode( " ",$registro['descartado_por'] );
      $selecciono=explode( " ",$registro['seleccionado_por'] );       

    @endphp

    <tr>
        <td>{{$registro['id']}}</td>
        <td>{{$registro['curp']}}</td>
        <td>{{$registro['nombre']}} {{$registro['aPaterno']}} {{$registro['aMaterno']}}</td>
        <td>{{$registro->getNivel->detalle}}</td>
        <td>{{$registro['especialidad']}}</td>
                
        <td class="mayusculas">{{ $registro->getEntidad->entidad }}</td>
                            
        <td>{{ $registro->getMunicipio->municipio }}</td>

        <td class='btn-{{$registro["id"]}}'>
          {!! botonesPostulados(Auth::user()->id_rol,$registro,0) !!}
        </td>

        

    </tr>
    
  @empty
<tr>
    <td>No hay coincidencias</td>
</tr>
@endforelse

    
</tbody>
</table>

<div class="container">
  <div class="row">
    <div class="col-2">
      Mostrando: {{ $paginacion->total() }}
    </div>

    <div class="col-10">
      {{ $paginacion->links() }}
    </div>
  </div>
</div>



<!-- Modal -->
<div class="modal fade bd-example-modal-xl" id="myModalDatosPostulados" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Terminar proceso</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <input id="tipo_ag" name="tipo_ag" type="hidden" value="" disabled>   
      <form class="needs-validation" novalidate id="formDatosPostulados">
        @csrf           
        <div class="form-row">
          <div class="form-group col-md-6">
          <input type="hidden" class="identificador" name="id_persona">
            <label for="nombrepostulado">Nombre</label>
            <input type="text" placeholder="" class="form-control nombrepostulado" disabled name="nombrepostulado">
          </div>
          <div class="form-group col-md-6">
            <label for="curp">Curp</label>
            <input type="text" class="form-control curp" name="curp" disabled>
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="numeroDePlaza">Número de plaza</label>
            <input type="text" placeholder="Ejemplo: 000000" class="form-control numeroDePlaza" name="numeroDePlaza" max="6" maxlength="6" pattern="[0-9]{10}">
            <small class="text-danger plaza-error d-none">Número de plaza no puede estar vacio</small>
          </div>
          <div class="form-group col-md-6">
            <label for="numeroDeEmpleado">Número de empleado</label>
            <input type="text" class="form-control numeroDeEmpleado" name="numeroDeEmpleado" max="6" maxlength="6" pattern="[0-9]{10}">
            <small class="text-danger empleado-error d-none">Número de empleado no puede estar vacio</small>
          </div>
        </div>
          <div class="form-row">
            <div class="form-group col-md-12">
              <label for="unidadMedicaAdscrita">Unidad Medica de adscripión</label>
              <select class="form-control unidadMedicaAdscrita" name="unidadMedicaAdscrita">
                <option value="">Seleccionar unidad medica</option>                                
                @foreach ($unidades as $unidad)
                  <option value="{{$unidad->id}}">{{$unidad->unidad}}</option>
                @endforeach
              </select>
              <small class="text-danger unidad-error d-none">Seleccione unidad medica</small>
            </div>
            <!-- <div class="form-group col-md-6">
              <label for="claveAdscripcionUnidadMedica">Clave de adscripción de la unidad medica</label>
              <input type="text" class="form-control" id="claveAdscripcionUnidadMedica" name="claveAdscripcionUnidadMedica" max="" maxlength="" pattern="[0-9]{10}">
            </div> -->
          </div>
          <!-- <div class="form-row">
            <div class="form-group col-md-6">
              <label for="tipoServicio">Tipo de servicio</label>
              <select class="form-control" id="tipoServicio" name="tipoServicio">
                <option disabled selected value>Seleccionar tipo de servicio</option>
                <option>Urgencias</option>
                <option>Hospitalizacion</option>
              </select>
            </div>
            <div class="form-group col-md-6">
              <label for="tipo">Tipo</label>
              <select class="form-control" id="tipo" name="tipo">
                <option disabled selected value>Seleccionar tipo</option>
                <option>A</option>
                <option>B</option>
              </select>
            </div>
          </div> -->
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="fechaInicioContrato" class="col-form-label">Fecha de alta</label>
              <input class="form-control fechaInicioContrato" type="date" value="" name="fechaInicioContrato">
              <small class="text-danger fechai-error d-none">Debe establecer una fecha de alta</small>
            </div>
            <div class="form-group col-md-6">
            <label for="fechaFinContrato" class="col-form-label">Fecha de baja</label>
              <input class="form-control fechaFinContrato" type="date" value="" name="fechaFinContrato" >
              <small class="text-danger fechaf-error d-none">La fecha de baja debe ser mayor o igual a la fecha de alta</small>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              
            </div>
            <div class="form-group col-md-6">
              <label for="tipo">Motivo de la baja</label>
              
              <!-- <input type="text" name="motivoBaja" class="form-control motivoBaja" max="191" maxlength="191"> -->

              <select class="form-control motivoBaja" name="motivoBaja">
                <option value="">Seleccionar motivo</option>                                
                @foreach ($catBajas as $baja)
                  <option value="{{$baja->id}}">{{$baja->nombre}}</option>
                @endforeach
              </select>
              <small class="text-danger motivo-error d-none">Debe establecer un motivo</small>
            </div>
          </div>
          <button type="button" class="btn btn-primary float-right btnFormSave">Guardar</button>
        </form>
      </div>
      <div class="modal-footer">
        <div class="wrapper_buttons">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function () {

/////////////rellenar selects AGUSTIN
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  $("#nivelTecnico").val(urlParams.get('nivel'));
  $("#entidadMunicipio").val(urlParams.get('entidad'));
  $("#especialidad").val(urlParams.get('especialidad'));
  $("#accion").val(urlParams.get('accion'));
  $("#nombre").val(urlParams.get('nombre'));

@if( Auth::user()->id_rol==1 )
  llenarMunicipio( urlParams.get('entidad') );
@elseif ( Auth::user()->id_rol>1 )
  llenarMunicipio( {{Auth::user()->id_entidad}} );
@endif

setTimeout(function(){ $("#curp").val(urlParams.get('curp')); }, 800);


/////////////////fitros AGUSTIN
$(".filtro").change(function(){
  var nivel=$("#nivelTecnico").val()?'nivel='+$("#nivelTecnico").val()+'&':'';
  var entidad=$("#entidadMunicipio").val()?'entidad='+$("#entidadMunicipio").val()+'&':'';
  var municipio=$("#municipio").val()?'municipio='+$("#municipio").val()+'&':'';
  var especialidad=$("#especialidad").val()?'especialidad='+$("#especialidad").val()+'&':'';
  var accion=$("#accion").val()?'accion='+$("#accion").val():'';
  window.location.replace(location.protocol + '//' + location.host + location.pathname+'?'+nivel+entidad+municipio+especialidad+accion);
});

function llenarMunicipio(entidad){

  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
 
  $.get("municipios/"+entidad, function(data, status){
      $('#municipio').empty()
      $("#municipio").append("<option value=''>TODOS</option>");
      $.each(data, function( index, value ) {        
        $("#municipio").append("<option value='"+value['id']+"'>"+value['municipio']+"</option>");        
      });  

      $("#municipio").val(urlParams.get('municipio'));      

  });
}


$(".limpiar").click(function(){
    window.location.replace(location.protocol + '//' + location.host + location.pathname);
});

  //Datos postuldos
  $(".btnseguimiento, .btn-danger").bind('click', function(e) {        
    e.stopPropagation();
    e.preventDefault();
    e.stopImmediatePropagation();
     obtenerDatosPostulado(e.currentTarget.dataset.id)
  }); 

  function obtenerDatosPostulado(idPost){
    const id = idPost;
    $.get("postulado/"+id, function(data, status){
      // console.log(data)
      $(".nombrepostulado").val("").val(data[0].nombre + " " + data[0].aPaterno + " " + data[0].aMaterno)
      $(".curp").val("").val(data[0].curp)
      $(".identificador").val("").val(data[0].id)
        //Proceso contratado
        switch(data[0].status_proceso){
        case "1": // en proceso de contratacion
          // Aun no termina el proceso
          $(".nombrepostulado").val("").val(data[0].nombre + " " + data[0].aPaterno + " " + data[0].aMaterno);
          $(".curp").val("").val(data[0].curp);
          $(".identificador").val("").val(data[0].id);
          $(".numeroDePlaza").val("").prop({disabled: false});
          $(".numeroDeEmpleado").val("").prop({disabled: false});
          $(".motivoBaja").val("").prop({disabled: true});
          $('.unidadMedicaAdscrita').val('');
          $(".fechaInicioContrato").val("").prop({disabled: false});
          $(".fechaFinContrato").val("").prop({disabled: true});
          $("#tipo_ag").val(1);
          break;
        case 2:
          // console.log("por ahora no existo") //no se esta utilizando estado dos es declinado
          break;
        case "3": //contratado
          $(".nombrepostulado").val("").val(data[0].nombre + " " + data[0].aPaterno + " " + data[0].aMaterno)
          $(".curp").val("").val(data[0].curp)  
          $(".identificador").val("").val(data[0].id)
          $(".numeroDePlaza").val("").val(data[0].contratacion.numeroDePlaza).prop({disabled: false});
          $(".numeroDeEmpleado").val("").val(data[0].contratacion.numeroDeEmpleado).prop({disabled: false})
          $(".motivoBaja").val("").prop({disabled: false});
          $('.unidadMedicaAdscrita').val(data[0].contratacion.unidadMedicaAdscrita);
          $(".fechaInicioContrato").val("").val(data[0].contratacion.fechaInicioContrato);//.prop({disabled: true});
          $(".fechaFinContrato").prop({disabled: false});
          $("#tipo_ag").val(3);          
          break;
        case "4": // es baja        
          $(".nombrepostulado").val("").val(data[0].nombre + " " + data[0].aPaterno + " " + data[0].aMaterno)
          $(".curp").val("").val(data[0].curp)  
          $(".identificador").val("").val(data[0].id)
          $(".numeroDePlaza").val("").val(data[0].contratacion.numeroDePlaza);//.prop({disabled: true});
          $(".numeroDeEmpleado").val("").val(data[0].contratacion.numeroDeEmpleado);//.prop({disabled: true});
          $(".motivoBaja").val("").val(data[0].contratacion.motivoBaja);//.prop({disabled: true});
          $('.unidadMedicaAdscrita').val(data[0].contratacion.unidadMedicaAdscrita);//.prop({disabled: true}); //reset select
          $(".fechaInicioContrato").val("").val(data[0].contratacion.fechaInicioContrato);//.prop({disabled: true});
          $(".fechaFinContrato").val("").val(data[0].contratacion.fechaFinContrato);//.prop({disabled: true});
          $("#tipo_ag").val(4);
          break;
        default:   //es null 
          // Aun no termina el proceso
          $(".nombrepostulado").val("").val(data[0].nombre + " " + data[0].aPaterno + " " + data[0].aMaterno);
          $(".curp").val("").val(data[0].curp);
          $(".identificador").val("").val(data[0].id);
          $(".numeroDePlaza").val("").prop({disabled: false});
          $(".numeroDeEmpleado").val("").prop({disabled: false});
          $(".motivoBaja").val("").prop({disabled: true});
          $('.unidadMedicaAdscrita').val('');
          $(".fechaInicioContrato").val("").prop({disabled: false});
          $(".fechaFinContrato").val("").prop({disabled: true});
          $("#tipo_ag").val(1);
          break;
        }
        setTimeout(function (){
          $('#myModalDatosPostulados').modal('show');
          $( ".plaza-error,.empleado-error,.unidad-error,.fechai-error,.fechaf-error,.motivo-error" ).addClass( "d-none" );          
        }, 100); 
    });
  }

});


/* Envio formulario terminar proceso */

$(".btnFormSave").bind('click', function(e) { 
  e.stopPropagation();
  e.preventDefault();
  e.stopImmediatePropagation();   

  var tipo_ag=$('#tipo_ag').val();
  $( ".plaza-error,.empleado-error,.unidad-error,.fechai-error,.fechaf-error,.motivo-error" ).addClass( "d-none" );

  var plaza=(!$('.numeroDePlaza').val())?$( ".plaza-error" ).removeClass( "d-none" ):0;
  var empleado=(!$('.numeroDeEmpleado').val())?$( ".empleado-error" ).removeClass( "d-none" ):0;
  var unidad=(!$('.unidadMedicaAdscrita').val())?$( ".unidad-error" ).removeClass( "d-none" ):0;
  var fechai=(!$('.fechaInicioContrato').val())?$( ".fechai-error" ).removeClass( "d-none" ):0;


  if( plaza==0 && empleado==0 && unidad==0 && fechai==0 ){    
    var f2=$('.fechaFinContrato').val(),validarFecha=true,motivo=1;    
    if(f2){
      var f1=$('.fechaInicioContrato').val();
      validarFecha=( new Date(f1).getTime() <= new Date(f2).getTime()  )?true:false;  
      motivo=(!$('.motivoBaja').val())?0:1;    
    }

    if(validarFecha && motivo==1){

      $.ajax({
        url: './guardar_contratacion',
        data:$("#formDatosPostulados").serialize(),
        dataType: 'json',
        type:'POST',
        success: function(data){          
          $('#myModalDatosPostulados').modal('hide')
          setTimeout(function(){            
            location.reload(); 
          }, 300);
        },
        error: function(data){ 
          // console.log('error');
        }
      });

    }else{
      if(!validarFecha){
        $( ".fechaf-error" ).removeClass( "d-none" );
      }
      if(motivo==0){
        $( ".motivo-error" ).removeClass( "d-none" );
      }
    }

  }else{
    // console.log('comprueba datos');
  }


});


/* Envio formulario terminar proceso */


</script>
@else
<H3 class="text-center">No tienes los permisos</H3>
@endif

@endsection