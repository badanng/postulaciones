<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        date_default_timezone_set('America/Mexico_City'); //Para que de el horario local en Base de datos
        Schema::defaultStringLength(191);  // --> PARA DEFINIR UTF8MB4 QUE ES PARA EMOJIS 
    }                                       // -->SOLUCIONA PROBLEMA DE MIGRACION
}
