<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\DatosPostulado;
use App\Models\User;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Artisan;
// use Illuminate\Database\Seeder;

// use Database\Seeds\CatMunicipioSeeder;

class matchMunicipios extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'matchMunicipios {entidades} {municipios}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';    
    public $match,$sanear,$regionales,$sanearRegionales,$nivelesTecnicos;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        include(app_path().'/Helpers/config.php');  
        $this->match=$match;
        $this->sanear=$sanear;
        $this->regionales=$regionales;
        $this->sanearRegionales=$sanearRegionales;
        $this->nivelesTecnicos=$nivelesTecnicos;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*
if($this->argument('entidades')==1){        
        $this->line('<fg=red;bg=yellow>::::::::::::::::::::INICIO ENTIDADES::::::::::::::::::::</>');
        // DatosPostulado::where('entidadMunicipio','>',8)        
        // ->update([ 'entidadMunicipio'=> DB::raw('entidadMunicipio+1') ]);
                
        // DatosPostulado::where('entidadMunicipio',52)->update([ 'entidadMunicipio'=> 9 ]);        

        // $this->info( 'fin Sanitización ENTIDADES - INICIA Sanitización USUARIOS' );

        // User::where('id_entidad','>',8)        
        // ->update([ 'id_entidad'=> DB::raw('id_entidad+1') ]);   
        
        // User::where('id_entidad',52)->update([ 'id_entidad'=> 9 ]);  
        // $this->info( 'Fin Sanitización USUARIOS' );
        
        DB::table('cat_entidad')->truncate();        

        Artisan::call('db:seed', [
            '--class' => 'CatEntidadSeeder'
        ]);  
        
        Artisan::output();
        $this->info( 'Termino seeder para Entidades' );
}        
        
        $this->line('<fg=red;bg=yellow>:::::::::::MUNICIPIOS:::::::::::</>');

if($this->argument('municipios')<1){             
        Artisan::call('migrate');    
        
        $this->info( 'MIGRANDO.....................' );

        DB::table('cat_municipio')->truncate();

        Artisan::call('db:seed', [
            '--class' => 'CatMunicipioSeeder'
        ]);         

        $this->info( 'Se completaron los seeder para municipios' );
}        

  
        foreach ($this->match as &$value) {
            DatosPostulado::where('entidadMunicipio', $value[0])
            ->where('municipio', $value[1])
            ->update(['municipio' => $value[2]]);
            $this->info( 'FIN MATCH -> '.$value[1] ); 
        }


        $this->line('<fg=red;bg=yellow>::::::Inicia Sanitización Municipios:::::::</>');
                
        foreach ($this->sanear as &$value) {
            DatosPostulado::whereIn('id',$value[0])
            ->update(['municipio' => $value[1]]);
            $this->info( 'Sanitización -> '.$value[2] ); 
        }
        
        $this->line('<fg=red;bg=yellow>::::::Inicia Sanitización Municipios Regionales:::::::</>');        

        foreach ($this->regionales as &$value) {
            DatosPostulado::where('entidadMunicipio', $value[0])
            ->where('municipio', $value[1])
            ->update(['municipio' => $value[2],'entidadMunicipio'=>$value[3]]);
            $this->info( 'FIN MATCH Regionales -> id:'.($value[0]-1).' valor:'.$value[1] ); 
        }        
        
        $this->line('<fg=red;bg=yellow>::::::Inicia Sanitización Municipios Regionales:::::::</>');

        foreach ($this->sanearRegionales as &$value) {
            DatosPostulado::whereIn('id',$value[0])
            ->update(['municipio' => $value[1],'entidadMunicipio'=>$value[2]]);
            $this->info( 'Sanitización -> '.$value[3] ); 
        }
        
        $this->line('<fg=red;bg=yellow>::::::Inicia Sanitización Niveles Tecnicos:::::::</>');
        
        foreach ($this->nivelesTecnicos as &$value) {
            DatosPostulado::where('nivelTecnico',$value[1])
            ->update( ['nivelTecnico' => $value[0] ] );
            $this->info( 'Sanitización -> '.$value[1] ); 
        }
        */
        $this->line('<fg=red;bg=green>::::::TERMINO PROCESO:::::::</>');
    }
}
