<?php

namespace App\Console\Commands;
use App\Models\DatosPostulado;

use Illuminate\Console\Command;

use App\Mail\PostuladoMail;
use App\Mail\SeleccionadoMail;
use Illuminate\Support\Facades\Mail;
// use Illuminate\Support\Facades\Log;

class EnviarCorreos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'enviarCorreos {inicio} {fin}';
    // protected $signature = 'config:register {activation=on}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Enviar Correos descripcion';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $horaInicio=date('Y-m-d H:i:s');

        $reportes=DatosPostulado::select('id','aPaterno','aMaterno','nombre','folio','correo','curp')
            ->where('editado',0)            
            ->whereBetween('id', array($this->argument('inicio'), $this->argument('fin')))
            ->where('correo', 'regexp', '^[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,63}$')
            ->get();
                
        foreach ($reportes as &$value) {
            // Log::info('Inicia el envio de correo a: '.$value['id'].' - '.$value['correo'].' - '.$value['curp']);
            $datos=array('name'=>$value['nombre'].' '.$value['aPaterno'].' '.$value['aMaterno'],'folio'=>$value['folio'],'enlace'=>'https://convocatoria.issste.gob.mx/');
            Mail::to( $value['correo'] )->send( new PostuladoMail($datos) );
            // Log::info('TERMINO ENVIO DE CORREO');
            $this->info( $value['id'].' - '.$value['correo'].' - '.$value['curp'] ); 
        }
        
        $this->info( 'TERMINO DE ENVIAR CORREOS RANGO['.$this->argument('inicio').','.$this->argument('fin').'] - Inicio:'.$horaInicio.' - Fin:'.date('Y-m-d H:i:s') );
        $this->info('TOTAL: '.$reportes->count());
        // Log::info( 'TERMINO DE ENVIAR CORREOS RANGO['.$this->argument('inicio').','.$this->argument('fin').'] - Inicio:'.$horaInicio.' - Fin:'.date('Y-m-d H:i:s') );

    }
}
