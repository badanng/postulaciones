<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Mail\SeleccionadoMail;
use Illuminate\Support\Facades\Mail;

// use Illuminate\Support\Facades\Log;

class CorreosUsuarios extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'correosUsuarios {name} {correo} {contrasena}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Enviar Correos para usuarios, pero a partir de un array no de la base';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $value=array('name'=> $this->argument('name'),'correo'=>$this->argument('correo'),'contrasena'=>$this->argument('contrasena') );
        // Log::info('Inicia el envio de correo a: '.$value['name'].' - '.$value['correo']);
        Mail::to( $value['correo'] )->send( new SeleccionadoMail($value) );
        $this->info( $value['name'].' - '.$value['correo'].' - '.$value['contrasena'] );        
        $this->info( 'TERMINO de enviar' );
        // Log::info('TERMINO DE ENVIAR CORREO:'.$value['name'].' - '.$value['correo']);
    }
}
