<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Artisan;

class UtilidadDev extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UtilidadDev';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ejecuta migraciones, agrega campos y seeds para el entorno';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//////////////////////MODULO CONTRATACIONES        
        $this->line('<fg=black;bg=blue>:::::::::::::::::::: INICIA PROCESO ::::::::::::::::::::</>');
    
        // Artisan::call('migrate');
        // $this->info( 'ACABO MIGRACION' );
        
        Artisan::call('cache:clear');        
        $this->info( 'cache:clear' );
        Artisan::call('config:clear');
        $this->info( 'config:clear' );

                
        $this->line('<fg=black;bg=blue>::::::::::::::::::: TERMINO PROCESO ::::::::::::::::::::</>');        
//////////////////////MODULO CONTRATACIONES        
    }
}
