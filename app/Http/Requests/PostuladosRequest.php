<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostuladosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [   
            'entidadMunicipio'=> 'required',    
            'municipio' => 'required',
            'nivelTecnico'=>'required',
            'curp' => 'required|unique:datos_postulados,curp',
            'rfc' => 'required',
            'nombre' => 'required',
            'aPaterno' => 'required',
            'aMaterno' => 'required',
            'fechaNacimiento' => 'required',
            'celularUno' => 'required',
            'correo' => 'required|email|confirmed|unique:datos_postulados,correo',
            'direccion' => 'required',
            'codigoPostal' => 'required'                    
        ];
    }

    public function messages()
    {
        return [
            'entidadMunicipio.required' => 'Seleccione Entidad',
            'municipio.required' => 'Seleccione Municipio',
            'nivelTecnico.required'=>'Seleccione Nivel de Escolaridad',
            'curp.required' => 'CURP es requerido',
            'curp.unique' => 'CURP ya esta registrado',
            'rfc.required' => 'RFC es requerido',
            'nombre.required' => 'Nombre es requerido',   
            'aPaterno.required' => 'Apellido Paterno es requerido',   
            'aMaterno.required' => 'Apellido Materno es requerido',   
            'fechaNacimiento.required' => 'Fecha Nacimiento es requerido',   
            'celularUno.required' => 'Celular es requerido',   
            'correo.required' => 'Correo es requerido',
            'correo.email'=>'No es un correo valido',
            'correo.confirmed' => 'Verifique su correo',             
            'direccion.required' => 'Direccion es requerido',   
            'codigoPostal.required' => 'Codigo Postal es requerido',           
        ];
    }    


}
