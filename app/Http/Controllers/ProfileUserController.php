<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Menu;
use App\Models\CatEntidad;

class ProfileUserController extends Controller
{
  public function perfil()
  {
  	 $id = Auth::user()->id;

  	$user = User::from('users')
          			->select('users.*')
           			->where('users.id', '=', $id)->get();
    $ubicaciones = CatEntidad::all();

//return $user;
      return view('users/perfil',compact('ubicaciones'))->with('user',$user);
  }


}
