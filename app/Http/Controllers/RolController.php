<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Rol;
use App\Models\Menurol;
use App\Models\LocationModel;

class RolController extends Controller
{
  public function listRol(){//regresa todos los usuarios registrados en la bd excepto el usuario logeado
    $allRol = Rol::all();
      return view('roles/control_roles')->with('allRol',$allRol);
     }// fin de listRol

    public function insertDataRol(Request $request)//funcion que permite el registro de roles
       {
         $roll = new Rol();
         $roll->rol = $request->rol;
         $roll->nombre_rol = $request->nombre_rol;
         $roll->descripcion = $request->descripcion;
         $roll->save();
       }//fin e la funcion insertdataRol


    public function permisosMenuRol(Request $request){


      foreach ($request->arreglo as $valor) {
      	
              $menuRol= new Menurol();
              $menuRol->id_menu = $valor;
              $menuRol->id_rol = $request->id;

               $menuRol->save();
      }
       
      //$menuRol= new Menurol();
    }   
}
