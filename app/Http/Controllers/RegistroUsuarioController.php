<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\DatosUsuariosModel;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Crypt;

class RegistroUsuarioController extends Controller
{
	public function insertData(Request $request)//funcion que permite el registro de ususarios
    {
        $usuarios = new User();
        $usuarios->nombre = $request->nombre;
        $usuarios->puesto = $request->puesto;
        $usuarios->ubicacion = $request->ubicacion;
        $usuarios->email = $request->email;
        $usuarios->unidad_administrativa = $request->unidad_administrativa;
        $usuarios->password = bcrypt($request->password);
        $usuarios->id_rol = '1';

        $usuarios-> save();
    }//fin e la funcion insertdata

     public function updateData(Request $request){//actualiza los datos del usuario que estan en la tabla control 

        // return $request;

       $id = Auth::user()->id;
       $usuarios = User::find($request->id);
       
       $usuarios->nombre = $request->nombre;
       $usuarios->puesto = $request->puesto;

       $usuarios->id_entidad = $request->id_entidad;
       $usuarios->telefono = $request->telefono;
       $usuarios->id_rol = $request->rol_usuario;
      //  $usuarios->id_ubicacion = $request->unidad;

       $usuarios->email = $request->email;

      $usuarios->save();
      // Cache::flush();
     }


		 public function deleteData(Request $request)
     {
        $usuarios = User::find($request->id);
        $usuarios->estatus = 0;
        $usuarios->save();
        // Cache::flush();
      }

      public function updatePassword(Request $request){//cambia el password desde la vista controlPersonal
        $usuarios = User::find($request->id);     
          $passwordnew = $request->new_pass;
          $passwordneww = $request->new_pass_confirm;
            if($passwordnew == $passwordneww){
                $usuarios->password=bcrypt($passwordnew);
                $usuarios->save();  
            } //fin de if
      } //-- fin de la funcion --//

      public function updateDataPersonal(Request $request){
        $id = Auth::user()->id;
        $usuarios = User::find($id);
        $contraseniaActual = $usuarios->password;
        $passwordInggresada = $request->passwordAct;

        if(password_verify($passwordInggresada, $contraseniaActual)){

             $passwordnew = $request->passwordnew;
             $passwordneww = $request->passwordneww;

               if($passwordnew == $passwordneww){
                  $usuarios->password=bcrypt($passwordnew);
                  $usuarios->save();  
              }
        }
    }
}
