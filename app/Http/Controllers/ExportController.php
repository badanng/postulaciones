<?php

namespace App\Http\Controllers;
use App\Exports\PuestoUnidadExport;
use App\Exports\UsuariosExport;
use Illuminate\Http\Request;

use App\Models\DatosPostulado;
use Illuminate\Support\Facades\DB;

use App\Models\CatEntidad;
use App\Models\CatMunicipio;
use App\Models\CatNivelTecnico;
use App\Models\User;

use Illuminate\Support\Collection as Collection;

class ExportController extends Controller
{
        public function __construct(){		
		$this->middleware(['auth']);
        }
        
	public function reportePuestoUnidad(PuestoUnidadExport $postuladosExport,Request $request){
                return $postuladosExport->datosRequest($request);
        }
                
        public function reportePorUsuarios(UsuariosExport $postuladosExport,Request $request){
                return $postuladosExport->datosRequest($request);
        }
}
