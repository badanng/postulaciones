<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\UnidadesActivas;
use App\Models\DatosPostulado;
use Illuminate\Http\Request;
use App\Models\CatMunicipio;
use Illuminate\Support\Str;
use App\Models\CatEntidad;
use App\Models\CatBajas;
use App\Models\User;
use Session;

class HomeController extends Controller
{
    
    public function index(Request $request){
        $ruta=$request->path();        
        $user = auth()->user();
        $paginacion = New DatosPostulado;       
        if($ruta=='contratacion'){                
            if( $user->id_rol==1 ){
                $paginacion=$paginacion->where('seleccionado',1);           
            }else{
                $paginacion=$paginacion->where('entidadMunicipio', $user->id_entidad)->where('seleccionado',1);
            }
            $paginacion=$paginacion->where(function ($query) {
                $query->where('status_proceso', 3)->orWhere('status_proceso',4)->orWhere('status_proceso',1)->orWhereNull('status_proceso');
            });
        }else{
            if( $user->id_rol>1 ){
                $paginacion=$paginacion->where('entidadMunicipio', $user->id_entidad);
            }
        }
        ///filtros
        if( $request->has('entidad') ){ 
			$paginacion=$paginacion->where('entidadMunicipio',$request->entidad);	
        }        
		if( $request->has('accion') ){ 
            switch ($request->accion) {
                case 5:
                    $paginacion=$paginacion->where('seleccionado',1)->where('status_proceso',null);
                break;
                case 6:
                    if($user->id_rol==2){
                        $paginacion=$paginacion                    
                        ->where(function ($query) use ($user) {
                            $query->where('estadoPostulado', '=', 0)->orWhere('descartado_por', 'like', '%'.$user->id.'%');
                        });                    
                    }else{
                        $paginacion=$paginacion->where(function ($query) {
                            $query->whereNotNull('descartado_por')->whereNull('seleccionado_por')->orWhere('estadoPostulado', 0);
                        });                        
                    }
                break;
                case 7:
                    $paginacion=$paginacion->where('seleccionado_por',$user->id);                    
                break;                                               
                default:  
                    $paginacion=$this->procesoPostulado($user->id_rol,$user,$paginacion,$request->accion);                  
            }
        }   
        if( $request->has('municipio') ){            
			$paginacion=$paginacion->where('municipio',$request->municipio);	
		}
        if( $request->has('nivel') ){ 
			$paginacion=$paginacion->where('nivelTecnico',$request->nivel); 
        }	
		if( $request->has('especialidad') ){ 
			$paginacion=$paginacion->where('especialidad',$request->especialidad); 
        }
        if( $request->has('curp') ){ 
			$paginacion=$paginacion->where('curp','like','%'.$request->curp.'%'); 
        }
        if( $request->has('nombre') ){
            $paginacion=$this->buscarPornombre($request->nombre,$paginacion);
		}        
        $paginacion=$paginacion->paginate(10)->appends([ 'nivel'=>$request->nivel,'entidad'=>$request->entidad,'especialidad'=>$request->especialidad,'accion'=>$request->accion,'municipio'=>$request->municipio,'curp'=>$request->curp,'nombre'=>$request->nombre ]);                
        $entidades=CatEntidad::select('id_entidad','entidad')->whereNotIn('id_entidad', [33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51])->get();
        if($ruta=='contratacion'){
            $contVars=$this->obtenerEstablecimentos($user);
            return view('Registro/contratacion',[ 'paginacion'=>$paginacion,'entidades'=>$entidades,'unidades'=>$contVars->unidades,'catBajas'=>$contVars->catBajas ] );
        }else{
            return view('layouts/home',compact(['paginacion','entidades']));
        }
    }

    public function buscarPornombre($nombreCompleto,$paginacion){
        $nombre=explode(' ',$nombreCompleto);            
        $a='';
        if( count($nombre)>3 ){     
            $apellidos=array_slice($nombre, -2, 2);           
            for ($i = 0; $i < count($nombre)-2; $i++) {
                $a=$a.' '.$nombre[$i];
            }
            $stripped = trim(preg_replace('/\s+/', ' ', $a));                
            $paginacion=$paginacion->where('nombre','like','%'.$stripped.'%');
            if( count($apellidos)>0 ){
                $paginacion=$paginacion->where('aPaterno','like','%'.$apellidos[0].'%');
            }
            if( count($apellidos)>1 ){
                $paginacion=$paginacion->where('aMaterno','like','%'.$apellidos[1].'%');
            }                
        }else{
            if( count($nombre)>0 ){  
                $paginacion=$paginacion->where('nombre','like','%'.$nombre[0].'%');
            }
            if( count($nombre)>1 ){  
                $paginacion=$paginacion->where('aPaterno','like','%'.$nombre[1].'%');
            }
            if( count($nombre)>2 ){  
                $paginacion=$paginacion->where('aMaterno','like','%'.$nombre[2].'%');
            }                
        }

        return $paginacion;
    }

    public function procesoPostulado($rol,$user,$paginacion,$estado){
        switch ($rol) {
            case 1:
                return $paginacion=$paginacion->where('status_proceso',$estado);
            break;
            case 2:
                return $paginacion=$paginacion->where('seleccionado_por',$user->id)->where('status_proceso',$estado);                            
            break;                       
            default: 
                return $paginacion=$paginacion->where('entidadMunicipio',$user->id_entidad)->where('status_proceso',$estado);
        }        
    }

    public function obtenerEstablecimentos($user){
        if( $user->id_rol>1 ){                
            if($user->id_entidad==9){
                $unidades = DB::table('unidades_activas')->join('establecimientos_salud', 'unidades_activas.id_establecimiento', '=', 'establecimientos_salud.id')->join('cat_master', 'establecimientos_salud.tipologia_id', '=', 'cat_master.id')
                ->join('cat_entidad', function ($join) use ($user) {
                    $join->on('establecimientos_salud.entidad_id', '=', 'cat_entidad.id_entidad')->whereIn('cat_entidad.id_entidad', [9,34,35,33] );
                })->select('unidades_activas.id_establecimiento as id', DB::raw('CONCAT(cat_master.etiqueta,establecimientos_salud.nombre_unidad) as unidad'))->get();
            }else{
                $unidades = DB::table('unidades_activas')->join('establecimientos_salud', 'unidades_activas.id_establecimiento', '=', 'establecimientos_salud.id')->join('cat_master', 'establecimientos_salud.tipologia_id', '=', 'cat_master.id')            
                    ->join('cat_entidad', function ($join) use ($user) {
                        $join->on('establecimientos_salud.entidad_id', '=', 'cat_entidad.id_entidad')
                            ->where('cat_entidad.id_entidad', '=', $user->id_entidad );
                    })->select('unidades_activas.id_establecimiento as id', DB::raw('CONCAT(cat_master.etiqueta,establecimientos_salud.nombre_unidad) as unidad'))->get();
            }
        }else{
            $unidades = DB::table('unidades_activas')->join('establecimientos_salud', 'unidades_activas.id_establecimiento', '=', 'establecimientos_salud.id')->join('cat_master', 'establecimientos_salud.tipologia_id', '=', 'cat_master.id')->join('cat_entidad', 'establecimientos_salud.entidad_id', '=', 'cat_entidad.id_entidad')->select('unidades_activas.id_establecimiento as id', DB::raw('CONCAT(cat_master.etiqueta,establecimientos_salud.nombre_unidad) as unidad'))->get();
        }
        $catBajas=CatBajas::select('id','nombre')->get();
        return (object)array('unidades'=>$unidades,'catBajas'=>$catBajas);
    }

}
