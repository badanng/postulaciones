<?php

namespace App\Http\Controllers;
use App\Http\Requests\PostuladosRequest;
use Illuminate\Support\Facades\Mail;
// use Illuminate\Support\Facades\Log;
use App\Models\DatosPostulado;
use Illuminate\Http\Request;
use App\Models\CatMunicipio;
use App\Models\Contratacion;
use App\Models\CatEntidad;

use App\Mail\SeleccionadoMail;
use App\Mail\PostuladoMail;

use Illuminate\Support\Str;

use App\Exports\PostuladosExport;
use Illuminate\Support\Facades\DB;

class RegistroController extends Controller
{

	public function __construct(){		
		$this->middleware(['auth'],[ 'except' => [ 'registro','insertarRegistro','municipios','getMunicipios','PostuladoRevalidacion','seguimientoView','seguimientoPostulado','getEntidades','edicionPostulado' ] ]);
	}

    public function registro(){			
		$entidades=$this->getEntidades();
	   return view('Registro/registro',compact(['entidades']));
   }	

    public function insertarRegistro(PostuladosRequest $request){		
		// return 'ESTAMOS EN MANTENIMIENTO, DISCULPE LAS MOLESTIAS';    	
		$request->validated();
		$braw=request()->all();
		unset($braw['correo_confirmation']);		
		$postulado=(new DatosPostulado)->fill( $braw );
		$postulado->terminosDeLabora=0;///parche
		$postulado->folio=(string) Str::uuid();//strtotime("now");	
		///pa' rapido
		if ( $request->hasFile('ineFile') ) {
			$postulado->ineFile=$request->file('ineFile')->store('public/adjuntos');
		}
		if ( $request->hasFile('rfcFile') ) {
			$postulado->rfcFile=$request->file('rfcFile')->store('public/adjuntos');					
		}
		if ( $request->hasFile('cedulaFile') ) {
			$postulado->cedulaFile=$request->file('cedulaFile')->store('public/adjuntos');			
		}		
		if ( $request->hasFile('tituloFile') ) {
			$postulado->tituloFile=$request->file('tituloFile')->store('public/adjuntos');				
		}			
		if ( $request->hasFile('certificadoFile') ) {
			$postulado->certificadoFile=$request->file('certificadoFile')->store('public/adjuntos');							
		}		
		if ( $request->hasFile('diplomaFile') ) {
			$postulado->diplomaFile=$request->file('diplomaFile')->store('public/adjuntos');
		}							
		$postulado->save();
		$nombreCompleto=$postulado->nombre.' '.$postulado->aPaterno.' '.$postulado->aMaterno;
		return redirect()->route('postulado')->with( ['postulado'=>$nombreCompleto,'folio'=>$postulado->folio] );    	
	}
	
	public function getMunicipios($id){
		return CatMunicipio::select('id','municipio')->where('status', 1)->where('id_entidad', $id)->orderBy('municipio', 'asc')->get();		
	}

	public function getEntidades(){
		return CatEntidad::select('id_entidad','entidad')->whereNotIn('id_entidad', [33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51])->get();		
	}

	public function seguimientoView(){
    	return view('Registro/seguimiento');		
	}

	public function seguimientoPostulado(Request $request){		
		$postulado=DatosPostulado::where('folio', $request->comprobar)->get();		
		if (count($postulado)>0) {
			return view('Registro/seguimiento',compact([ 'postulado' ]) );
		} else {
			return redirect()->route('seguimietoView')->with( ['nombre'=>1,'seleccionado'=>'No se encontraron registros' ] );
		}
	}

	public function edicionPostulado(Request $request){		
		///Rapido yayayaya
		$postulado = DatosPostulado::where('folio',$request->folio)->get();
		$postulado[0]->entidadMunicipio=$request->entidadMunicipio;
		$postulado[0]->disponibilidadViajar=$request->disponibilidadViajar;
		$postulado[0]->municipio=$request->municipio;
		$postulado[0]->nivelTecnico=$request->nivelTecnico;
		$postulado[0]->especialidad=$request->especialidad;
		$postulado[0]->terminosDeLabora=$request->terminosDeLabora;
		$postulado[0]->terminosDeLabora=$request->manifiesto;
		$postulado[0]->editado=1;
		
		if ( $request->hasFile('ineFile') ) {
			$postulado[0]->ineFile=$request->file('ineFile')->store('/public/adjuntos');				
		}
		if ( $request->hasFile('rfcFile') ) {
			$postulado[0]->rfcFile=$request->file('rfcFile')->store('public/adjuntos');				
		}
		if ( $request->hasFile('cedulaFile') ) {
			$postulado[0]->cedulaFile=$request->file('cedulaFile')->store('public/adjuntos');	
		}		
		if ( $request->hasFile('tituloFile') ) {
			$postulado[0]->tituloFile=$request->file('tituloFile')->store('public/adjuntos');
		}			
		if ( $request->hasFile('certificadoFile') ) {
			$postulado[0]->certificadoFile=$request->file('certificadoFile')->store('public/adjuntos');
		}
		if ( $request->hasFile('diplomaFile') ) {
			$postulado[0]->diplomaFile=$request->file('diplomaFile')->store('public/adjuntos');
		}
		$postulado[0]->save();		
		$nombreCompleto=$postulado[0]->nombre.' '.$postulado[0]->aPaterno.' '.$postulado[0]->aMaterno;
		return redirect()->route('postulado')->with( ['postulado'=>$nombreCompleto,'folio'=>$postulado[0]->folio] );		
	}
	
	public function mostrarDetalle($id){		
		$postuladoMap=DatosPostulado::where('id',$id)->with(['getEntidad','getMunicipio','getNivel'])->get();
		if($postuladoMap[0]->status_proceso>2){
			$contratacion=Contratacion::where('id_persona',$id)->get();			
			// $texto = DB::table('establecimientos_salud')          
			// ->join('cat_master', function ($join) use ($contratacion) {
			// 	$join->on('establecimientos_salud.tipologia_id', '=', 'cat_master.id')->where('establecimientos_salud.id', '=', $contratacion[0]->unidadMedicaAdscrita );
			// })->select(DB::raw('CONCAT(cat_master.etiqueta,establecimientos_salud.nombre_unidad) as unidad'))->get();
			// $contratacion[0]->nombreUnidad=$texto[0]->unidad;
			$postuladoMap[0]->contratacion=$contratacion[0];
		}
		return $postuladoMap;
	}

	public function reportePostulados(PostuladosExport $postuladosExport,Request $request){
		return $postuladosExport->datosRequest($request);
	}

	public function accionesPostulado(Request $request){		
		$user = auth()->user();
		switch ($request->estadoPostulado) {
			case 0:///Descartado
				$postulado=$this->mostrarDetalle($request->id);
				if( $postulado[0]['disponibilidadViajar']==0 || isset($postulado[0]['status_proceso']) ){
					$affectedRows = DatosPostulado::where( 'id', $request->id )->update(array('estadoPostulado' => 0));
				}else{
					if( $postulado[0]['descartado_por']!=null ){
						$descartadosAg=$postulado[0]['descartado_por'].' '.$user->id;
					}else{
						$descartadosAg=$user->id;
					}
					$affectedRows = DatosPostulado::where( 'id', $request->id )->update( ['descartado_por' => $descartadosAg  ] );
				}
				return 0;				
			break;
			case 1:///Selccionado
				$postulado=$this->mostrarDetalle($request->id);
				if($postulado[0]['seleccionado']==0){
					$affectedRows = DatosPostulado::where( 'id', $request->id )->update(['seleccionado' => 1,'seleccionado_por'=>$user->id]);
					return 1;	
				}else{
					return 2;
				}									
			break;
			case 2:///En proceso de contratación
				$affectedRows=DatosPostulado::where( 'id', $request->id )->update(['status_proceso' => 1]);
				return 3;
			break;
			case 3:///Declinación del aspirante
				DatosPostulado::where( 'id', $request->id )->update(['status_proceso' => 2]);
				return 4;
			break;
			case 4:///Contratado
				DatosPostulado::where( 'id', $request->id )->update(['status_proceso' => 3]);
				return 5;
			break;
			case 5:///ACTIVAR DESCARTADO
				$postulado=$this->mostrarDetalle($request->id);
				if( !isset($postulado[0]['seleccionado_por']) ){
					DatosPostulado::where( 'id', $request->id )->update(['descartado_por' => null,'estadoPostulado'=>1]);
					return 6;
				}else{
					return 2;
				}
			break;												
			default:
				
		}

	}

	public function guardarContratacion(Request $request){
		$checar=DatosPostulado::select('status_proceso')->where( 'id', $request->id_persona )->get();
		if($checar[0]->status_proceso==4){
			Contratacion::where( 'id_persona', $request->id_persona )->update(['fechaFinContrato' => $request->fechaFinContrato,'fechaInicioContrato' => $request->fechaInicioContrato,'numeroDePlaza'=>$request->numeroDePlaza,'motivoBaja' =>$request->motivoBaja,'unidadMedicaAdscrita'=>$request->unidadMedicaAdscrita]);
			return 1;
		}
		if($checar[0]->status_proceso==3){
			if(isset($request->fechaFinContrato)){
				Contratacion::where( 'id_persona', $request->id_persona )->update(['fechaFinContrato' => $request->fechaFinContrato,'status'=>0,'motivoBaja' =>$request->motivoBaja]);
				DatosPostulado::where( 'id', $request->id_persona )->update(['status_proceso' => 4]);
			}else{
				$contratacion = Contratacion::where('id_persona',$request->id_persona)->get();
				$contratacion[0]->numeroDePlaza=$request->numeroDePlaza;
				$contratacion[0]->numeroDeEmpleado=$request->numeroDeEmpleado;
				$contratacion[0]->unidadMedicaAdscrita=$request->unidadMedicaAdscrita;	
				$contratacion[0]->fechaInicioContrato=$request->fechaInicioContrato;
				$contratacion[0]->save();
			}
		}else{
			$contratacion=(new Contratacion)->fill( $request->all() );
			$contratacion->contrato=auth()->user()->id;
			DatosPostulado::where( 'id', $request->id_persona )->update(['status_proceso' => 3]);
			$contratacion->save();						
		}		
		return 1;		
	}

}
