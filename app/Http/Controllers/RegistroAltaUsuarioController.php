<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DatosUsuariosModel;
use App\Models\User;
use Illuminate\Support\Facades\Cache;

class RegistroAltaUsuarioController extends Controller
{
    public function registerUser (Request $request){

		// return $request;
		$usuario = new User();
		$validate_email = User::where('email', $request->email)->exists();
		if($validate_email == true){
			return 0;
		}else{		

			$usuario = new User();
			$usuario->email = $request->email;
			$usuario->password = bcrypt($request->password);
			$usuario->nombre = $request->nombre;
			$usuario->puesto = $request->puesto;
			$usuario->estatus = 1;
			$usuario->avatar = 'avatar.png';
			$usuario->id_rol = $request->id_rol;
			$usuario->id_entidad = $request->estado;
			// $usuario->id_municipio = $request->municipio;
			// $usuario->id_ubicacion = $request->unidad;	
			$usuario->save();
			return 1;
		}

        // Cache::flush();
    }
}
