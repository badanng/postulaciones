<?php
 
namespace App\Http\Controllers;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Rol;
use App\Models\DatosUsuariosModel;
use App\Models\LocationModel;
use App\Models\Menu;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redirect;
use Response,DB,Config;
use App\Models\CatEntidad;
use App\Models\CatMunicipio;
use App\Models\Ubicaciones;
use App\Models\CatMaster;
use App\Models\EstablecimientoSalud;
use Datatables;

class UsuariosController extends Controller
{
  public function listAll(){//regresa todos los usuarios registrados en la bd excepto el usuario logeado

   
          $key= "allUser.page".request('page',1);
          $allUser= Cache::rememberForever($key, function(){
          $id = Auth::user()->id;

                return User::from('users')
                ->select('users.*')
                ->where('users.id', '!=', $id)
                ->where('users.estatus', '=', 1)
                ->latest()->paginate(10);
                });
          $id = Auth::user()->id;
          $user = User::from('users')
                ->select('users.*')
                ->where('users.id', '=', $id)->get();

                return view('users/control_usuarios')->with('allUser',$allUser);

     }// fin de listALL

		 public function listEntidades()  //-- funcion para llenar los select
		{
      // $entidades = CatEntidad::all(); BRAWgit 
      $entidades=CatEntidad::whereNotIn('id_entidad', [33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51])->get();
			// dd($roles);
			return response()->json(array( 'entidad' => $entidades));
		}


     public function listMunicipios( Request $request)  //-- funcion para llenar los select
    {

       $municipios = CatMunicipio::where('id_entidad', '=',$request->id_entidad)->get();

      return response()->json(array( 'municipios' => $municipios));
    }

    public function listUnidades( Request $request)  //-- funcion para llenar los select
    {

 
       $inmueble = EstablecimientoSalud::where('municipio_id', '=',$request->id_municipio)->where('entidad_id', '=',$request->id_entidad)->get();
 
      return response()->json(array( 'inmuebles' => $inmueble));
    }


  public function datosUsuario(){//regresa datos de usuario actualmente logeado
           Cache::flush();//quitar
          $key= "allUser.page".request('page',1);
          $user= Cache::rememberForever($key, function(){
          $id = Auth::user()->id;

                return User::from('users')
                ->select('users.*')
                ->where('users.id', '=', $id)->with(['usuario','rol'])->get();
                });


                 return $user;



     }// fin de listALL


    // public function datosPersonal(){


    //   return view('users/control_personal');
      
    // }


    public function datosPersonal(){//lista los usuarios en la vista controlusuarios
      $tabla = User::from('users')->select('*')->where('users.estatus',  '!=' , 0)->get();
      $roles=Rol::all();
      return view('users/control_personal',compact(['tabla','roles']) );
    }


    public function userList(Request $request)
    {
        $allPersonal = DB::table('users')->select('*')->where('users.estatus', '!=', 0);
        return datatables()->of($allPersonal)
            ->make(true);
    }


}

