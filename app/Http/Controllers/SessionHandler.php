<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionHandler extends Controller 
{
    //
    public function destroy(Request $request) {
    	 $request->session()->flush();
    	return redirect("/");

    }
}
