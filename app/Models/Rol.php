<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable; 

class Rol extends Model  implements Auditable
{
	use \OwenIt\Auditing\Auditable;

  	protected $auditInclude = [
  		'rol','nombre_rol','descripcion'
  	];

     protected $table ='rol';
}
