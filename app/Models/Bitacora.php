<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bitacora extends Model
{
    //
protected $table ='bitacora';
protected $fillable = [
  'user_id',
  'tipo',
   'ip_address'
];

}
