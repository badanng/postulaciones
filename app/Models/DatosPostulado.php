<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class DatosPostulado extends Model
{
     protected $guarded = array('id');

     public function getEntidad(){        
          return $this->belongsTo('App\Models\Entidades', 'entidadMunicipio', 'id_entidad');
     }

     public function getMunicipio(){        
          return $this->belongsTo('App\Models\CatMunicipio', 'municipio');
     } 
     
     public function getNivel(){        
          return $this->belongsTo('App\Models\CatNivelTecnico', 'nivelTecnico');
     }      

     public function selecciono(){        
          return $this->belongsTo('App\Models\User', 'seleccionado_por');
     }

     // public function entidadMunicipio(){        
     //      return $this->belongsTo('App\Models\Entidades', 'entidadMunicipio', 'id_entidad')->pluck('entidad');
     //      // return $this->roles->pluck('name')->intersect($roles)->count();
     // }

}
