<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use OwenIt\Auditing\Contracts\Auditable; 
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use App\Models\Menu;

class User extends Authenticatable implements Auditable

{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','puesto', 'email', 'password','email','ubicacion','telefono','unidad_administrativa'
    ];


//protected $fillable = [
  //      'name','puesto', 'email', 'password','apellidoPaterno','apellidoMaterno','email','telefono','unidad_administrativa'
    //];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    use \OwenIt\Auditing\Auditable;

    protected $auditInclude = [
        'email', 'password','status','avatar','id_personal','id_rol','id_ubicacionLaboral'
    ];

    public function getAvatarUrl()//funcion para llamar la imagen de cada usuario
        {
               $id = Auth::user()->id;
                 $user = User::from('users')
                ->select('users.avatar')
                ->where('users.id', '=', $id)->get();
               //dd(Storage::url($user[0]->avatar));
            
                 return asset(Storage::url($user[0]->avatar));  
        }

    public function menuPrincipal(){//funcion para pintar el menu principal

        
        $rol = Auth::user()->id_rol;
        
   

        $menus = new Menu();
        $data = $menus->opcionesMenu($rol);
        $menuAll = [];
        foreach ($data as $line) {
            $item = [ array_merge($line, ['submenu' => $menus->gethijos($data, $line) ]) ];
            $menuAll = array_merge($menuAll, $item);
        }
  
       return $menus->menuAll = $menuAll;
    }



    public function persona(){
         return $this->belongsTo(DatosUsuariosModel::class, 'id_personal');
     }
    //

    public function rol(){

        return $this->belongsTo(Rol::class,'id_rol');

    }

    public function entidad(){        
        return $this->belongsTo('App\Models\Entidades', 'id_entidad', 'id_entidad');
    }    

    public function ubicaciones(){
        return $this->belongsTo(LocationModel::class,'id_ubicacionLaboral');
   }

}
