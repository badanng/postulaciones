<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatEntidad extends Model
{
    //
        protected $table ='cat_entidad';
        protected $primaryKey = 'id_entidad';
    	protected $guarded = ['id_entidad'];

    	
}
