<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable; 

class Menu extends Model implements Auditable
{
   
  use \OwenIt\Auditing\Auditable;

  protected $table ='menu';


  protected $auditInclude = [
    'etiqueta', 'nombre_menu','prioridad','padre','url','clase','status'];
    


   public function opcionesMenu($id_rol)
    {
      
           $menuss = Menu::from('menu')
          ->join('rol_menu', 'menu.id', '=', 'rol_menu.id_menu')
          ->orderby('padre')
          ->orderby('prioridad')  
          ->orderby('etiqueta')         
          ->where('rol_menu.id_rol','=', $id_rol)
          ->where('menu.status','=', '1')
          ->select('menu.*')
          ->get()
          ->toArray();

        //dd($menuss);
         return $menuss;

    }




	public function gethijos($data, $line)
    {
        $hijos = [];
        foreach ($data as $line1) {
            if ($line['id'] == $line1['padre']) {
                $hijos = array_merge($hijos, [ array_merge($line1, ['submenu' => $this->gethijos($data, $line1) ]) ]);
                
            }
        }

      
        return $hijos;
    }
 


    // public static function menus($id)
    // {
    //     $menus = new Menu();
    //     $data = $menus->opcionesMenu($id);
    //     $menuAll = [];
    //     foreach ($data as $line) {
    //         $item = [ array_merge($line, ['submenu' => $menus->gethijos($data, $line) ]) ];
    //         $menuAll = array_merge($menuAll, $item);
    //     }
  
    //    return $menus->menuAll = $menuAll;
    // }







}
