<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable; 

class RolMenu extends Model implements Auditable
{
    //
    use \OwenIt\Auditing\Auditable;

  	protected $auditInclude = [ 
  		'id_rol','id_menu'
  	];
}
