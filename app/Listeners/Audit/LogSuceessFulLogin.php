<?php

namespace App\Listeners\Audit;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Auth\Events\Login; //utlizamos clase login
use App\Models\Bitacora;
use OwenIt\Auditing\Events\Audited;
use Illuminate\Http\Request;

class LogSuceessFulLogin
{
    /**
     * Create the event listener.
     * @param  Request  $request
     * @return void
     */
    public function __construct(Request $request)
    {
        //
         $this->request = $request;

    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        //

               
            Bitacora::create([
            'user_id' => $event->user->id, 
            'tipo' => 'LOGIN',
            'dispositivo' => 'chrome',
            'ip_address' => $this->request->ip()
        ]);   
    }
}
