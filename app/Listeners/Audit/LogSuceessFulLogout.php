<?php

namespace App\Listeners\Audit;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Auth\Events\Logout;  //utlizamos clase login
use App\Models\Bitacora;
use OwenIt\Auditing\Events\Audited;
use Illuminate\Http\Request;

class LogSuceessFulLogout
{/**
     * Create the event listener.
     * @param  Request  $request
     * @return void
     */
    public function __construct(Request $request)
    {
        //
         $this->request = $request;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        //
         Bitacora::create([
            'user_id' => $event->user->id, 
            'tipo' => 'LOGOUT',
            'dispositivo' => 'chrome',
            'ip_address' => $this->request->ip()

        ]);   
    }
}
