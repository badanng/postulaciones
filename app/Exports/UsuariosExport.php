<?php

namespace App\Exports;

use App\Models\DatosPostulado;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Contracts\Support\Responsable;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

use App\Models\CatEntidad;
use App\Models\CatMunicipio;
use App\Models\CatNivelTecnico;
use App\Models\User;

class UsuariosExport implements FromArray,Responsable,ShouldAutoSize,WithStrictNullComparison ,WithHeadings
{

    use Exportable;

    private $fileName='Postulados.csv';
    private $datos;

    public function headings(): array
    {
        $niveles=CatNivelTecnico::select('detalle')->get();
        $a=['USUARIO'];
        foreach ($niveles as &$valor) {
                array_push($a,$valor['detalle']);
        }

        return $a;
    }     

    public function datosRequest($datos){
        $this->datos=$datos;
        return $this;
    }

    public function array(): array
    {

        $postulados=new DatosPostulado;
        $todo=array();        
        $usuarios=User::select('id','nombre')->get();
        foreach ($usuarios as &$valor) {

                $niveles=CatNivelTecnico::select('id as nivel')->addSelect(['contador' => $postulados::select(DB::raw('count(nivelTecnico) cantidad'))                                                
                ->where('seleccionado_por', $valor['id'])
                ->whereColumn('nivelTecnico', 'nivel')                        
                ])->get();

                $satan=[ $valor['nombre'],$niveles[0]['contador'],$niveles[1]['contador'],$niveles[2]['contador'],$niveles[3]['contador'],$niveles[4]['contador'],$niveles[5]['contador'],$niveles[6]['contador'],$niveles[7]['contador'],$niveles[8]['contador'],$niveles[9]['contador'],$niveles[10]['contador'],$niveles[11]['contador'],$niveles[12]['contador'],$niveles[13]['contador'],$niveles[14]['contador'],$niveles[15]['contador'] ];                

                array_push( $todo, $satan);                       
        }
        return $todo;

    }

}
