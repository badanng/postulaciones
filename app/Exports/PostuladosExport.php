<?php

namespace App\Exports;

use Excel;
use Maatwebsite\Excel\Sheet;

// add event support
use Maatwebsite\Excel\Concerns\WithEvents;

use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Cell\Hyperlink;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

use App\Models\DatosPostulado;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\Support\Responsable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;



class PostuladosExport implements FromQuery, Responsable, WithHeadings,WithMapping,ShouldAutoSize #,WithDrawings,WithCustomStartCell,WithEvents
{

    // public function drawings()
    // {
    //     $drawing = new Drawing();
    //     $drawing->setName('Postulaciones');
    //     $drawing->setDescription('Postulaciones');
    //     $drawing->setPath(public_path('/images/convocatoria.jpg'));
    //     $drawing->setHeight(250);
    //     $drawing->setCoordinates('B1');

    //     return $drawing;
    // }    

    // public function startCell(): string
    // {
    //     return 'A15';
    // }    

    use Exportable;

    private $fileName='Postulados.csv';
    private $datos;
    public $a=1;
    
    public function datosRequest($datos){
        $this->datos=$datos;
        return $this;
    }
    
    public function headings(): array
    {
        return [
            '#',
            'ID',
            'CURP',
            'RFC',
            'APELLIDO PATERNO',
            'APELLIDO MATERNO',
            'NOMBRE',
            'FECHA DE NACIMIENTO',
            'NACIONALIDAD',
            'ENTIDAD',
            'MUNICIPIO',
            'NIVEL TECNICO',
            'CEDULA',
            'ESPECIALIDAD',
            'OTRA ESPECIALIDAD',
            'TELEFONO',
            'CELULAR',
            'OTRO CELULAR',
            'CORREO',
            'RFC ADJUNTO',
            'INE ADJUNTO',
            'TITULO ADJUNTO',
            'CEDULA ADJUNTO',
            'CERTIFICADO ADJUNTO',
            'DIPLOMA ADJUNTO',
            'DIRECCION',
            'CODIGO POSTAL',
            'SELECCIONADO POR'
        ];
    }    

    public function map($invoice): array
    {
        return [
            $this->a++,
            $invoice->id,
            $invoice->curp,
            $invoice->rfc,
            $invoice->aPaterno,
            $invoice->aMaterno,            
            $invoice->nombre,
            $invoice->fechaNacimiento,
            $invoice->nacionalidad,            
            $invoice->entidad['entidad'],
            $invoice->municipio,
            $invoice->nivelTecnico,
            $invoice->noCedula,
            $invoice->especialidad,
            $invoice->otraEspecialidad,
            $invoice->telefono,
            $invoice->celularUno,
            $invoice->celularDos,
            $invoice->correo,
            isset($invoice->rfcFile)?'http://convocatoria.issste.gob.mx/storage/'.$invoice->rfcFile:'',
            isset($invoice->ineFile)?'http://convocatoria.issste.gob.mx/storage/'.$invoice->ineFile:'',
            isset($invoice->tituloFile)?'http://convocatoria.issste.gob.mx/storage/'.$invoice->tituloFile:'',
            isset($invoice->cedulaFile)?'http://convocatoria.issste.gob.mx/storage/'.$invoice->cedulaFile:'',
            isset($invoice->certificadoFile)?'http://convocatoria.issste.gob.mx/storage/'.$invoice->certificadoFile:'',
            isset($invoice->diplomaFile)?'http://convocatoria.issste.gob.mx/storage/'.$invoice->diplomaFile:'',
            $invoice->direccion,
            $invoice->codigoPostal,
            $invoice->selecciono['nombre'],            
        ];
    }    

    public function query()
    {
        $user = auth()->user();
        // $postulados = New DatosPostulado;
        $postulados=DatosPostulado::query();

		if( isset($this->datos->accion) ){ 

            switch ($this->datos->accion) {
                case 1:
                    $postulados=$postulados->where('seleccionado',1)
                                ->where('status_proceso',null);
                break;
                case 2:
                    // $postulados=$postulados                    
                    // ->where(function ($query) use ($user) {
                    //     $query->where('estadoPostulado', '=', 0)
                    //           ->orWhere('descartado_por', 'like', '%'.$user->id.'%');                            
                    // });  
                    if($user->id_rol==2){
                        $postulados=$postulados                    
                        ->where(function ($query) use ($user) {
                            $query->where('estadoPostulado', '=', 0)                                
                                  ->orWhere('descartado_por', 'like', '%'.$user->id.'%');
                        });                    
                    }else{
                        $postulados=$postulados->where(function ($query) {
                            $query->whereNotNull('descartado_por')->whereNull('seleccionado_por')
                                  ->orWhere('estadoPostulado', 0);
                        });
                        // $postulados=$postulados->where('estadoPostulado',0)->union($a)->orderBy('id', 'asc');                                               
                    }                                                    
                break;
                case 3:
                    $postulados=$postulados->where('seleccionado_por',$user->id);                    
                break;
                case 4:
                    ///En proceso de contratación
                    $postulados=$this->procesoPostulado($user,$postulados,1);
                break;  
                case 5:
                   ///Contratado
                   $postulados=$this->procesoPostulado($user,$postulados,3);
                break;
                case 6:
                    ///Declinación del aspirante
                    $postulados=$this->procesoPostulado($user,$postulados,2);
                 break;                                                
                default:                    
            }            

        }

        if( isset($this->datos->entidad) ){ 
			$postulados=$postulados->where('entidadMunicipio',$this->datos->entidad);	
        }   
        if( isset($this->datos->municipio) ){
            // $municipio=CatMunicipio::select('municipio')->where('id', $request->municipio)->get();
			$postulados=$postulados->where('municipio','like','%'.$this->datos->municipio.'%');	
		}
        if( isset($this->datos->nivel) ){ 
			$postulados=$postulados->where('nivelTecnico',$this->datos->nivel); 
        }	
		if( isset($this->datos->especialidad) ){ 
			$postulados=$postulados->where('especialidad',$this->datos->especialidad); 
        }
        if( isset($this->datos->curp) ){ 
			$postulados=$postulados->where('curp','like','%'.$this->datos->curp.'%'); 
		}
        
        if( $user->id_rol>1 ){
            $postulados=$postulados->where('entidadMunicipio', $user->id_entidad);            
        }        
        
        return $postulados;
    }


    public function procesoPostulado($user,$paginacion,$estado){
        switch ($user->id_rol) {
            case 1:
                return $paginacion=$paginacion->where('status_proceso',$estado);
            break;
            case 2:
                return $paginacion=$paginacion->where('seleccionado_por',$user->id)
                ->where('status_proceso',$estado);                            
            break;
            case 3:
                return $paginacion=$paginacion->where('entidadMunicipio',$user->id_entidad)
                ->where('status_proceso',$estado);                            
            break;                        
            default:                            
        }        
    }    



    // public function registerEvents(): array
    // {
    //     return [
    //         AfterSheet::class    => function(AfterSheet $event) {
    //             /** @var Worksheet $sheet */
    //             foreach ($event->sheet->getColumnIterator('S') as $row) {
    //                 foreach ($row->getCellIterator() as $cell) {
    //                     if (str_contains($cell->getValue(), '://')) {
    //                         $cell->setHyperlink(new Hyperlink($cell->getValue()));

    //                          // Upd: Link styling added
    //                          $event->sheet->getStyle($cell->getCoordinate())->applyFromArray([
    //                             'font' => [
    //                                 'color' => ['rgb' => '0000FF'],
    //                                 'underline' => 'single'
    //                             ]
    //                         ]);
    //                     }
    //                 }
    //             }
    //         },
    //     ];
    // }    
    


}
