<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BoletinesMail extends Mailable
{
    use Queueable, SerializesModels;

    public $subject="Nuevo módulo postulados (convocatoria.issste.gob.mx)";
    public $msj;

    public function __construct($message)
    {
        $this->msj=$message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.usuariosBoletin')
                    ->attach( public_path('/docs/guia-contratacion.pdf') );
    }
}
