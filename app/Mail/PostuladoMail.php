<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class PostuladoMail extends Mailable
{
    use Queueable, SerializesModels;

    public $subject="RECLUTAMIENTO";
    public $msj;

    protected $listen = [
        'Illuminate\Notifications\Events\NotificationSent' => [
        'App\Listeners\YourListenerClass',
     ],
    ]; 

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($message)
    {
        $this->msj=$message;
    } 

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.postuladosSeleccionados');
    }

    public function handle(NotificationSent $event)
    {

    //access your $event data here 
    //which includes notification details too
        Log::info('CORREO_ISSSTE:'.$event);

    }


}
