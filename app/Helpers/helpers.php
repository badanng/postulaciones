<?php

	function botonesPostulados($rol,$registro,$tipo=1){
		$descartaron=explode( " ",$registro['descartado_por'] );
		$selecciono=explode( " ",$registro['seleccionado_por'] );
		
		if($tipo==1){
			if( $rol==2 ){
				if( isset($registro['status_proceso']) ){
					return $botonag=$registro['status_proceso']==4?'<button type="button" class="btn bg-danger">Baja</button>':($registro['status_proceso']==3?'<button type="button" class="btn bg-success verPostulado" data-id="'.$registro['id'].'" data-contratado="1">CONTRATADO</button>':($registro['estadoPostulado']==0?'<button type="button" class="btn bg-danger">Descartado por proceso</button>':(($registro['status_proceso']==1)?'<button type="button" class="btn bg-success verPostulado" data-id="'.$registro['id'].'" data-seleccionado="1" data-proceso="1">En proceso de contratación</button>':'<button type="button" class="btn bg-warning">Declinación del aspirante</button>')));
				}else{    
					return $botonag= $registro['estadoPostulado']==0?'<button type="button" class="btn bg-danger">Descartado</button>':( in_array(Auth::user()->id, $descartaron) ?'<button type="button" class="btn bg-danger">Descartado</button>':($registro['seleccionado']==1?'<button type="button" class="btn bg-'.(( in_array(Auth::user()->id, $selecciono) )?'success verPostulado':'warning').'" '.(( in_array(Auth::user()->id, $selecciono) )?'data-id='.$registro['id'].' data-seleccionado="1"':'').' >Seleccionado</button>':($registro['estadoPostulado']==0?'<button type="button" class="btn bg-danger">Descartado</button>':"<button  class='btn btn-focus btn-sm m-btn m-btn--pill verPostulado' data-id=".$registro['id']." type='button'>Ver</button>")) ); 
				}
			}else{
				if( isset($registro['status_proceso']) ){
					return $botonag=$registro['status_proceso']==4?'<button type="button" class="btn bg-danger">Baja</button>':($registro['status_proceso']==3?'<button type="button" class="btn bg-success verPostulado" data-id="'.$registro['id'].'" data-contratado="1">CONTRATADO</button><br><small>'.$registro->selecciono['nombre'].'</small>':($registro['estadoPostulado']==0?'<button type="button" class="btn bg-danger verPostulado" data-id='.$registro["id"].' data-descartado="1">Descartado por proceso</button>':(($registro['status_proceso']==1)?'<button type="button" class="btn bg-success verPostulado" data-id="'.$registro['id'].'" data-seleccionado="1" data-proceso="1">En proceso de contratación</button><br><small>'.$registro->selecciono['nombre'].'</small>':'<button type="button" class="btn bg-warning">Declinación del aspirante</button><br><smaill>'.$registro->selecciono['nombre'].'</small>')));
				}else{
					return $botonag= ($registro['seleccionado']==1?'<button type="button" class="btn bg-warning verPostulado" data-id='.$registro["id"].' data-seleccionado="1">Seleccionado</button> <br> <small>'.$registro->selecciono['nombre'].'</small>':($registro['estadoPostulado']==0 || isset( $registro['descartado_por'] ) ?'<button type="button" class="btn bg-danger verPostulado" data-id='.$registro["id"].' data-descartado="1">Descartado</button>':"<button  class='btn btn-focus btn-sm m-btn m-btn--pill verPostulado' data-id=".$registro['id']." type='button'>Ver</button>")) ;
				}
			}	
		}else{			
			$clase=(!$registro['status_proceso']?'btn-primary btnseguimiento':($registro['status_proceso']==3?'btnseguimiento bg-success':($registro['status_proceso']==4?'btn-danger btnseguimiento':($registro['status_proceso']==1?'btn-primary btnseguimiento':'btn-warning') ) ) );
			
			$texto=(!$registro['status_proceso']?'Seguimiento':($registro['status_proceso']==3?'Contratado':($registro['status_proceso']==4?'Baja':($registro['status_proceso']==1?'Seguimiento':'Declinacion') ) ) );

			
			return '<button data-id="'.$registro["id"].'" type="button" class="btn btn-sm '.$clase.'">'.$texto.'</button>';
		}
			
	}


?>